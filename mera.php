<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>MERA PEAK CLIMB</h1>
            <p style="font-size:17px">“Mera Peak Climb one of the highest trekking peaks of Nepal Himalaya
                Enjoy the adventure within remote corners of eastern Khumbu valley
                Highest successful climbing record on Mera Peak than other peaks of the country
                Straight-forwards climb of less technical skill with fabulous mountain panorama
                Within hidden areas of Inku valley enclosed by high hills and giant mountains
                An adventure of lifetime-experience with great climb on top Mera peak summit”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure and climb within hidden pockets of Khumbu far eastern valley’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Introduction:
                                    Mera Peak Climb an adventure that leads you to hidden corners east of Khumbu and
                                    Everest region, where the area of trek and climb leads you into beautiful Inku
                                    valley often spelled as Hinku located south to Mt. Everest and close towards Mt.
                                    Baruntse near Mt. Makalu.
                                    Mera Peak Climb leads you on top at above 6,461 m and 21,190 ft high, one of the
                                    highest trekking peaks within Nepal Himalaya where you can enjoy glorious moments
                                    facing outmost panorama of highest mountains as far your can take.
                                    Mera Peak Climb offer views of surrounding peaks Amadablam, Amphu, Baruntse, Makalu
                                    with Everest and as far towards Kanchenjunga in the eastern horizon a great
                                    adventure and feat that takes you on top with much time to prepare for the climb.
                                    We have taken the grade of the climb from popular French and Swiss Alpine Climbing
                                    Classification System as F which means ‘Facial Easy’ where ascent to the summit
                                    leads with straight-forward climb of least technical difficulty, however all
                                    climbers requires full climbing gear for safety measures.
                                    Mera Peak Climb starts from the town of Lukla after reaching on scenic short flight
                                    from Kathmandu and then beyond villages and human settlement with climb over few
                                    high passes of Zatwar-la above Kalo or Naulekh Himal that separates Inkhu with Dudh
                                    Kosi river valley.
                                    Finally after days of good and scenic walks reaching at the base of Mera Peak for
                                    great climb to its summit and then back to Lukla after feeling in great high spirit
                                    on completing Mera Peak Climb.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br>Hotels with camping facilities on walks and climb.<br>
                                    <b>Trek Grade: </b><br> Adventurous Walks<br>
                                    <b>Climbing Grade: </b><br> F ‘Facial Easy’ a French / Swiss Alpine Climbing Systems<br>
                                    <b>Area of Trek:</b><br> Nepal North Mid-East within Khumbu district of Everest<br>
                                    <b>People and Culture:</b><br> Populated by Sherpa the highlanders of Everest
                                    enriched
                                    with colorful Buddhist religion and impressive culture.<br>
                                    <b>Trek and Climb: </b><br> 14 Nights and 15 Days (Lukla to Lukla)<br>
                                    <b>Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b>Total Trip: </b><br> 18 Nights and 19 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br> April to May is good for trek and climb when wild-
                                    flowers in full bloom, where most of the days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to November another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of
                                    snow.

                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: </b> Arrival in Kathmandu and transfer to hotel.</br>
                                    <b> Day 02: </b> In Kathmandu preparation for the trek and climb.</br>
                                    <b> Day 03: </b> From Kathmandu flight to Lukla 2,820 m and trek to Chutanga
                                    3,020 m - 04 hrs.</br>
                                    <b> Day 04: </b> Trek to Thuli-Kharka 4,300 m - 06 hrs.</br>
                                    <b> Day 05: </b> Trek to Kothe 3,600 m - 05 hrs.</br>
                                    <b> Day 06: </b> Trek to Thangnak 4,356 m - 04 hrs.</br>
                                    <b> Day 07: </b> At Thangnak for acclimatization and short hike.</br>
                                    <b> Day 08: </b> Trek to Khare Mera Peak Base Camp at 5,045 m - 04 hrs.</br>
                                    <b> Day 09: </b> At Base Camp in Khare for acclimatization pre-climbing
                                    practice.</br>
                                    <b> Day 10: </b> Climb to Mera High Camp 5,780 m - 04 hrs.</br>
                                    <b> Day 11: </b> Ascent of Mera Peak (6,461 m/21,190 ft) back to Khare - 08
                                    hrs.</br>
                                    <b> Day 12: </b> Reserve Day as Contingency.</br>
                                    <b> Day 13: </b> Trek to Kothe - 06 hrs.</br>
                                    <b> Day 14: </b> Trek to Thuli-Kharka - 05 hrs.</br>
                                    <b> Day 15: </b> Trek to Chutanga via Zatwar-La pass - 05 hrs.</br>
                                    <b> Day 16: </b> Trek to Lukla- 04 hrs.</br>
                                    <b> Day 17: </b> Fly to Kathmandu and transfer to hotel with free afternoon.</br>
                                    <b> Day 18: </b> Reserve and contingency day in Kathmandu with individual
                                    activities.</br>
                                    <b> Day 19: </b> Depart Kathmandu for international departure homeward bound.</br>

                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu and transfer to hotel.</b></br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of Mera
                                    Peak Climb.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks and climb local culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>

                                    <b>Day 02: In Kathmandu preparation for the trek and climb.</b></br>
                                    A free day preparation for Mera Peak climb and treks around high Khumbu, with
                                    optional tour at places around Kathmandu world heritage sites of few hours, allows
                                    you getting ready with packing for next early morning flight to Lukla where our walk
                                    begins towards Mera Peak Climb.<br><br>

                                    <b> Day 03: From Kathmandu flight to Lukla 2,820 m and trek to Chutanga</b></br>
                                    3,020 m - 04 hrs.
                                    Morning reach Kathmandu domestic airport terminal, where short scenic flight of 35
                                    minutes takes you to Lukla, a busy and interesting Sherpa town at the height of
                                    2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage
                                    with porters to carry, where walk leads past Lukla town heading to Chutanga beyond
                                    Lukla town and village in sheer wilderness to camp at Chutanga in the middle of
                                    lovely woodland.<br><br>

                                    <b> Day 04: Trek to Thuli-Kharka 4,300 m - 06 hrs.</b></br>
                                    After overnight camp at Chutanga, adventure starts with climb to cross over Kalo
                                    Himal Ridge also called as Nau-Lekh that separates Khumbu area with Inkhu Valley and
                                    western Makalu region.
                                    Trek leads over first pass with steep climb and then reaching a top ridge festooned
                                    with many Buddhist prayer flags at the height of 4,450 m, the pass called Zatrwa Og,
                                    and Zatrwa-La at 4,600 ms, facing views across Dudh Kosi river valley and of
                                    Karyolung and Lumding Himal.
                                    From here descend to Thuli-Kharka for overnight camp in a isolated areas within
                                    Inkhu valley.<br><br>

                                    <b> Day 05: Trek to Kothe 3,600 m - 05 hrs.</b></br>
                                    After a nice and long day walk to Thuli Kharka, morning walk leads to scenic areas
                                    within summer grazing pasture of Yak and other cattle within lovely woodland to
                                    reach an open and wide meadow at Kother for overnight stop with camps within its
                                    beautiful surrounding.<br><br>

                                    <b> Day 06: Trek to Thangnak 4,356 m - 04 hrs.</b></br>
                                    From Kothe, morning walk on downhill with views of Peak 43 and Kusum Kanguru peaks,
                                    then slowly reaching into a scenic valley within Inkhu area, overlooking Mera Peak
                                    South Face, as walk enters rhododendron and pine forest, with gradual walk through
                                    serene forest to reach at Mosum Kharka near Inkhu Drangka River.
                                    After Mousum Kharka walk continue towards north on leaving forest then reaching a
                                    wide open valley within a pasture having Yak herder huts to reach at Gondishung
                                    close to Thangnak area, a place with small wooden huts and shelters of stone wall
                                    enclosures. Gondishung with a small 200 year old small Gompa (monastery shrine),
                                    from here walk continues for another hour to our overnight camp at Thangnak, a small
                                    yak herder’s settlement with few tea houses and small lodges.<br><br>

                                    <b>Day 07: At Thangnak for acclimatization and short hike.</b></br>
                                    A lovely place for rest day and to get acclimatize better before the climb, with
                                    time here for short hike on hills for better views of surrounding nearby peaks, and
                                    rest preparation for next day walk to Mera Peak Base Camp at Khare.<br><br>

                                    <b> Day 08: Trek to Khare Mera Peak Base Camp at 5,045 m - 04 hrs.</b></br>
                                    From Thangnak, morning starts with moderate walk for few hours with views as route
                                    leads at the end of Inkhu valley, trek follows on moderate trail crossing over
                                    moraine leading to a dam of Charpatti Tal (lake), and then reaching high area lined
                                    with cairns overlooking glacial lake.

                                    Then climb leads to short descend at Dig Kharka, from here walk for an hour across
                                    boulder strewn hillside, to reach at overnight camp in Khare and Mera Peak Base
                                    Camp.<br><br>

                                    <b> Day 09: At Base Camp in Khare for acclimatization pre-climbing
                                        practice.</b></br>
                                    Rest day at base camp in Khare for Mera Peak climb, where members can practice climb
                                    on rocks and ice near campsite, necessary to get familiar with you personal gear in
                                    using crampons-ice axe and learning making fix ropes, where our guide will provide
                                    you with other necessary information regarding the climb.<br><br>

                                    <b> Day 10: Climb to Mera High Camp 5,780 m - 04 hrs.</b></br>
                                    After Khare at Mera base camp, walk leads further up to set High Camp to make climb
                                    more approachable, trek leads across Mera La to reach at Mera High Camp, after few
                                    hours of hard walking reaching at Mera High Camp, for overnight camp.
                                    Afternoon our expert guides will scout the area of the route of climb and will fix
                                    ropes where necessary, rest of the day preparation for Mera Peak climb, this high
                                    camp offers outstanding views of Mt. Everest, Mt. Makalu, Mt. Cho Oyu, Mt. Lhotse,
                                    Nuptse, Chamlang and Baruntse peaks.<br><br>

                                    <b>Day 11: Ascent of Mera Peak (6,461 m/21,190 ft) back to Khare - 08 hrs.</b></br>
                                    Morning leads over Mera Glacier, heading straight on ice sections, as route takes
                                    with hard climb but without any technical difficulty and then slowly reaching on top
                                    Mera peak summit, rewards you with panorama of mountains that surrounds you, facing
                                    view of Mt. Everest, Mt. Lhotse, Mt. Makalu, Mt. Cho- Oyu as far towards
                                    Mt. Kanchenjunga in the east horizon, after a vigorous and long climb descend with
                                    care to base camp at Khare.<br><br>

                                    <b>Day 12: Reserve Day as Contingency.</b></br>
                                    A spare day as contingency in case of bad weather conditions, as Himalayan weather
                                    patterns sometimes changes even in good season, if all goes well using the extra day
                                    on the way back to Lukla with easier and short days on walks.<br><br>

                                    <b> Day 13: Trek to Kothe - 06 hrs.</b></br>
                                    From Khare and Mera Peak base camp return journey will be pleasant without problem
                                    of altitude gain, although walks leads to few ups and downs and back into green
                                    vegetations and tree lines at Kothe within a small cattle herders shelters.<br><br>

                                    <b> Day 14: Trek to Thuli-Kharka - 06 hrs.</b></br>
                                    Morning walk continues leads into woodland with steep climb on some small hills and
                                    continue walk along grassy meadow with gradual walk to reach our last camp of the
                                    adventure at Thuli - Kharka.<br><br>

                                    <b> Day 15: Trek to Chutanga via Zatrwa La pass 4,610 m - 06 hrs.</b></br>
                                    Today, last day within Inkhu valley where morning a long day walk with climb over
                                    high ridge of Naulekh or Kalo Himal and Zatrwa Og including Zatrwa-La, after
                                    strenuous climb then descend towards Chutanga within greenery and thick forest and
                                    then ending our walk with last overnight camp before Lukla.<br><br>

                                    <b> Day 16: Trek to Lukla - 04 hrs and transfer to a lodge.</b></br>
                                    Enjoying time within serene wilderness back into village and town where walk leads
                                    into beautiful green vegetation and woodland to reach above Lukla and then a short
                                    downhill to reach into a nice lodge at Lukla for overnight stop before short scenic
                                    flight back to Kathmandu, in Lukla enjoy the last dinner within Everest and Khumbu
                                    area.<br><br>

                                    <b> Day 17: Fly to Kathmandu and transfer to hotel with free afternoon.</b></br>
                                    Morning having breakfast in Lukla transfer to air terminal named after Tenzing and
                                    Hillary, the first to conquer Mt. Everest in 1953.
                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to
                                    reach Kathmandu airport and then transfer back to your respective hotels.<br><br>

                                    <b> Day 18: Reserve as contingency day in Kathmandu for individual
                                        activities.</b></br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Lukla or in the beginning, where extra day
                                    allows you with time for yourself for individual activities or join in our exclusive
                                    tour or just relax after a great adventure on high Khumbu and Everest base camp.<br><br>
                                    <b> Day 19: Depart Kathmandu for international departure homeward bound.</b></br>
                                    Approaching your last day in Kathmandu after great memories and experience on Mera
                                    Peak Climb, with last final day in Nepal where Mount Vision Trek staff and guide
                                    transfer you to Kathmandu international airport for your flight home ward bound.<br><br>


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



