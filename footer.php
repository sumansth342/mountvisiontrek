<div id="vision-subscribe" style="background-image: url(assets/images/footer.jpg); padding-top: 1px; " data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
            <h2 style="color:white;">Sign Up for a Newsletter</h2>
            <p>Sign up for our mailing list to get latest updates and offers.</p>
            <form class="form-inline qbstp-header-subscribe" action="subscribemailer.php" method="POST">
                <div class="row">
                    <div class="col-md-12 col-md-offset-0">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter your email">
                            <button type="submit" class="btn btn-primary">Subscribe</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<footer id="vision-footer" role="contentinfo" style="padding-bottom: 1px; padding-top: 1px;">
    <div class="container">
        <div class="row">
<!--                <div class="col-md-6  text-center vision-heading animate-box">-->
<!---->
<!--                    <p style="color:white; font-size:36px;">Recommended By:</p>-->
<!--                    <img  src="assets/images/tripadvisor.png" alt="Avatar" style="width:50px" height:50px>-->
<!--                    <img  src="assets/images/tripadvisor.png" alt="Avatar" style="width:50px" height:50px>-->
<!--                    <img  src="assets/images/tripadvisor.png" alt="Avatar" style="width:50px" height:50px>-->
<!--                </div>-->
                <div class="col-md-6  text-center vision-heading animate-box">
                    <p style="color:white; font-size:36px;">Affiliations:</p>
                    <img  src="assets/images/nepal-govt.png" alt="Avatar" style="width: 100px;" >
                    <img  src="assets/images/taan.png" alt="Avatar" style="width: 100px;" >
                    <img  src="assets/images/tourism.jpg" alt="Avatar"style="width: 100px;" >
                </div>


            </div>
        <div class="row row-pb-md">
            <div class="col-md-4 vision-widget">
                <h4>Tour Agency</h4>
                <p>We organize trekking, tour, bungy, rafting, paragliding, jungle safari and others as your interest. for further information please check contact information</p>
                <p>
                <ul class="vision-social-icons">
                    <li><a href="https://twitter.com/mountvisiontrek?lang=en" target="_blank"><i class="icon-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/Mount-Vision-treks-and-expedition-1960454267509742/" target="_blank"><i class="icon-facebook"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/mount-vision-treks-1a4497155/"><i class="icon-linkedin"></i></a></li>
                    <li><a href="https://www.instagram.com/mount_vision.treks/" target="_blank"><i class="icon-instagram"></i></a></li>
                    <li><a href="https://plus.google.com/113748352347524637037" target="_blank"><i class="icon-google-plus"></i></a></li>
                </ul>
                </p>
            </div>

            <div class="col-md-3 vision-widget col-md-push-1 ">
                <h4>For Travellers</h4>
                <p>
                <ul class="vision-footer-links">
                    <li><a href="company.php">Why With US</a></li>
                    <li><a href="insurance.php">Travel Insurance</a></li>
                    <li><a href="travel.php">Travel Guides</a></li>
                    <li><a href="#">Privacy & Policy</a></li>
                    <li><a href="terms.php">Terms & Conditions</a></li>
                </ul>
                </p>
            </div>


            <div class="col-md-5  vision-widget col-md-push-2">
                <h4>Contact Information</h4>
                <ul class="vision-footer-links">
                    <li>Nagarjun Nagarpalika kathmandu,Nepal <br> </li>
                    <li><a href="tel://+9779841960370">+9779841960370</a></li>
                    <li><a href="mailto:info@yoursite.com">info@mountvisiontreks.com</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>

                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> Mount Vision Pvt.Ltd.All rights reserved
                   </span>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>
