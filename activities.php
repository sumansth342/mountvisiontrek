<?php include('navigation.php');
$newpage = $_GET['id'];
?>
<?php
include "inc/connect.inc.php";

$sql = "SELECT * FROM tb_activities WHERE id =" . $newpage;

$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result)) {
        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image = $row['ads_image'];
        $ads_disc = $row['ads_disc'];
        $price_range = $row['price_range'];
    }
}

?>
<?php echo $image = '<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(' . $ads_image . ');">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>' ?>


<div class="vision-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="wrap-division">

                        <div class="panel">
                            <div class="panel-body" style="font-family: Arial;">
                                <h1><?php echo $ads_title; ?></h1>

                                <p style="font-size: larger"><?php echo $ads_disc; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="sidebar-wrap">
                    <div class="side search-wrap animate-box">
                        <h3>
                            <p class="price " style="color:white;"><span><?php echo $price_range; ?></span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <br>
                            <a href="booking.php?id=<?php echo $ads_title; ?>"
                               class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                            <a href="enquiry.php?id=<?php echo $ads_title; ?>"
                               class="btn btn-primary btn-outline btn-block">Enquiry</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include('footer.php'); ?>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lightgallery").lightGallery();
        });
    </script>


