<?php
//session_start();
require "navigation.php";
require "inc/connect.inc.php";

$x = $_GET['id'];
?>
<div id="page">
    <aside id="vision-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(assets/images/cover-img-3.jpg);">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h1>BooK NoW</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>
    <div id="vision-contact">
        <div class="container">
            <div class="row">

                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Booking Form</h3>
<!--                    --><?php
//                    if (isset($_SESSION['message']) && $_SESSION['message'] != null) {
//                        echo '<div class="alert alert-success session_message">' . $_SESSION['message'] . '</div>';
//                        $_SESSION['message'] = null;
//
//                    }
//                    ?>
                    <form action="mailer.php" method="POST">
                        <div class="row form-group">
                            <div class="col-md-6 padding-bottom">
                                <label for="fname">First Name</label>
                                <input type="text" id="name" name="name" class="form-control"
                                       placeholder="Your firstname" required>
                            </div>
                            <div class="col-md-6">
                                <label for="lname">Last Name</label>
                                <input type="text" id="lname" name="lname" class="form-control"
                                       placeholder="Your lastname" requried>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control"
                                       placeholder="Your email address" requried>
                            </div>
                            <div class="col-md-6">
                                <label for="phone">Phone No.</label>
                                <input type="text" id="phone" name="phone" class="form-control"
                                       placeholder="Your Phone Number" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="subject">Tour</label>
                                <input type="text" id="tour" name="tour" class="form-control"
                                       value="<?php echo $x ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="country">Country</label>
                                <input type="text" id="country" name="country" class="form-control"
                                       placeholder="Your Country" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">

                                <label for="date">Check-in:</label>
                                <div class="form-field">
                                    <i class="icon icon-calendar2"></i>
                                    <input type="text" id="date" name="indate" class="form-control date"
                                           placeholder="Check-in date" requried>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="date">Check-out:</label>
                                <div class="form-field">
                                    <i class="icon icon-calendar2"></i>
                                    <input type="text" id="date" name="outdate" class="form-control date"
                                           placeholder="Check-out date" requried>
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="guests">Guest</label>
                                <div class="form-field">
                                    <i class="icon icon-arrow-down3"></i>
                                    <select name="people" id="people" class="form-control">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5+">5+</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="message">Message</label>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control"
                                          placeholder="Say something about us"></textarea requried>
                            </div>
                        </div>
                        <div class="form-group text-center">

                            <input type="submit" value="Send Message" class="btn btn-primary">
                        </div>

                    </form>
                </div>
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Contact Information</h3>
                    <div class="row contact-info-wrap">
                        <div class="col-md-3">
                            <p><span><i class="icon-location"></i></span> Thamel Kathmandu, <br></p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-phone3"></i></span> <a href="tel://1234567920">9841960370</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-paperplane"></i></span> <a href="mailto:info@yoursite.com">info@mountvisiontreks.com</a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map" class="vision-map"></div>


    <?php include('footer.php'); ?>

<!--    <script>-->
<!--        $(".session_message").fadeTo(2000,500).slideUp(500, function(){-->
<!--            $(".session_message").slideUp(500);-->
<!--        });-->
<!--    </script>-->