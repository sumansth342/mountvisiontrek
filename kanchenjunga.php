<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>LAKE RARA JOURNEY -10 Days</h1>
            <p style="font-size:17px">“Adventure at Nepal Far East around scenic hills and ridges of Kanchenjunga
                mountains range
                On least visited region of Far Eastern Nepal where you will be walking in complete wilderness
                Glorious mountain views of Eastern mountain range with Makalu and Kanchenjunga
                Walk into enchanting alpine forest covered with oaks, pine, spruce and fir tree lines
                From low warm southern country to cooler alpine hills then reaching into arctic zone
                Explore traditional Rai and Sherpa villages of Far Eastern Himalaya with its interesting culture
                Explore both sides of Kanchenjunga South and North Base Camps in one single journey
                Trekking towards World’s third highest Mt. Kanchenjunga and its massive range of peaks”

            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>'adventure around Nepal Far East within world third highest Mt. Kanchenjunga’</h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Kanchenjunga Base Camp Trekking, an adventure that leads you to visit both South and
                                    North Base Camp in one single journey where walks leads you at the end of Nepal
                                    around Far Eastern Himalaya the last and second highest mountain within the country,
                                    and world third highest Mt. Kanchenjunga.
                                    Mt. Kanchenjunga stands tall at above 8,586 m and 28,169 ft high, first ascent by
                                    British Expedition way back in 1955 using the South Base Camp route near Tseram and
                                    Oktang with Yarlung glacier.
                                    Mt. Kanchenjunga will be a perfect adventure for eager trekkers and adventurer
                                    seeking for remoteness and isolated destinations around Himalaya, where on this
                                    Kanchenjunga base camp trekking offers you with exciting and scenic moments on daily
                                    walks, on the hidden areas of Far East Himalaya.
                                    Kanchenjunga base camp trekking begins with scenic air journey to land at Nepal Far
                                    South East within warmer areas of Terai belt, facing panorama of mountains from
                                    Everest-Makalu with Kanchenjunga on flights, on landing at Bhadrapur airport where
                                    drive leads to cooler hills of Ilam town famous for its Tea and vast fields of
                                    tea-garden in the shade of mighty Kanchenjunga.
                                    As journey continues leading to a nice scenic walk on reaching at Taplejung area in
                                    Sukhetar heading past many farms and villages entering into deep alpine woods and
                                    crossing several streams into gorge and green hills, then walking first towards near
                                    Kanchenjunga South Base Camp where camp is set at Tseram for hike to Oktang to be
                                    close at Southern Face of Kanchenjunga.
                                    Adventure carries on crossing few scenic high passes Tselele to reach other sides of
                                    Kanchenjunga at Ghunsa home to many Sherpa and Bhotia people the highland tribes of
                                    upper Kanchenjunga with strong Buddhist religion and culture.
                                    After Ghunsa slowly gaining much elevation on leaving green vegetation and tree
                                    lines to reach at the highest spot of the adventure at Pang Pema within Kanchenjunga
                                    North Base Camp located above 5,065 m high facing incredible panorama of massive
                                    Kanchenjunga peaks with Mt. Jannu and Wedge peaks includes Kabru, Kirat Chuli, Nepal
                                    Peak, Taple Sikhar, and Givegela Chuli.
                                    Enjoying glorious moments at Kanchenjunga North Base Camp return journey on
                                    alternative and main trail with long descend to reach back into tree lines and farm
                                    villages within warmer areas, and the driving back to low south for sweeping flight
                                    back to Kathmandu, after a most wonderful experience and adventure on Kanchenjunga
                                    base camp trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br> Hotels and lodge with camping on walks.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous.<br>
                                    <b> Area of Trek: </b><br> Nepal Far North East within Taplejung district.<br>
                                    <b> Highest Altitude Gain: </b><br> At Pang-Pema Kanchenjunga North Base Camp 5, 414
                                    m high.<br>
                                    <b> People and Culture: </b><br> Mainly populated with Kirat Rai-Magar and Sherpa,
                                    Bhotia of
                                    varied culture and religion of Hindu and Buddhism religions.<br>
                                    <b> Trek Duration:</b><br> 20 Nights and 21 Days (Bhadrapur to Bhadrapur with
                                    drives).<br>
                                    <b> Average walks:</b><br> Minimum 5 hrs to Maximum 7 hrs.<br>
                                    <b>Total Trip: </b><br> 22 Nights and 23 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons: </b><br> April to May and September to November months.
                                    April to May nice for walks when wild-flowers are in full bloom,
                                    where most of the days are clear for views. Morning and Night
                                    time as well in shade will be cold without wind-chill factor,
                                    October to November another best months for trek when day is
                                    clear, but with short sunlight hours due to autumn season cold
                                    morning and night times with chances of snow.
                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b>Day 02: </b> Fly to Bhadrapur 350 m and drive to Ilam Town 1,360 m - 45 mins
                                    flight and 4 hrs drive for overnight stop.<br>
                                    <b>Day 03: </b> Drive to Sukhetar 1,490 m and start trekking to Lalikharka 1,900
                                    m-04 hrs.<br>
                                    <b>Day 04: </b> Trek from Kande Bhanjyang 2,100m -07 hrs.<br>
                                    <b> Day 05: </b> Trek to Phompe Danda 1,850m -06 hrs.<br>
                                    <b> Day 06: </b> Trek to Yangphudin 2,150m at Sherpa Gaon - 07 hrs.<br>
                                    <b>Day 07: </b> Trek to Dorongding 2,880 m - 07hrs (Cross Lashe Tham, 3,800m
                                    Pass).<br>
                                    <b>Day 08: </b> Trek to Tseram 3,770m - 05 hrs.<br>
                                    <b>Day 09: </b> Rest day at Tseram, Excursion to Oktang near Kanchenjunga South
                                    BC.<br>
                                    <b>Day 10: </b> Cross Tselele pass 4,250m and camp at Tselele bottom 3,700m - 08
                                    hrs.<br>
                                    <b>Day 11: </b> Trek to Ghunsa 3,480 m - 04 hrs.<br>
                                    <b>Day 12: </b> Trek to Khambachen 4,000m -06hrs.<br>
                                    <b>Day 13: </b>Trek to Lhonak 4,780m - 06 hrs.<br>
                                    <b>Day 14: </b>Hike to Pang Pema at Kanchenjunga North Base Camp 5,065m - 5 hrs
                                    round trip.<br>
                                    <b>Day 15: </b></b>    Trek to Ghunsa, 3,480 m - 08hrs.<br>
                                    <b>Day 16: </b>Trek to Gyapla Phedi 2,300m - 07hrs.<br>
                                    <b>Day 17: </b>Trek to Sekathum / Japantar 1,350m - 07 hrs.<br>
                                    <b>Day 18: </b> Trek to Chirwa 1,250 m - 06 hrs.<br>
                                    <b>Day 19: </b> Trek to Phurumbu School 1,500 m -07hrs.<br>
                                    <b>Day 20: </b> Trek to Sukhetar 2,440 m -04 hrs.<br>
                                    <b>Day 21: </b>Drive back to Ilam town or further - 05 hrs.<br>
                                    <b>Day 22: </b>Drive to Bhadrapur airport and fly to Kathmandu and transfer to
                                    hotel.<br>
                                    <b> Day 23: </b> International departure for home ward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of
                                    Kanchenjunga Base Camp Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b>Day 02: Fly to Bhadrapur 350 m and drive to Ilam Town 1,360 m - 45 mins flight and 4
                                    hrs drives for overnight stop.</b><br>
                                    Morning transfer to Kathmandu domestic airport where flight takes you to Nepal Far
                                    East
                                    Lower Terai of sub-tropical temperatures within warm flat land at Bhadrapur town
                                    airport, from
                                    here Kanchenjunga base camp adventure begins with scenic drive to reach at hill town
                                    of Ilam,
                                    famous for vast Tea-Garden, here for overnight in the hill town of Far Eastern
                                    Nepal.

                                    Ilam an interesting town with time for strolls to explore the traditional life of
                                    local hill people of eastern Nepal.<br><br>
                                    <b>Day 03: Drive to Sukhetar 1,490 m and start trekking to Lalikharka 1,900 m-04 hrs.</b><br>
                                    From Ilam town transfer to local vehicle where drive leads towards mid hills past
                                    farm villages and town around Taplejung, which is also a headquarter town of the
                                    district Taplejung within Kanchenjunga region.

                                    Drive continues on steep winding hills past farm village to reach at Sukhetar, where
                                    walk begins, Sukhetar with a small airstrip (we avoid flying in at Sukhetar airport
                                    due to unpredictable flight times and connection).

                                    From this village walk leads further east trail to reach a remote farm areas and
                                    village at Lalikharka for overnight, where camp is set near the village farm yard.<br><br>

                                    <b> Day 04: Trek from Kande Bhanjyang 2,100m -07 hrs.</b><br>
                                    Morning starts walking downhill within farm villages, mostly populated by Hindu
                                    people a mixture of Brahmins, Chettries and as well with Rai and Limbu, as well
                                    practice Hinduism and with their own ancient culture and deities worships natural
                                    spirit.

                                    After walking past village of Tembawa descending towards Phawa khola, crossing Phawa
                                    khola coming to Pokora village, the path from here leads through terraced farm
                                    fields, and finally crossing Pokora's suspension bridge our route climbs steeply to
                                    the village of Kunjari,
                                    a mountain village, from Kunjari walk leads through mixed forest with some small
                                    farm villages, and then climb to our overnight stop at Kande Bhanjyang, a small farm
                                    village with grand views of Kanchenjunga and Yalung-Khang peaks.<br><br>

                                    <b>Day 05: Trek to Phompe Danda 1,850m-06 hrs.</b><br>
                                    Today, walk leads to a steep descend towards the lush terraced rice and millet
                                    fields, then into dense forested area within headwaters of Nandeva khola towards
                                    left. On crossing stream trek continues downhill to a river, as our route enters a
                                    high hills where climbs start back into forest coming to the village of Loppoding.

                                    After Loppoding walk on ups and down-hill path to reach a mountain village of
                                    Yangpang, and then walk along upper edge of the ridge with a climb on gentle slope
                                    for an hour to Pompe Dhada at the crest of a scenic ridge, an excellent spot for
                                    overnight stop with view of Mt. Jannu.<br><br>

                                    <b>Day 06: Trek to Yangphudin 2,150m at Sherpa Gaon - 07 hrs.</b><br>
                                    From here on a winding path with downhill past hamlets and terraced farm fields to
                                    Khesewa khola, after crossing a suspension bridge as walk leads to a climbs to reach
                                    another resting spot, after a gradual ascent for an hour through terraced fields,
                                    then heading towards Mamankhe village. Afternoon leads to slow climb, skirting
                                    around a ridge to a level path that leads to Dekadin village.

                                    On this walk with grand views and then descends to stone paved steps to Kabeli
                                    khola, on leaving the river behind our route climbs to a splendid ridge through
                                    villages and farm fields.

                                    After a climb reaching an isolated village of Yamphudin from here climb to a hill
                                    for overnight stop at Sherpa Gaon. This is the last permanent human settlement
                                    inhabited by Sherpa tribe till our adventures reach the other side at Gunsa. The
                                    campsite will be in a field or in village yards.<br><br>

                                    <b>Day 07: Trek to Dorongding 2,880 m - 07hrs (Cross Lashe Tham, 3,800m Pass).</b><br>
                                    From this small Sherpa village trek leads to Chitre, walk along Kabeli khola,
                                    crossing tributary of Omje khola and start climbing steep up to Dhupi Bhangyang for
                                    two hours or more through open grassland.

                                    From the top of Lahse Tham at 3,800 m pass and then leading to a steady descent
                                    through the deciduous forest to Omje Khola.

                                    After tough climb reaching on top of a ridge, where our walks lead to Dorongding
                                    past Chitre for overnight halt in this small and scenic place with views of snow
                                    capped peaks.<br><br>

                                    <b>  Day 08: Trek to Tseram 3,770m - 05 hrs.</b><br>
                                    From the camp walk towards Ramite Bhanjyang (3,430m.), over steep uphill for few
                                    hours into magnolias, rhododendron and bamboo forest to reach at Ramite a place with
                                    single hut, from here our path leads to a climb within rhododendron tree covered
                                    ridge, passing towards a small pond on the right.

                                    From here crossing stream as walk continues reaching a gravel ground, and then walk
                                    past this area leads to a shrine with a huge boulder shaped like a serpent.

                                    Walking past forested areas our route leads to a riverbed, then ascending from
                                    riverbed path up to a terrace for overnight stop at Tseram, a place with roofless
                                    bivouac caves.
                                    Ahead views of terminal moraine fed from Yalung glacier and behind looking at Kabru
                                    7,353m and Talung peak 7,349m.<br><br>

                                    <b> Day 09: Rest day at Tseram, Excursion to Oktang near Kanchenjunga South BC.</b><br>
                                    Rest day at Tseram for day hike to Oktang which is near to Kanchenjunga South Base
                                    Camp, where British expedition established base camp for the climb of Kanchenjunga
                                    summit in 1955.

                                    Hike leads with close view of Yalung Glacier and south wall of Kanchenjunga, the
                                    hike takes for nearly three hours with views of Talung Peak and Kabru. After a
                                    scenic and exciting hike back to Tserem for overnight stop.<br><br>

                                    <b>Day 10: Cross Tselele pass 4,250m and camp at Tselele bottom 3,700m - 08 hrs.</b><br>
                                    Today, descend via Yalung Bar with fifteen minutes of steep up, crossing a river and
                                    then on gradual path through forest, and then a steep climb for an hour and half
                                    towards two small ponds. From here cross three passes, Miring La, Tamo La and Selele
                                    pass.

                                    The first part of the trail is uphill climb for 1 hour to the top of the pass with
                                    the great view of Kabru and Kanchenjunga. From Miring La, the trail goes gently down
                                    and then up to Selele La (4,250 m). From the top of the pass magnificent views of
                                    Makalu, Baruntse, and Chamlang and then descend gently to the base of the pass for
                                    overnight stop.<br><br>

                                    <b>Day 11: Trek to Ghunsa 3,480 m - 04 hrs.</b><br>
                                    The first part of the walk is on gentle down slope and then leads to a steep descend
                                    for an hour into forest of rhododendron and pines as the walk reach a level ground
                                    near Ghunsa river, from here on gradual path to Ghunsa village for overnight in a
                                    village inhabited by Bhotia tribe (Tibetan origin) with community stone houses a
                                    neat and tidy small farm.<br><br>

                                    <b>Day 12: Trek to Khambachen 4,000m -06hrs.</b><br>
                                    Walk from here leads to climb following a river with views of waterfalls along the
                                    walks, Yak herders may be met along the trail depending upon grazing season.

                                    The trail goes from right bank of river Ghunsa, and through rhododendron and pine
                                    forest, as walk leads to a gradual ascend to Rambuk Kharkas and then crossing a
                                    small glacier landslide.

                                    The trek continues with excellent views of Khumbukarna and Janu and other high snow
                                    capped peaks.

                                    On leaving tree lines behind, heading into barren country with few short bushes and
                                    scrub of juniper, after Rambuk Kharka climb, and then trail leads to a gentle down
                                    to Khambachen after crossing a landslide section to camp at Khambachen.
                                    This scenic valley dominated by peaks of Sharphu (7,070m.) with north face of Mt.
                                    Janu and Wedge peak.<br><br>

                                    <b>Day 13: Trek to Lhonak 4,780m - 06 hrs.</b><br>
                                    From here our route follows above river for an hour and then heading further east,
                                    as our walk heads down to a river, as climbs reaches rocks and boulder areas leading
                                    to a terminal moraine of Kanchenjunga glacier, on leaving Ramtang Kharka (a Sheppard
                                    place) where trail joins Kanchenjunga glacier and then crossing at a junction of
                                    Lhonak and Kanchenjunga glacier overlooking grand views of Mera, Nepal Peak and
                                    Wedge Peak.

                                    <b> Day 14: Hike to Pang Pema at Kanchenjunga North Base Camp 5,065m - 06 hrs round
                                    trip back to Lhonak - 08hrs.</b><br>
                                    Today a leisure day for hike around Pang Pema our main highlight of the adventure
                                    within North Kanchenjunga base camps, starting with gentle climb above a river with
                                    impressive views of Kirat Chuli, Nepal Peak, Taple Sikhar, and Givegela Chuli. The
                                    first part of the climbs is gentle and then heading through a boulder on rocky
                                    trail.

                                    Walking between grassy slopes and to a glacier path up at Kanchenjunga North Base
                                    Camp with chances of spotting herds of blue sheep's around this wilderness of
                                    mountains and glaciers.
                                    After a wonderful scenic moment overlooking massive Kanchenjunga peaks and then
                                    heading back to camp at Lhonak with wonderful photographs around Pang Pema and
                                    Kanchenjunga base camp.<br><br>

                                    <b> Day 15: Trek to Ghunsa, 3,480 m - 08hrs.</b><br>
                                    Retrace walking back on the same trail past Kambachen to reach camp at Ghunsa after
                                    a long good day walks.<br><br>

                                    <b> Day 16: Trek to Gyapla Phedi 2,300m - 07hrs.</b><br>
                                    From here walks leads on alternative new route and trail allows you to explore more
                                    of beautiful surrounding country facing views of mountains and then walking past
                                    several scattered cattle herders summer grazing area to reach at Gypla Phedi, around
                                    lower alpine fields for overnight camp.<br><br>

                                    <b> Day 17: Trek to Sekathum / Japantar 1,350m - 07 hrs.</b><br>
                                    With pleasant easy morning walking towards warmer areas back into farm fields and
                                    terraces within nice rural villages to reach at Sekathum or near to Japantar for
                                    overnight stop, where walks leads into much lower areas within farm villages growing
                                    much better crops and citric fruits.<br><br>

                                    <b>Day 18: Trek to Chirwa 1,250 m - 06 hrs.</b><br>
                                    Enjoy the walk heading much lower and warm areas, as morning trek progress into
                                    several scattered farm houses and fields, where you will be greeted with friendly
                                    cheerful smiles as walk leads through villages and patch of cool forest areas to
                                    reach at Chirwa for overnight stops.<br><br>

                                    <b> Day 19: Trek to Phurumbu School 1,500 m -07hrs.</b><br>
                                    As our route descends a long way, then reaching areas where walk leads to short
                                    climb within cool shade of forest, and then entering to much warmer low country
                                    within foot hills of Kanchenjunga areas, slowly walk reaches a small place to camp
                                    overnight at Phurumbu School compound.<br><br>

                                    <b>Day 20: Trek to Sukhetar 2,440 m -04 hrs.</b><br>
                                    Our final and last day walk of the adventure where route leads at Sukhetar within
                                    Tapeljung large town and villages encountering more local busy areas, as walk leads
                                    through number of smaller farm settlement all the way to Sukhetar for overnight
                                    stop.<br><br>

                                    <b>  Day 21: Drive back to Ilam town or further - 05 hrs.</b><br>
                                    From Sukhetar ending our trek and taking a drive as far to Ilam town or further to
                                    make it easier for next day flight to Kathmandu, as time permits overnight in Ilam
                                    town or further reaching low warm areas of Terai belt and large town of Bhirtamod
                                    with fine good hotels to stay.<br><br>

                                    <b>Day 22: Fly back to Kathmandu and transfer to hotel.</b><br>
                                    Morning after overnight in Ilam or at Bhirtamod town, where short drive leads to
                                    Bhadrapur air port for scenic short flight to land at Kathmandu, on reaching
                                    Kathmandu transfer to your respective hotels with free afternoon for individual
                                    activities.<br><br>
                                    <b> Day 23: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memorable experience and
                                    adventure on Kanchenjunga Base Camp, with last final day in Nepal where Mount Vision
                                    Trek staff and guide transfer you to Kathmandu international airport for your flight
                                    home ward bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>
                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>
                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=KANCHENJUNGA BASE CAMP TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=KANCHENJUNGA BASE CAMP TREKKING" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



