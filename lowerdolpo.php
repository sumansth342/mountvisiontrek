<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>LOWER DOLPO TREKKING -19 Days</h1>
            <p style="font-size:17px">“Adventure towards Nepal Far and remote wild west around Inner and Lower Dolpo
                areas
                On least visited region of western Nepal where you will be walking in complete wilderness
                Glorious mountain views of Dhaulagiri and other western mountain range on exciting walks
                Walk into enchanting alpine forest covered with oaks, pine, spruce and fir tree lines
                From low warm southern country to cooler alpine hills then reaching into arctic zone
                Explore traditional Dolpo villages the highlanders of western Himalaya with interesting cultures
                Crossing high Kang-la and other passes facing panorama of peaks and dramatic landscapes Starting and
                ending with sweeping-scenic flights to and from high country of Dolpo”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘adventure around Nepal Far West at Dolpo in the land of ancient Bon sect of pre
                                    Buddhism’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Lower Dolpo Trekking, an adventure that takes you around Nepal Far Western Himalaya
                                    in one of the wildest and most remote parts within Nepal Himalaya, in the land of
                                    snow-leopard and herd of blue-sheep, which can be spotted sometimes on route
                                    trekking around Lower and inner Dolpo area.
                                    An enjoyable walk on Lower Dolpo Trekking of little more than two week duration
                                    allows you to enjoy marvelous time within its scenic area of dramatic and fabulous
                                    scenery of lovely landscapes and mountain views along with local immense culture of
                                    Dolpo villagers.
                                    Lower Dolpo Trekking where you will be around Nepal largest national park known as
                                    Shey & Phoksumdo National Park with an area of 3,555 sq.kms that stretches towards
                                    Jumla and Mugu districts further west and close to Mustang in the east direction.
                                    Adventure leads you to enjoy rich alpine vegetations of luxuriant forest with blue
                                    pine, spruce, cypress, poplar, deodar, fir and birch. The trans-Himalayan area of
                                    Lower and Inner Dolpo with near-desert type vegetation of mainly dwarf juniper and
                                    caragna shrubs.

                                    Lower Dolpo starts with scenic flight to reach Nepal Far South West near the India
                                    border city at Nepalgunj within sub-tropical area, where next short flight takes you
                                    to land at Jhupal within inner and Lower Dolpo area.

                                    From here walking past Dunai a major town of Dolpo where route leads following
                                    Thulo-Bheri and into remote areas with nice lovely farm village to reach at our main
                                    highlight of the trip at Phoksumdo Lake, a beautiful and scenic place to visit and
                                    experience where walk leads over high Baga-la and Numa-la to reach around charming
                                    and attractive villages of Tokyu-Gaon.

                                    Exploring interesting culture of Bon and Buddhism sects then heading back to Jhupal
                                    after walking into nice and longest valley of Dolpo to reach Tarakot a nice
                                    historical village of Malla period times, and then reaching slowly back to Dunai and
                                    at Jhupal for sweeping flight to Kathmandu via Nepalgunj, after a most amazing
                                    adventure and experience on Lower Dolpo Trekking.
                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking: </b><br> Hotels and lodge with camping on walks.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous.<br>
                                    <b> Area of Trek: </b><br> Nepal Far North West within Dolpo region.<br>
                                    <b> Highest Altitude Gain: </b><br> Crossing Baga-la Pass 5,167 m and Numa-La pass
                                    5,310 m high.<br>
                                    <b> People and Culture: </b><br> Mainly populated by Dolpo and Magar hill tribes
                                    with some
                                    Hindu Thakuri Malla around low areas of Dolpo of Buddhist
                                    and ancient Bon religion with impressive culture.<br>
                                    <b> Trek Duration:</b><br> 14 Nights and 15 Days (Jhupal to Jhupal).<br>
                                    <b> Average walks:</b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip:</b><br> 18 Nights and 19 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br> April to November months April to May nice for walks when wild-
                                    flowers are in full bloom, where most of the days are clear for
                                    views. Morning and Night time as well in shade will be cold
                                    without wind-chill factor, October to November another best
                                    months for trek when day is clear, but with short sunlight hours
                                    due to autumn season cold morning and night times with chances
                                    of snow.

                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu with optional sightseeing tour and flight to Nepalgunj.<br>
                                    <b> Day 03: </b>Fly to Jhupal 2,470 m and drive to Dunai 2,230 m - 02 hrs.<br>
                                    <b> Day 04: </b> Trek to Chepka 2,835 m - 06 hrs.<br>
                                    <b>Day 05: </b> Trek to Samdua village 2,955 m -06 hrs.<br>
                                    <b>Day 06: </b> Trek to Phoksundo Lake 3,630 m - 06 hours.<br>
                                    <b> Day 07: </b> Rest day at Phoksundo Lake for local excursion.<br>
                                    <b>Day 08: </b> Trek to Temche 3,994 m near Ghuchun Khola - 05 hrs.<br>
                                    <b>Day 09: </b> Trek and cross Baga-La 5,167 m and camp at Danigar 4,512 m - 06 hrs.<br>
                                    <b> Day 10: </b> Cross Numa-La 5,310 m and camp at base (phedi) 4,440 m - 06
                                    hrs.<br>
                                    <b>Day 11: </b> Trek to Tokyu Gaon 4,200 m via Khanger - 06 hrs.<br>
                                    <b>Day 12: </b> Trek to Dho-Tarap 4,040m- 05 hrs.<br>
                                    <b> Day 13:</b> Trek to Ghymagar 3,759 m - 07 hrs - 06 hrs.<br>
                                    <b> Day 14: </b> Trek to Tarap Khola 3,652 via Chyugar 3,440 m - 06 hours.<br>
                                    <b> Day 15: </b> Trek to Tarakot 2,540 m - 06 hrs.<br>
                                    <b>Day 16: </b> Trek on route Dunai - 04 hrs and drive to Jhupal - 02 hr jeep
                                    ride.<br>
                                    <b>Day 17: </b> Fly back to Kathmandu via Nepalgunj and transfer to hotel.<br>
                                    <b>Day 18:</b> Spare and contingency days in case of flight delay at Jhupal.<br>
                                    <b>Day 19:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                        On arrival where you will be well received by our staff and guide and then
                                        transfer to your respective hotels in the centre of the city in Kathmandu, after
                                        checking into your rooms getting refreshed from jet-leg join with other members
                                        of Lower Dolpo Trekking.
                                        Our guide / leader will brief with information about the hotels-including detail
                                        of trekking regarding local lodge, walks, view, history and culture with few
                                        important do’s and don’ts while you are with us having enjoyable times.
                                        Evening includes a nice dinner in a pleasant Nepalese restaurant with local
                                        cultural programs of all parts of the country to enlighten the environment along
                                        with your dinner.<br><br>
                                    <b>Day 02: In Kathmandu with optional sightseeing tour and flight to Nepalgunj.</b><br>
                                        A half day to prepare for upcoming treks to remote areas of Lower Dolpo morning
                                        an optional sightseeing tour at places of great interest, exploring around
                                        Kathmandu world heritage sites for few hours and back at hotel, afternoon
                                        transfer to Kathmandu domestic airport for scenic flight to land at warm areas
                                        of Nepalgunj city airport.
                                        Located around Nepal Far South West for transit flight to Jhupal from Kathmandu
                                        here transfer to city hotel close to airport, for next morning early flight to
                                        Dolpo where late afternoon with short time to witness the city areas of much
                                        Indian cultures due to its closeness to India border.<br><br>
                                    <b> Day 03: Fly to Jhupal 2,470 m and drive to Dunai 2,230 m - 02 hrs.</b><br>
                                        Morning after having early breakfast transfer to airport for short scenic flight
                                        to Jhupal at Dolpo area, during flight views of Dhaulagiri and other western
                                        Himalaya, on reaching at Jhupal after 35-40 minutes flight meet our trekking and
                                        camping staff with porters.
                                        From Jhupal, after getting organized then taking a short jeep drives to reach at
                                        Dunai headquarter and administrative town of Dolpo district.
                                        In Dunai for overnight stop in the comfort of nice local lodge, afternoon walk
                                        around Dunai town and observe its interesting local culture and traditional
                                        life.<br><br>
                                    <b> Day 04: Trek to Chepka 2,835 m - 06 hrs.</b><br>
                                        With overnight in Dunai, morning walks leads past the market area and then
                                        crossing over a bridge of Thulo Bheri River, where climb takes you over a ridge
                                        to Phoksumdo River valley with views of Kagmara peak.
                                        From here walk leads to descend past farm villages with fields growing walnut
                                        trees and getting close to a stream of Dhera, a small cattle herders settlements
                                        and then short climb to Rahagaon village of Thakuri a Hindu local tribe.
                                        As walk leads to downhill path into forested area to reach our overnight camp at
                                        Chepka a small farm village.<br><br>
                                    <b> Day 05: Trek to Samdua village 2,955 m -06 hrs.</b><br>
                                        From Chepka located close to Ankhe village, where morning walk leads to short
                                        climb and then descend into forested area to reach a river bank, from here climb
                                        after leaving the forest.
                                        On reaching top of a ridge with downhill to forest leading to cross a small
                                        bridge at Ryajik village, trek further to Samdua for overnight stop near the
                                        entrance of Phoksumdo National Park.<br><br>
                                    <b> Day 06: Trek to Phoksundo Lake 3,630 m - 06 hours.</b><br>
                                        Morning walk to our first destination around Phoksumdo Lake after walking over
                                        Pungmo Kholas valley and crossing streams, where walk follows Pungmo Khola on
                                        downhill into pine and birch trees to reach upper side of Phoksumdo Khola.
                                        After a short refreshing stop continue walk to Ringmo village, an interesting
                                        Tibetan origin village adorned with mud plastered Chorten and Mani (prayer)
                                        walls.
                                        From here a short walk leads to Phoksundo Lake for overnight camp by the shore
                                        of beautiful turquoise Lake of Phoksundo.<br><br>
                                    <b> Day 07: Rest day at Phoksundo Lake for local excursion.</b><br>
                                        An ideal spot for rest and free day, here marvel the beauty of its natural
                                        surroundings with its scenic landscapes overlooking peaks of Kagmara, and
                                        explore interesting Ringmo village and witness Dolpo native culture with its
                                        age-old heritage and custom.<br><br>
                                    <b>Day 08: Trek to Temche 3,995 m near Ghuchun Khola - 05 hrs.</b><br>
                                        After an enjoyable and interesting time at Ringmo village and Phoksundo Lake,
                                        today’s walk leads to other side of Dolpo area heading east by an alternative
                                        way crossing few passes of Baga-la and Numa-la. Morning walk leads past Muduwa
                                        village and the stream of Muduwa on a valley gorge to reach a small grazing area
                                        at Temche for overnight stop near Ghuchun Khola (stream).<br><br>
                                    <b> Day 09: Cross Baga-La 5,167 m and camp at Danigar 4,512 m - 06 hrs.</b><br>
                                        Starting the morning walk on a pleasant and gradual trail and then leading
                                        uphill to cross over Baga-la at 5,169 meters high, with steep climb for about
                                        near 3 hours to reach the top of the pass, the top offers panorama of Phoksumdo
                                        lake and Kagmara peaks, after catching great views a long descend to our
                                        overnight camp at Danigar or Phedi, meaning base or a bottom of the hills and
                                        passes.<br><br>
                                    <b> Day 10: Cross Numa-La (South) 5,310 m and camp at the base (phedi) 4,440 m - 06
                                        hrs.</b><br>
                                        Morning walk leads to another pass Numa-la South at 5,309 meters, the highest
                                        spot of this adventure, climb on nice grassy slopes for about three hours then
                                        reaching at the top of Numa-la with impressive views of Dhaulagiri mountain
                                        range to Kagmara peak and looking down at beautiful valleys and surrounding
                                        dramatic landscapes, after an enjoyable moment descend to the base of this high
                                        pass, then with few hours of good walk on gradual path to reach our overnight
                                        camp on a summer pasture fields at the base or Phedi.<br><br>
                                    <b>Day 11: Trek to Tokyu Gaon 4,209 m via Khanger - 06 hrs.</b><br>
                                        From the base Numa-la, morning starts with steep climb to reach top of a small
                                        ridge with views of massive Mt. Dhaulagiri range, from here a long descend to
                                        Modo valley, time permitting visit the monastery, here the trail from Upper and
                                        Lower Dolpo joins.
                                        After Modo and Khanger few hours of good walk to Tokyu-Gaon for overnight, this
                                        is one of the most impressive and interesting village of Dolpa area, consist of
                                        more than ten villages and nice tended farm fields with old impressive
                                        monasteries.<br><br>
                                    <b>Day 12: Trek to Dho-Tarap 4,040m- 05 hrs.</b><br>
                                        From Tokyu-Gaon, morning walk on downhill to Tarap Chu on wider green valley,
                                        one of the scenic broader valleys of the area, here you will notice many
                                        Sheppard’s settlements of Yaks and Sheep, as walk continues to our overnight
                                        camp at Dho Tarap, another interesting village of mix tribes of Tibetans origin
                                        and Magar hill tribe people.<br><br>
                                    <b>Day 13: Trek to Ghymagar 3,759 m - 07 hrs - 06 hrs.</b><br>
                                        After a great stop at Dho-Tarap and its quiet nice village, morning leads to
                                        long descend towards Tarap valley, one of largest and longest valley of Dolpo
                                        area which is about 20 k.m. which stretches following Tarap Chu River most of
                                        the way to joins with Thuli Bheri River near Tarakot village, walk leads past
                                        Langa with beautiful cascading waterfalls heading due south following the trail
                                        after Sisaul and small place of Kesila around Yak herders temporary camps to
                                        reach our overnight camp at Ghymagar.<br><br>
                                    <b> Day 14: Trek to Tarap Khola 3,652 via Chyugar 3,440 m - 06 hours.</b><br>
                                        Today walk along Tarap nice valley, as the trail enter into a narrow gorge with
                                        some vegetation of bushes of juniper and wild rose, walk leads past Toltol with
                                        a large overhanging cave, around this area might see some wild life like Naur or
                                        Blue Sheep and other animals.
                                        As the walk progress reaching a confluence of River Tarap Chu and Lang Khola,
                                        overnight camp by the river after a long day walk of about 6 hours.<br><br>
                                    <b>Day 15: Trek to Tarakot 2,540 m - 06 hrs.</b><br>
                                        Morning walk starts past smaller villages and then entering into small forest
                                        area, following a river all the way to Tarakot, this is one of the important and
                                        large village of Dolpo with interesting history houses an old historical
                                        fortress (Dzong or Kot) to guard the village from outsiders in early days.
                                        Tarakot village with famous old Sandul Gomba located at the junction of Barbung
                                        Khola and Tarap Chu River where our overnight camp is set.<br><br>
                                    <b> Day 16: Trek on route Dunai - 04 hrs and drive to Jhupal - 02 hr jeep ride.</b><br>
                                        After a marvelous time at Tarakot, last day walk leads to Dunai for short while
                                        and then taking a short drive to Jhupal for the flight back to Kathmandu.
                                        Morning starts walking into spread out villages and farm huts, walk on wider
                                        path with some short uphill and downhill to reach at Thuli Bheri River valley
                                        and then at Dunai, on reaching Dunai for lunch. From here jeeps drive back to
                                        Jhupal to catch our next morning flight to Nepalgunj unto Kathmandu.<br><br>
                                    <b> Day 17: Fly back to Kathmandu via Nepalgunj and transfer to hotel.</b><br>
                                        Morning after overnight stop in a local lodge at Jhupal, transfer to Jhupal
                                        airstrip for scenic short flight to land at Nepalgunj airport, here with times
                                        to get refreshed before catching another flight back to Kathmandu. On reaching
                                        Kathmandu transfer to your respective hotels with free afternoon for individual
                                        activities.<br><br>
                                    <b> Day 18: Spare and contingency days in case of flight delay at Jhupal.</b><br>
                                        We have set this day as free as contingency with time to enjoy extra leisure day
                                        in Kathmandu after scenic trek on Inner and Lower Dolpo, this day free for
                                        individual activities and shopping souvenirs, or join in for another exciting
                                        tour around Kathmandu-Patan and Bhaktapur at places of great interest.<br><br>

                                    <b>Day 19: Depart Kathmandu for international departure homeward bound.</b><br>
                                        Finally approaching last day in Kathmandu after great memorable experience and
                                        adventure on Lower Dolpo Trekking, with last final day in Nepal where Mount
                                        Vision Trek staff and guide transfer you to Kathmandu international airport for
                                        your flight home ward bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=LOWER DOLPO TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=LOWER DOLPO TREKKING" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



