<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/gokyori1.jpg); ">


            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>GOKYO-RI EVEREST PANORAMA- 11 Days</h1>
            <p style="font-size:17px">“Marvelous walks with enjoyable mountains views to scenic Gokyo valley and its
                lakes
                Adventure on top high Gokyo-Ri facing world’s four highest mountains with Mt. Everest
                Exceptional views of Khumbu high mountains in the shade of mighty Everest
                Explore impressive Sherpa villages enriched with age-old heritage and culture
                Flying in and taking off at unique Lukla airstrip with sweeping mountain panorama
                Within World Heritage Sites of Sagarmatha National Park and its cool alpine woodland”

            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure over high Tsho-la pass to famous Everest base camp from scenic Gokyo
                                    valley’</h3>
                                <p style="font-size: 17px;text-align: justify">Gokyo-Ri Everest Panorama Trekking, where
                                    adventure leads you within photogenic and picturesque Gokyo valley and its emerald
                                    colored lakes hidden by high peaks with close views of world 6th highest Mt. Cho-Oyu
                                    8,201 meter and 26, 906 ft high, situated in between Nepal and Tibet / China.
                                    A fantastic walks on Gokyo-Ri Everest Panorama trekking where you will stand on the
                                    highest spot at above 5,357 m overlooking breathtaking panorama of worlds four
                                    highest mountains that includes Cho-Oyu, Everest, Lhotse with 5th highest Mt. Makalu
                                    towards east direction.
                                    Truly a fantastic hill top for excellent views of surrounding dramatic Khumbu
                                    landscapes with rolling hills, rocky and icy peaks with Ngozumba Glacier and
                                    simmering lakes of Gokyo down below from high Gokyo-Ri view point.
                                    Gokyo-Ri Everest Panorama, begins with sweeping short flight to land on a small
                                    airstrip at Lukla, the gateway town for many adventure around high Khumbu, where
                                    walk leads at the end of Lukla town and then downhill with pleasant winding trails
                                    into Dudh-Kosi River valley and gorge with views of snow peaks.
                                    Walking includes visiting interesting villages of Ghat-Phakding and Monjo adorned
                                    with religious monuments of Mani-Walls (prayer walls) with intricate carved Tibetan
                                    inscriptions as walk leads into cool shade of pine and rhododendron forest to reach
                                    at famed Namche Bazaar.
                                    At Namche with necessary rest days for acclimatization with sidewalks and hike to
                                    scenic hills to catch views of Everest, magnificent Amadablam with range of peaks,
                                    Namche the hub of Khumbu Sherpa where every Saturday a busy colorful market takes
                                    place.
                                    From Namche our route diverts from the main trekking trails heading towards remote
                                    and isolated corners of high Khumbu valley to reach at beautiful Gokyo valley
                                    enclosed by series of peaks where morning hike takes you on top Gokyo-Ri for
                                    incredible panorama of world highest mountains along with stunning sunrise views.
                                    After a glorious and exciting time around scenic Gokyo valley and its lakes, return
                                    journey leads all the way on downhill walks past Namche Bazaar to reach at Lukla for
                                    scenic short flight back to Kathmandu with ever-lasting memories of wonderful
                                    experience and adventure on Gokyo-Ri Everest Panorama Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br>Hotels and in lodge on walks.</br>
                                    <b>Trek Grade:</b><br>Moderate to Adventurous due to high altitude terrain</br>
                                    <b>Area of Trek:</b> <br>Nepal North Mid-East within Khumbu district of Everest</br>
                                    <b>Highest Altitude Gain: </b><br>On top Gokyo-Ri 5,357 m high.</br>
                                    <b> People and Culture:</b><br>Mainly populated by Sherpa the highlanders of
                                    Everest</br>
                                    enriched with colorful Buddhist religion and impressive culture.</br>
                                    <b>Trek Duration:</b> <br>10 Nights and 11 Days (Lukla to Lukla)</br>
                                    <b>Average walks:</b> <br>Minimum 4 hrs to Maximum 6 hrs or more.</br>
                                    <b> Total Trip:</b> <br> Nights and 15 Days Kathmandu to Kathmandu.</br>
                                    <b>Seasons:</b><br>All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.
                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: </b>Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.</br>
                                    <b>Day 02: </b>In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.</br>
                                    <b> Day 03: </b>Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m
                                    and trek
                                    to Phakding 2,655 m - 03 hrs.</br>
                                    <b>Day 04: </b>Trek to Namche Bazaar 3,440 m - 06 hrs.</br>
                                    <b>Day 05: </b>In Namche for rest to support acclimatization with short hike and
                                    excursion.</br>
                                    <b>Day 06: </b>Trek to Dole 4,100 m - 06 hrs.</br>
                                    <b>Day 07: </b>Trek to Machhermo 4,465 m - 04 hrs.</br>
                                    <b> Day 08: </b>Trek to Gokyo Lake 4,810 m - 04 hrs.</br>
                                    <b> Day 09: </b>Free day at Gokyo for hike to Gokyo-Ri 5,357 m.</br>
                                    <b> Day 10: </b>Trek back to Dole - 06 hrs.</br>
                                    <b> Day 11: </b>Trek to Namche Bazaar - 06 hrs.</br>
                                    <b> Day 12: </b>Trek to Lukla - 06 hrs.</br>
                                    <b> Day 13: </b>Fly back to Kathmandu and transfer to hotel with free afternoon at
                                    leisure.</br>
                                    <b> Day 14: </b>Reserve and contingency day in Kathmandu with individual
                                    activities.</br>
                                    <b>Day 15: </b>Depart Kathmandu for international departure homeward bound.</br>


                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b> Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of
                                    Gokyo-Ri Everest Panorama Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b>Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel.
                                    Afternoon at leisure and free for individual activities and getting ready with
                                    packing for next early morning flight to Lukla where our walk begins towards
                                    Gokyo-Ri for Everest Panorama.<br><br>
                                    <b>Day 03: Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m and
                                        trek</b><br>
                                    to Phakding 2,655 m - 03 hrs.
                                    Morning at the earliest before or after quick breakfast or one carry pack breakfast
                                    to save time reaching at Kathmandu domestic airport terminal, where a short scenic
                                    flight of 35 minutes reaches you at Lukla, a gateway Sherpa town at the height of
                                    2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage
                                    with porters to carry or on pack animals (Jopkyo a mix breed of normal cow and Yak).
                                    As walk leads past Lukla town heading down to Choplung small village and then around
                                    Dudh-Kosi River valley and gorge towards our first overnight stop at Phadking
                                    village near Dudh-Kosi a glacial river.<br><br>

                                    <b>Day 04: Trek to Namche Bazaar 3,440 m - 06 hrs.</b><br>
                                    Morning begins with gradual walk following Dudh Kosi river upstream past several
                                    Sherpa nice villages with short ups and down and then crossing a long suspension
                                    bridge to Monjo village, where Sagarmatha National Park starts entering into park
                                    areas past Jorsalle the last village before Namche Bazaar.
                                    From Jorsalle crossing bridge and into serene woodland of tall pines and
                                    rhododendron trees with climb on winding trail facing views of Kongde, Kusum
                                    Khanguru and Tharmasakru peaks as walk brings you into famous Namche Bazaar, which
                                    is more of a town rather than village with many good restaurants, hotels, lodge
                                    having number of shops and stores includes Bank and Post-Office.
                                    At Namche enjoy the comfort of nice cozy lodge, facing great views of snow capped
                                    peaks the closest lies Kongde peaks.<br><br>

                                    <b>Day 05: In Namche for rest to support acclimatization with short hike and
                                        excursion.</b><br>
                                    At Namche rest day for acclimatization before heading higher area, where short hike
                                    up to view point like towards Everest View Hotel takes nearly 3-4 hours both ways,
                                    offers excellent panorama of mountains with Everest and magnificent Amadablam
                                    In Namche visit museum where you can gain knowledge of Khumbu Sherpa, culture,
                                    flora-fauna as well of mountaineering expedition history with rest of the afternoon
                                    at leisure.<br><br>

                                    <b>Day 06: Trek to Dole 4,100 m - 06 hrs.</b><br>
                                    With pleasant time at Namche walk leads to short climb and then on good wide trail
                                    facing views of Everest and Amadablam with array of peaks, as our route leads into
                                    woodland then for a climb to reach a hill top at Mong, a small place with few lodge
                                    and shops with grand spectacular views of surround mountains.
                                    From here descend towards Dudh Kosi River to reach a small place in Phortse-Tenga,
                                    possible lunch stop in the middle of a wood.
                                    Afternoon climb into forested areas facing views of Kangtenga-Tharmasarku and other
                                    peaks as walk brings at a small wide open plateau in Dole for overnight
                                    stop.<br><br>

                                    <b> Day 07: Trek to Machhermo 4,465 m - 04 hrs.</b><br>
                                    After Dole, walk leads on high trail for an hour towards Lhabarma Kharka at 4,325 m
                                    and Luza at 4,390m; then skirt around a ridge facing views of Machherma, situated
                                    below on a small valley, walk downhill to Machherma for lunch and overnight stop.
                                    Here after lunch rest of the afternoon free for short hike with views of Mt. Cho Oyo
                                    8,210m and other mountains flanking above Ngozumba Glacier.<br><br>
                                    <b>Day 08: Trek to Gokyo Lake 4,810 m - 04 hrs.</b><br>
                                    Morning walk leads to Gokyo Lake following a scenic trail to Pangka 4,545m, then
                                    climb on rocky path into ablation valley passing first lake and then second lakes,
                                    to reach a third lake called Dudh Pokhari or Gokyo Lake.
                                    This beautiful area within Gokyo at 4,790 meters high, after lunch short walk around
                                    lake with views of Mt. Cho Oyo world’s 06th highest and Gyachung Kang that reflects
                                    on emerald Gokyo Lake.<br><br>
                                    <b>Day 09: Free day at Gokyo for hike to Gokyo-Ri 5,357 m.</b><br>
                                    A free day in Gokyo valley around its beautiful glacial lakes, where morning climb
                                    of few hours leads you to highest point of the adventure on top Gokyo Ri above 5,357
                                    m high, offers spectacular wide panorama of Mt. Cho-Oyu, Mt. Everest, Lhotse and of
                                    Mt. Makalu covering four world highest mountains at one single glance with other
                                    adjoining peaks.
                                    Enjoying views of grand peaks with massive Ngozumba Glacier and then down to Gokyo
                                    for lunch with rest of the afternoon at leisure.<br><br>
                                    <b>Day 10: Trek back to Dole - 06 hrs.</b><br>
                                    After a wonderful and exciting time around Gokyo valley and its beautiful lakes,
                                    morning easier walk downhill to reach back at Dole, as walk leads along with
                                    wonderful views and then heading backing into tree lines at Dole for overnight stop.<br><br>
                                    <b>Day 11: Trek to Namche Bazaar - 06 hrs.</b><br>
                                    On completing our memorable and exciting adventure around beautiful areas of Gokyo
                                    and on top scenic Gokyo-Ri, an easy walk from here on without worry of high altitude
                                    sickness as route leads loosing elevation every hour of walks.
                                    From Phortse-Tenga a short climb to Mong, from here on downhill walks to Namche
                                    Bazaar for last night in Khumbu before Lukla.<br><br>
                                    <b>Day 12: Trek to Lukla - 06 hrs.</b><br>
                                    Our last final day of walks a longer treks to reach back at Lukla starting with long
                                    descend to reach a river valley and at Sagarmatha National Park entrance and exit to
                                    Monjo village, where walk continues past Phakding with last short climb to reach at
                                    last overnight stay in Lukla before flying back to Kathmandu.<br><br>
                                    <b>Day 13: Fly back to Kathmandu and transfer to hotel with free afternoon at
                                        leisure.</b><br>
                                    Depending upon flight time for Kathmandu, morning with last breakfast in Khumbu and
                                    Lukla transfer to air terminal named after Tenzing and Hillary, the first to conquer
                                    Mt. Everest in 1953.
                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to
                                    reach Kathmandu airport and then transfer back to your respective hotels.<br><br>
                                    <b>Day 14: Reserve and contingency day in Kathmandu with individual
                                        activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Lukla or in the beginning, where extra day
                                    allows you with time for yourself for individual activities or join in our exclusive
                                    tour or just relax after a great adventure on high Khumbu and Everest base camp.<br><br>
                                    <b>Day 15: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and experience on
                                    Gokyo-Ri Everest Panorama trekking, with last final day in Nepal where Mount Vision
                                    Trek staff and guide transfer you to Kathmandu international airport for your flight
                                    home ward bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/gokyori1.jpg">
                                                        <img src="assets/images/gokyori1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/gokyori.jpg">
                                                        <img src="assets/images/gokyori.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">

                    <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1700</strike></strong>
                    </h3>
                    <h3>
                        <p class="price " style="color:white;"><span>$1604</span>
                            <small>/ person</small>
                        </p>
                    </h3>


                    <a href="booking.php?id=GOKYO-RI EVEREST PANORAMA " class="btn btn-primary btn-outline btn-block">Book
                        Now</a></p>
                    <a href="enquiry.php?id=GOKYO-RI EVEREST PANORAMA" class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



