<?php
require "inc/connect.inc.php";
include'navigation.php';
?>
    <aside id="vision-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(assets/images/untitled1.png); ">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h2>Mount Vision Treks & Expedition</h2>
                                    <h1>Search Activities</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>
<?php
$activities = "";
$searchParameter=$_POST['search'];
$query_run = mysqli_query($con, "SELECT * FROM tb_activities WHERE ads_title LIKE '%".$searchParameter."%'");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image=$row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];

        $activities.='<a href="activities.php?id='.$ads_id.' " class="tour-entry animate-box">
    <div class="tour-img" style="background-image: url('.$ads_image.');">
    </div>
    <span class="desc">
                        <h2>'.$ads_title.'</h2>
                        <span class="price">'.$price_range.'</span>
                    </span>
</a>';
    }
}
$everestarea = "";
$searchParameter=$_POST['search'];
$query_run = mysqli_query($con, "SELECT * FROM tb_everestarea WHERE ads_title LIKE '%".$searchParameter."%'");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image=$row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];

        $everestarea.='<a href='.$ads_url.' class="tour-entry animate-box">
    <div class="tour-img" style="background-image: url('.$ads_image.');">
    </div>
    <span class="desc">
                        <h2>'.$ads_title.'</h2>
                        <span class="price">'.$price_range.'</span>
                    </span>
</a>
';
    }
}
$annapurna = "";
$searchParameter=$_POST['search'];
$query_run = mysqli_query($con, "SELECT * FROM tb_annapurna WHERE ads_title LIKE '%".$searchParameter."%'");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image=$row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];

        $annapurna.='<a href='.$ads_url.' class="tour-entry animate-box">
    <div class="tour-img" style="background-image: url('.$ads_image.');">
    </div>
    <span class="desc">
                        <h2>'.$ads_title.'</h2>
                        <span class="price">'.$price_range.'</span>
                    </span>
</a>
         ';
    }
}
$langtang = "";
$searchParameter=$_POST['search'];
$query_run = mysqli_query($con, "SELECT * FROM tb_langtang WHERE ads_title LIKE '%".$searchParameter."%'");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image=$row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];

        $langtang.='<a href='.$ads_url.' class="tour-entry animate-box">
    <div class="tour-img" style="background-image: url('.$ads_image.');">
    </div>
    <span class="desc">
                        <h2>'.$ads_title.'</h2>
                        <span class="price">'.$price_range.'</span>
                    </span>
</a>
         ';
    }
}





?>

    <div class="vision-tour vision-light-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                    <h2>your Search Results</h2>

                </div>
            </div>
        </div>
        <div class="tour-wrap">
            <?php echo $activities;?>
            <?php echo $everestarea;?>
            <?php echo $annapurna;?>
            <?php echo $langtang;?>







        </div>
    </div>
<?php include 'footer.php';?>