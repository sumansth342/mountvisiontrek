<?php include('navigation.php');
?>
    <aside id="vision-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(assets/images/travel.jpg); ">
                    <div class="overlay"></div>

                </li>
            </ul>
        </div>
    </aside>
    <div class="vision-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="wrap-division">
                            <div class="panel">
                                <div class="panel-body">
                                    <h3>Travel Guide</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php'); ?>