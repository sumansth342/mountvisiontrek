<?php
session_start();
include('navigation.php'); ?>
<div id="page">

    <aside id="vision-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(assets/images/terms.jpg);">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <strong><h1>Contact US</h1></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>
    <div id="vision-contact">
        <div class="container">
            <div class="row">
                <?php
                if (isset($_SESSION['message']) && $_SESSION['message'] != null) {
                    echo '<div class="alert alert-success session_message">' . $_SESSION['message'] . '</div>';
                    $_SESSION['message'] = null;

                }
                ?>
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Get In Touch</h3>
                    <form action="contactmailer.php" method="POST">
                        <div class="row form-group">
                            <div class="col-md-6 padding-bottom">
                                <label for="fname">Full  Name</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Your firstname" required>
                            </div>
                            <div class="col-md-6">
                                <label for="phone">Phone No.</label>
                                <input type="text" id="phone" name="phone" class="form-control" placeholder="Your Phone Number" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Your email address" required>
                            </div>
                            <div class="col-md-6">
                                <label for="country">Country</label>
                                <input type="text" id="country" name="country" class="form-control" placeholder="Your Country" required>
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="subject">Subject</label>
                                <input type="text" id="subject" name="subject" class="form-control" placeholder="Your subject of this message" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="message">Message</label>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something about us" required></textarea>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" value="Send Message" class="btn btn-primary">
                        </div>

                    </form>
                </div>
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Contact Information</h3>
                    <div class="row contact-info-wrap">
                        <div class="col-md-3">
                            <p><span><i class="icon-location"></i></span> Thamel Kathmandu, <br> </p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-phone3"></i></span> <a href="tel://1234567920">9841960370</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-paperplane"></i></span> <a href="mailto:info@yoursite.com">info@mountvisiontreks.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include('footer.php'); ?>
<script>
    $(".session_message").fadeTo(2000,900).slideUp(900, function(){
        $(".session_message").slideUp(900);
    });
</script>
