<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>YALA PEAK CLIMB</h1>
            <p style="font-size:17px">“Yala Peak climb leads you to great adventure to its scenic top with grand views
                Located within beautiful Langtang Valley surrounded by array of mountains
                A climb for all from beginners in mountaineering to tough expert adventurer
                Wonderful walk with scenic mountain views through lovely Tamang villages
                Explore the local impressive culture around Langtang village of great interest”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘scenic and cultural walk with an adventure on top stunning Yala Peak’</h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Yala Peak Climb an interesting and striking mountain to climb where adventure leads
                                    you in the midst of beautiful Langtang valley enclosed by series of Central Himalaya
                                    peaks and in the harmony of local culture around Tamang villages.
                                    Yala Peak Climb an adventure for all types of trekkers and mountaineers from
                                    beginners to mountaineering as well to hardy tough climbers as a refreshing practice
                                    before joining in major longer expeditions.
                                    Yala Peak Climb stands at above 5,732 m / 18,806 ft high, one of lesser and below
                                    6,000 meters of all Nepal Trekking peaks, that leads you to exciting climb facing
                                    tremendous panorama of mountains that encircles you at the summit top of Yala Peak.
                                    The climb grade is just ‘F’ means Facial Easy and comes from French and Swiss Alpine
                                    Climbing Classification Systems, a mountain of straight climb with less technical
                                    effort required.
                                    However all climbers requires adequate climbing gear for safe and successful climb
                                    to its summit top and to enjoy every minute along with glorious panorama of
                                    mountains.
                                    Starts with drive from the capital Kathmandu to reach at North and Central Himalaya
                                    region around Langtang Himal range situated in Rasuwa district and home of
                                    interesting tribe known as Tamang enriched with Buddhist religion and fascinating
                                    culture.
                                    Walk leads into dense lovely forest of rhododendron-pines-oaks and bamboos within
                                    deep gorge of Langtang Khola (stream) heading past farm villages to reach a wide
                                    scenic valley of Langtang at Kyanjin.
                                    With time here for rest and acclimatization then our route leads to the bottom of
                                    Yala Peak to set our base camp in tented camps for the great adventure, with time
                                    here for some practice and to study the route of the climb.
                                    Finally making our ascent to reach at the summit of Yala Peak with vigorous climb on
                                    snow-rocks and ice on the top enjoy marvelous views of mountain range that includes
                                    all of Langtang peaks with Jugal Himal as far to Ganesh and Manaslu peaks with some
                                    mountains towards Tibet side.
                                    After a glorious experience head back to the road-head for exciting drive to
                                    Kathmandu to complete our fabulous adventure on Yala Peak Climb with great services
                                    of Mount Vision Trek staff and guides.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking: </b><br>Hotels with camping facilities on walks and climb.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous Walks.<br>
                                    <b>Climbing Grade: </b><br> F ‘Facial Easy’ comes from French / Swiss Alpine
                                    Climbing Systems.<br>
                                    <b> Area of Trek and Climb:</b><br> Nepal North Central Himalaya around Langtang
                                    Himal.
                                    People and Culture: Mainly populated by Tamang enriched with Buddhist
                                    religions with impressive culture and traditional farm
                                    life.<br>
                                    <b> Trek and Climb: </b><br>11 Nights and 12 Days (with drives both ways).<br>
                                    <b> Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b> Total Trip: </b><br> 14 Nights and 15 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons: </b><br> April to May best for trek and climb when wild-
                                    flowers in full bloom, most days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to early December another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of


                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu and transfer to hotel.<br>
                                    <b>Day 02:</b> In Kathmandu optional sightseeing tour and trek preparations.<br>
                                    <b>Day 03: </b>Drive to Syabrubesi village 1,475 m via Dhunche 1,945 m - 06 hrs.<br>
                                    <b> Day 04:</b> Trek to place called Lama Hotel 2,280 m - 06 hrs.<br>
                                    <b>Day 05: </b>Trek to Langtang Village 3, 380 m - 06 hrs.<br>
                                    <b>Day 06:</b> Trek to Kyanjin 3,750 m - 04 hrs walks.<br>
                                    <b>Day 07:</b> At Kyanjin rest day for hike with panorama of mountain range.<br>
                                    <b> Day 08:</b> Trek to Yala Peak Base Camp 4,400 m / 14,435 ft - 05 hours.<br>
                                    <b> Day 09:</b> Climb to the summit of Yala Peak 5,732 m / 18,806 ft.<br>
                                    <b>Day 10:</b> Spare and contingency day.<br>
                                    <b>Day 11:</b> Return to Kyanjin - 05 hrs.<br>
                                    <b> Day 12:</b> Trek to Lama Hotel - 06 hrs.<br>
                                    <b>Day 13:</b> Trek to Syabrubesi 1,475 m - 06 hrs.<br>
                                    <b>Day 14:</b> Drive to Kathmandu and transfer to hotel with free afternoon.<br>
                                    <b> Day 15:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: Arrival in Kathmandu and transfer to hotel.</b><br>
                                    On arrival received by our staff and guide and then transfer to your respective
                                    hotels in the centre of Kathmandu city, on checking your rooms and getting refreshed
                                    from jet-leg join with other members of Yala Peak and Langtang Valley Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, camping for few nights, with history and culture as
                                    well few important details.
                                    Evening include nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b> Day 02: In Kathmandu with optional tour and preparation for climb and trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach Langtang
                                    Himal around Nepal Central Himalaya.<br><br>
                                    <b> Day 03: Drive to Syabrubesi village 1,475 m via Dhunche town 1,945 m - 06 hrs.</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal North-West highway that
                                    connects with Langtang Himal of Nuwakot and Rasuwa district as well to the border of
                                    Tibet-China, where drive leads around warmer low areas past farm areas with villages
                                    and towns, following higher winding road as drive heads further west to reach at
                                    nice large town of Dhunche, headquarter of Rasuwa district.
                                    From here on downhill winding drive to reach our overnight stop at Syabrubesi
                                    village within home to large number of Tamang native tribe of the region.<br><br>
                                    <b> Day 04: Trek to place called Lama Hotel 2,280 m - 06 hrs.</b><br>
                                    Trek starts of the first day heading to cross a bridge over Upper Trisuli River then
                                    climb up towards a hill top with scattered farm houses and terraces, then enter into
                                    a cool shade of forest within tall rhododendron-pines-oaks and fir tree lines.
                                    As walk leads to more uphill in the forested path to reach a nice clearing where our
                                    overnight stop located in a place called Lama Hotel with few good lodges to stay.<br><br>
                                    <b> Day 05: Trek to Langtang Village 3, 380 m - 06 hrs.</b><br>
                                    From this peaceful small settlement in the midst of forest surrounding walk leads
                                    following the river upstream entering into a gorge covered within thick green
                                    vegetation and tree lines as our route leads climb to reach a nice spot of
                                    Ghoretabela, with few houses serving as lodge and tea-house.
                                    After a refreshing break walk continues into forested areas crossing few streams to
                                    reach our overnight stop in Langtang Village, an interesting place to explore around
                                    after a great walk of the day.<br><br>
                                    <b> Day 06: Trek to Kyanjin 3,750 m - 04 hrs walks.</b><br>
                                    With pleasant overnight at Langtang village, today a shortest walk of the journey
                                    where route leads to our main highlight at Kyanjin with scenic Langtang valley where
                                    our route leads to a short steep climb to enter a wide open valley to reach at
                                    Kyajin situated in the midst of Langtang valley enclosed within high peaks of
                                    Langtang Himal.
                                    At Kyanjin with time to relax inside a nice cozy lodge and explore around this
                                    scenic areas with close views of glaciers towards north fed from Mt. Langtang Lirung
                                    and other close adjoining peaks.<br><br>
                                    <b> Day 07: At Kyanjin rest day for hike with panorama of mountain range.</b><br>
                                    A great scenic spot for full rest day with options of short hike to climb on top
                                    view-point of Tsego or Kyanjin-Ri a nearby hill top rewards you with fantastic
                                    panorama of Langtang range of peaks facing Langtang Himal, Ganesh Himal (7,405 m),
                                    Langtang Lirung (7,234 m), Ghengu Liru (Langtang II 6,571m), Kimshun (6,745 m) and
                                    Shalbachum(6,918m), Chimsedang, Naya-Kanga or Ganja Chuli (5,846m), Gangchempo,
                                    Tilman's beautiful Fluted Peak, Jugal Himal and ending towards eastern horizon at
                                    Dorje Lakpa (6,980m), rest of the afternoon at leisure and short walk around this
                                    dramatic and scenic Langtang Valley.<br><br>
                                    <b> Day 08: Trek to Yala Peak Base Camp 4,400 m / 14,435 ft - 05 hours.</b><br>
                                    After Kyanjin Gompa, heading towards our main highlight and adventure towards Yala
                                    Peak base camp leading at the end of beautiful and scenic Langtang valley, walking
                                    on rocks and glacier with steep climb over icy path to reach available and
                                    reasonable flat spot to set our base camp at 4,400m.

                                    Base camp provides views of Ganchenpo, Naya Kang, Tserko Ri, Yala peak and others
                                    peaks till Yala base camp is reached for the first camp of this trip after being in
                                    the comfort of a lodge, base camp with marvelous view of high massif surrounding
                                    mountains.<br><br>

                                    <b> Day 09: Trek & Climb to Yala Peak summit above 5,732 m / 18,806 ft.</b><br>
                                    Adventure starts early morning for climb on top Yala Peak summit, adventure lead to
                                    less technical difficulties, but taking nearly eight hours to reach the top ridge
                                    the summit top and back at base camp.

                                    A strenuous tough climb leads you on the summit top at 5,732 meters / 18,806 feet
                                    high, from the top enjoy the view of super sweeping panorama of mountains and then
                                    descend safely to base camp, after an exciting and enjoyable climb on top Yala peak.<br><br>

                                    <b> Day 10: Spare and contingency day in case of bad weather.</b><br>
                                    Reserved an extra day in case of technical problem or with unfavorable bad weather
                                    condition where this day can be used relaxing after a tough climb with easy walk
                                    back to the road-head at Syabrubesi or at Dhunche town.
                                    Day 11: Return to Kyanjin -05 hrs.</b><br>
                                    On leaving Yala Peak base camp, walk back to Kyanjin following icy path steeply
                                    descends offering marvelous views of massif Himalaya, enjoying views of mountains
                                    and gently ascend to reach Kyanjin Gompa for overnight stop.<br><br>

                                    <b>Day 12: Trek to Lama Hotel - 06 hrs.</b><br>
                                    Our return journey leads on the same path allowing you with more opportunity to
                                    witness more of the country beautiful scenery and culture around Tamang farm
                                    villages, as walk enters into deep forest and river gorge with downhill to Lama
                                    Hotel for overnight stop.<br><br>
                                    <b> Day 13: Trek to Syabrubesi 1,475 m - 06 hrs.</b><br>
                                    End of our lovely walk today to reach on the road-head at Syabrubesi village, as
                                    morning starts with slow downhill walk into forest to reach a place called
                                    River-side with few huts serving as tea-house and refreshment.
                                    From here walking past forested area with long descend to reach a bridge, after
                                    crossing over on nice pleasant trail to Syabrubesi for last overnight stop around
                                    Langtang Himal region.<br><br>
                                    <b>  Day 14: Drive to Kathmandu and transfer to hotel with free afternoon.</b><br>
                                    With enjoyable time in high mountain areas of Langtang Himal where morning taking an
                                    overland journey to reach back at Kathmandu, after a wonderful time and experience
                                    on Yala Peak Climb and trek, rest of the afternoon free at leisure.<br><br>
                                    <b> Day 15: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and adventure on Yala
                                    Peak and Langtang Valley Trekking, last final day in Nepal where Mount Vision Trek
                                    staff and guide transfer you to Kathmandu international airport for your flight home
                                    ward bound.


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



