<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css" />

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">



            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>AROUND MANASLU TREKKING- 19 Days</h1>
            <p style="font-size:17px">“Trek within massive Manaslu range of peaks close to Ganesh and Annapurna Himalaya
                Adventure around Nepal Far North-West Himalaya and close on route to Tibet border
                Walking with scenic views of peaks from the start to an end of this wonderful journey
                Crossing high Larke-La with awesome panorama views of surrounding mountains
                Into beautiful hill forest covered with rhododendron-oak-magnolia and pine trees
                Explore high Manaslu impressive villages of great interesting culture and colorful custom
                On newly opened areas of Gorkha district to reach other side of Manaslu at Manang region”




            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen" >Overview</button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day Itinerary</button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include Excludes</button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘enjoy trekking around world 8th highest Mt. Manaslu within Nepal Mid-West Himalaya’</h3>
                                <p style="font-size: 17px;text-align: justify">Around Manaslu Trekking, where you can unleash yourself into pristine environment and feel fresh mountain air, and in the harmony of local native immense culture and ancient colorful heritage in the backdrop of world 8th highest Mt. Manaslu.
                                    Around Manaslu Trekking which was closed for outside world for hundreds of years been opened since last few decades, where trekkers and adventurer can view its impressive treasures of high mountains within beautiful landscapes with touch of local traditions.
                                    Mt. Manaslu stands as world’s eight highest at above 8,163 m and 26,781 ft high which was first climbed by Japanese Expedition way back in May 9th, 1956, had been a restricted areas due to its closeness with Tibetan border, a mere distance from the trail of wonderful Around Manaslu Trekking.
                                    Starts with interesting long overland journey to reach low foot hills of Nepal Mid-West Himalaya around Gorkha district, where walks follows raging Buri-Gandaki River upstream right to near source high up at Manaslu valley.
                                    Walking from warmer paddy areas to reach cooler alpine hills covered with tall rhododendron-oaks-pines and fir trees as walk progress exploring nice mountain villages of Jagat, Philm, Dyang, Lo-Gaon and Sama-Gaon the main village of upper Manaslu region with exclusive views of Mt. Manaslu North Face at close distance.
                                    Enjoying great wonderful time at Sama-Gaon having acclimatization and rest days around impressive village of Buddhism religions similar to Tibetans across the border, where walk leads right at the end of valley rim on the foot of high Larke-La pass.
                                    Crossing major highlight and tough climb of the adventure over scenic Larke-La pass with spectacular panorama of high surrounding peaks of Manaslu, Annapurna with some peaks of Nar-Phu areas, where a long downhill walk to reach at Manang district where famous Annapurna Circuit trail joins at Dharapani village.
                                    Dharapani, a lovely village located in lower Manang area, where our great adventure ‘Around Manaslu Trekking’ concludes after an exciting and interesting drives to reach back into hustle and bustle city life in Kathmandu.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p><b> Mode of Trekking:	</b><br>	Hotels and in lodge on walks.<br>
                                    <b> Trek Grade:</b><br>			Moderate to Adventurous.<br>
                                    <b> Area of Trek:	</b><br>		Nepal Far North Mid-West Himalaya around Manaslu region.<br>
                                    <b>Highest Altitude Gain:</b><br>		Crossing over Larke-La pass 5,215 m and 17, 110 ft high.<br>
                                    People and Culture:</b><br>		Higher areas populated by Manaslu people of Tibetan origin with
                                    Buddhist religion and impressive colorful culture.
                                    Lower and Mid-Hill mixed tribes of Magar-Gurung and Hindu
                                    Brahmin and Chettries with various interesting cultures.<br>
                                    <b>Trek Duration:</b><br>			15 Nights and 16 Days (with drive both ways)<br>
                                    <b>Average walks:</b><br>		Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip:</b><br>			18 Nights and 19 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br>			March to May and September to November months.<br>
                                    March to May when wild-flowers in full bloom, and day is clear
                                    for views, morning and night time as well in shade will be cold
                                    without wind-chill factor, October to November another best
                                    months where most of day is much clear, but short sunlight hours
                                    which falls in autumn and winter months will be very cold
                                    morning / night times with chances of snow sometimes.

                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p >
                                   <b> Day 01:</b><br>	Arrival in Kathmandu via respective international airline and transfer to hotel.<br>
                                    <b>Day 02:</b>	In Kathmandu with optional sightseeing tour and preparation for the trek.<br>
                                    <b>Day 03:</b>	Drive to Soti-Khola village 755 m via Arughat town 630 m - 06 hrs.<br>
                                    <b>Day 04:</b>	Trek to Machha Khola 940 m - 06 hrs.<br>
                                    <b>Day 05: </b>	Trek to Dobhan 1,075 m via Tatopani (Hot Spring) - 05 hrs.<br>
                                    <b>Day 06:</b>	Trek to Jagat 1,450 m - 05 hrs.<br>
                                    <b>Day 07: </b>	Trek to Dyang 1,885 m via Philim 1,570 m - 05 hrs.<br>
                                    <b>Day 08: </b>	Trek to Bihi Phedi 1,990 m -05 hrs.<br>
                                    <b>Day 09: </b>	Trek to Namrung 2,630 m - 05 hrs.<br>
                                    <b> Day 10:</b> 	Trek to Lo-Gaon 3,180 m - 05 hrs.<br>
                                    <b> Day 11:</b> 	Trek to Sama-Gaon 3,530 m -05 hrs.<br>
                                    <b>Day 12: </b>	At Sama-Gaon rest day for acclimatization and local excursion.<br>
                                    <b> Day 13:</b>	Trek to Samdo 3,690 m - 05 hrs.<br>
                                    <b> Day 14:</b> 	Trek to Larke Phedi / Dharama-Shala 4,470 m - 04 hrs.<br>
                                    <b> Day 15: </b>	Cross Larke La 5,215 m / 17, 110 ft and trek to Bhimthang 3,785 m -07 hrs.<br>
                                    <b>Day 16: </b>	Trek to Gho 2,690 m - 05 hrs.<br>
                                    <b>Day 17:</b> 	Trek to Dharapani 1,915 m -05 hrs.<br>
                                    <b>Day 18:</b>	Drive back to Kathmandu and transfer to hotel with free afternoon.<br>
                                    <b> Day 19:</b>	Depart Kathmandu for international departure homeward bound.<br>



                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent"style="background-color: whitesmoke">
                                <p><b>Day 01:	Arrival in Kathmandu via respective international airline and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer to your respective hotels in the centre of the city in Kathmandu, after checking into your rooms getting refreshed from jet-leg join with other members of Around Manaslu Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of trekking regarding local lodge, walks, view, history and culture with few important do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural programs of all parts of the
                                    country to enlighten the environment along with your dinner.<br><br>

                                    <b>Day 02:	In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Manaslu, morning an optional sightseeing tour at places
                                    of great interest, exploring around Kathmandu world heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach Nepal Mid-West for Manaslu trekking.<br><br>

                                    <b> Day 03:	Drive to Soti-Khola village 755 m via Arughat town 630 m - 06 hrs.</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal North Mid-West highway that connects to Dhading and Gorkha district, where drive leads around warmer low areas past farm areas with villages and towns, following on winding road as drive heads further west to reach at nice large town in Arughat within Gorkha district.
                                    From here following Buri-Gandaki River on winding road d to reach our overnight stop at Soti-Khola village within warm farm area.<br><br>
                                    <b>Day 04: 	Trek to Machha Khola 940 m - 06 hrs.</b><br>
                                    Trek starts of the first day heading into forest walking past smaller villages then reaching a wide valley opposite of a large tributary stream that runs into Burigandaki River.
                                    Nice views of cultivated terrace of Lapbesi upper village as walk leads downhill to a wide, sandy riverbed following a path that meanders below a steep, craggy valley side.
                                    As walk progress leading to a climb towards Machha-Khola for overnight stop, a village with number
                                    of teashops with small and simple lodge where Maccha Khola means a Fish Stream.<br><br>
                                    <b> Day 05: 	Trek to Dobhan 1,075 m via Tatopani (Hot Spring) - 05 hrs.</b><br>
                                    From here onward encountering many interesting farm villages and neat cultivated fields as  vegetation changes slowly due to altitude and climate wise, as our morning walk leads to cross over a stream where Machha Khola (Fish stream) name comes from, and then walk upstream towards Khola-Bensi village with Tatopani a place with natural 'hot spring'.
                                    As walk continues where valley becomes much narrow and steeper, where our trail switches to left bank of the river
                                    and then crossing a suspension bridge, after a short period of walk entering patch of forest and then reaching at Doban located on
                                    the high shelf of Burigandaki River.<br><br>
                                    <b>Day 06:	Trek to Jagat 1,450 - 05 hrs.</b><br>
                                    Morning walk leads across a broad gravel field towards Lauri stream and then crossing suspension bridge which leads to a climb on high trails and then descending into open valley of Yara Khola, where our journey continues on for another hour to the village of Jagat.
                                    Jagat a nice looking farm village of moderate size after Arughat; people here with mix race Magar, Chettries,
                                    Brahman, Manaslu Bhotia (Tibetan looking tribes) and some Newar people a lodge owner and shopkeepers.<br><br>
                                    <b> Day 07: 	Trek to Dyang 1,885 m via Philim 1,570 m - 05 hrs.</b><br>
                                    From Jagat morning walks down-hill within a long stone steps to reach a river, where path
                                    climbs on terraced hillside at Saguleri, with distance view of Sringi Himal 7,187m, walk continue
                                    with ups and down as route reach a bridge over Buri-Gandaki.
                                    After crossing over a bridge where our trail winds up to reach at Philim village for short break and to observe this nice farm village with rich cultivated fields and terraces.
                                    After a refreshing stop at Philim, walk from here leads higher past scattered farm villages with climb through terraces of golden fields of wheat’s and barley,  to reach a rocky ridge top covered within bamboo forest area towards Eklai Bhatti means a lonely Inn, at present few more shops has been come up.

                                    From here rest of the walk leads into higher Manaslu area, where our route divert toward North West to reach our overnight stop at Dyang a small nice village of Tibetan influence in culture and way of life.<br><br>

                                    <b>Day 08: 	Trek to Bihi Phedi 1,990 m -05 hrs.</b><br>
                                    Morning walk lead into sheer cliff walls, after entering into dramatic areas our route continue on much better path to reach at a farm village, where houses and terrains completely changes of more Tibetan form Dyang onward.
                                    After a short good rest a few hours walk leads you at Bihi a small village for overnight stop, here people are more of Tibetan origin with ancient Buddhist culture.
                                    Villagers of the area grows buck wheat, wheat, barley and potatoes as staple crops, since trekking
                                    gain popular around the area, spinach, carrots and cabbages are also grown.<br><br>

                                    <b> Day 09: 	Trek to Namrung 2,630 m - 05 hrs.</b><br>
                                    From here where you can feel much cooler air, as morning walk continues for two hours on a steep sided valley, coming across Mani prayer stone walls as walk leads at Ghap a nice village with stop for possible lunch break.
                                    After Ghap walk altitude gains entering into alpine and coniferous woodland where path meanders with steep up as valley opens out with extensive pastures and then crossing a large stream flowing from the glacier of Lidana.
                                    On walk grand views of Manaslu with native name as Kutang (Manaslu) Himal range with Pang Phuchin 6,335m, Saula Himal 6,235m with many unnamed peaks of above 6000m.
                                    Walk from here with gradual uphill as altitude gains at slow pace as walk leads
                                    past farm village to reach at Namrung for overnight stop, the village also called Sho Namru.<br><br>
                                    <b> Day 10: 	Trek to Lo-Gaon 3,180 m - 05 hrs.</b><br>
                                    After a pleasant overnight stop at Namrung, morning walk continues around spread out farm fields and Yak herder huts and shelters, on reaching within high Manaslu valley where locals much engaged in farming during summer, where young folks in winter travels to big cities of Nepal and India for trade and to escape harsh cold winter.
                                    From here walk leads to high and scenic valley all the way to Lo-Gaon a nice village
                                    for overnight stop, one of attractive villages around Manaslu region, at Lo enjoy close views of towering
                                    Manaslu North Face, around Lo visit its monastery with numerous chhorten and Mani walls with views of Ganesh Himal towards east.<br><br>
                                    <b> Day 11: 	Trek to Sama-Gaon 3,530 m -05 hrs.</b><br>
                                    Enjoying time at Lo-Gaon morning walk follows on much easier but high altitude gain as route leads to a slow and gradual climb with constant views of mountain with Peak 29, after few hour climb reaching into a small forested area at Shala village with excellent view of Phungi, Manaslu and Himal Chuli.
                                    Walking for few hours further where day ends at Sama-Gaon village, with grand views of
                                    Mt. Manaslu, Sama-Gaon a large village within high Manaslu Valley, where natives are mostly farmers cultivating buck wheat, barley which are the main crops in this high and harsh terrain, potatoes and some high and cold vegetables are also grown. Villagers around high Manaslu valley descended from South West Tibet way back more than 400 years of history where custom and culture of ancient Buddhist heritage similar to its origin Tibetan predecessor.
                                    The local villagers are still active trading and bartering across the border with Tibet taking
                                    few days to reach the frontier of Nepal and Tibet, as this region falls within Old Trans
                                    Himalayan Salt Trade route between two countries.<br><br>
                                    <b>Day 12: 	At Sama-Gaon rest day for acclimatization and local excursion.</b><br>
                                    Today having a free day to support acclimatization before heading towards high Larke-La pas, here with to explore around Sama-Gaon with houses
                                    adorned with similar style of Tibet with grand views of peaks, where one can visit this village old Monastery of Tibetan Buddhism.<br><br>
                                    <b>Day 13: 	Trek to Samdo 3,690 m - 05 hrs.</b><br>
                                    After a marvelous time and rest-day in Sama-Gaon, where walk leads to four hour or more to reach the last village within Gorkha and Manaslu region at Samdo, morning walk begins with slow climb as elevation slowly gains within dry and arid country, taking slowly with short break to enjoy surrounding beautiful views of peaks and dramatic landscapes.
                                    As walk continues then reaching at Samdo village with short climb to a small flat plateau where this last village of the region located,
                                    a moderate size village with more than fifty houses where nearly two hundred villagers lives around this high harsh and dry country.<br><br>
                                    <b>Day 14: 	Trek to Larke Phedi / Dharama-Shala 4,470 m - 04 hrs.</b><br>
                                    On leaving last village of Manaslu valley, walk continues to reach at the end of Manaslu area
                                    facing views of snow capped peaks includes Mt. Manaslu also known as Kutang in local
                                    languages of Tibetan origin, which is world’s 8th highest at 8,163 m.

                                    Walk continues crossing few icy streams to reach at Larke Phedi means bottom of the pass,
                                    also called Dharmasala (travelers rest house / shade), where in old days which was also known
                                    as Larke Bazaar, (where trade and bartering use to take place between merchant-traders and
                                    locals of Gorkha-Manaslu-Manang people and Tibetans on this remote isolated spot).

                                    At Larke Phedi afternoon free for short walks and preparations for next early morning long
                                    climb and descend crossing high Larke-La pass to reach Manang areas.<br><br>

                                    <b> Day 15: 	Cross Larke La 5,215 m / 17, 110 ft and trek to Bhimthang 3,785 m -07 hrs.</b><br>
                                    After rest with overnight at Larke Phedi, an early morning start walk leads to a climb past few
                                    huts, walking over moraine of rocks and boulders in snow conditions can be tough job to find
                                    the right trail.<br><br>

                                    Walk continues past ablation of north Larke glacier, facing views of Cho Danda with Larke Peak, as walk continues with gradual climb gets much steeper to reach top of Larke-la festooned with thousands of colorful Buddhist prayer flags.
                                    From the top excellent panorama of Himlung, Cheo Himal, Kang Guru and Annapurna II include series of Manaslu peaks.
                                    From here long stretch of downhill walks to valley at a place called Larcia, further hour walks on moraine ridge to Tambuche at 3,900m at the bottom of the pass, and then a short walk leads to Bhimthang for overnight stop after a long, tough day walks.
                                    Bhimthang a lovely place located on a wide green valley with number of lodges adorned with prayer Mani walls, the area
                                    surrounded by green tall trees of rhododendron-pines and oaks.<br><br>

                                    <b>Day 16: 	Trek to Gho 2,690 m - 05 hrs.</b><br>
                                    From Bhimthang rest of the journey much easier on downhill path with few short ups
                                    on route to Gho towards Dharapani village, morning views of peaks then on gradual walk into
                                    dense alpine woodland.
                                    Walking into beautiful forested areas to reach a small clearing in the midst of woodland at Gho
                                    for overnight stop, a small cattle and yak herders summer pasture and temporary settlement
                                    due to trekkers passing this route, where few lodge has been built to cater the needs of
                                    travelers.<br><br>

                                    <b> Day 17: 	Trek to Dharapani 1,915 m -05 hrs.</b><br>
                                    Today, last day walk of this great adventure Around Manaslu treks where morning
                                    starts on gradual path with downhill into cool shade of forest to reach at Telje a nice village
                                    situated by raging Marysangdi River, here after a short rest then heading for a last climb of the
                                    journey crossing over a bridge to reach a lovely Dharapani. A large spread-out village located at
                                    the meeting point of famous Annapurna Circuit trail and of Manaslu route.

                                    Dharapani located in a river gorge within lovely tree lines where motor road has reached since
                                    last few years, this village provides excellent lodges for travelers.<br><br>

                                    <b>Day 18:	Drive to Kathmandu and transfer to hotel.</b><br>
                                    With enjoyable time in high mountains of Manaslu within North Mid-West Himalaya where morning takes
                                        you back to Kathmandu from Dharapani, as drive leads towards warmer areas of Lamjung district at Besisar town and then on good road
                                        towards Kathmandu, after a wonderful time and experience on Around Manaslu trekking with rest of the afternoon free at leisure.<br><br>
                                    <b>Day 19:	Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after a great adventure and wonderful memories of Around Manaslu Trekking,
                                        with your last final day in Nepal where Mount Vision Trek staff and guide transfer you to
                                        Kathmandu international airport for your flight home ward bound.<br><br>

                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg" style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg" style="height:200px;width: 200px;" />
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent"style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1" >
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box" >
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1500</strike></strong></h3>
                        <h3> <p class="price "style="color:white;"><span>$1250</span> <small>/ person</small></p></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png"  style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>   </center><br>



                    <a href="booking.php?id=AROUND MANASLU TREKKING" class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=AROUND MANASLU TREKKING"class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {font-family: Arial;}

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    function showImage(src){
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>



