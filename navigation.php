<?php
$current_url = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE HTML>
<html>
<head>
    <style>

     body{

                
  overflow-x: hidden;
  overflow-y: scroll;
}
                           
 
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mount Vision Treks & Expedition</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link rel="icon" href="assets/images/lodo2.png" type="images/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="assets/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="assets/css/flexslider.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <!-- Date Picker -->
    <link rel="stylesheet" href="assets/css/bootstrap-datepicker.css">
    <!-- Flaticons  -->
    <link rel="stylesheet" href="assets/fonts/flaticon/font/flaticon.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizr JS -->
    <script src="assets/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->

</head>


<div class="vision-loader"></div>

<div id="page">
    <nav class="vision-nav " role="navigation" >
        <div class="top-menu" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="vision-logo"><a href="index.php">Mount Vision</a></div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li <?php echo($current_url=="index.php")?'class="active"':''?>><a href="index.php">Home</a></li>
                            <li<?php echo($current_url=="company.php")?'class="active"':''?>><a href="company.php">Company</a></li>
                            <li class="has-dropdown" >
                                <a >SightSeeing</a>
                                <ul class="dropdown">
                                    <li><a href="#">Kathmandu Durbar Square</a></li>
                                    <li><a href="#">Patan Durbar Square</a></li>
                                    <li><a href="#">bhaktapur durbar square </a></li>
                                    <li><a href="#">kritipur </a></li>
                                    <li><a href="#">dakshinkali</a></li>
                                </ul>
                            </li>
                            <li class="has-dropdown">
                                <a>cultural & piligrams visit</a>
                                <ul class="dropdown">
                                    <li><a href="#">pasupati</a></li>
                                    <li><a href="#">swayambhu</a></li>
                                    <li><a href="#">bouddhanath</a></li>
                                    <li><a href="#">machhendranath temple</a></li>
                                </ul>
                            </li>
                            <li class="has-dropdown">
                                <a>Everest Area</a>
                                <ul class="dropdown">
                                    <li><a href="#">Everest Base Camp</a></li>
                                    <li><a href="#">Thyangboche Everest panorama </a></li>
                                    <li><a href="#">Gokyo Lake</a></li>
                                    <li><a href="#">Classic Everest Base Camp Trek</a></li>
                                    <li><a href="#">Everest Three Pass Trek</a></li>
                                    <li><a href="#">Everest View Trek</a></li>
                                    <li><a href="#">Everest Luxury Trek</a></li>
                                </ul>
                            </li>
                            <li class="has-dropdown">
                                <a>Annapurna Area</a>
                                <ul class="dropdown">
                                    <li><a href="#">Annapurna Base Camp</a></li>
                                    <li><a href="#">Annapurna Circuit Trek</a></li>
                                    <li><a href="#">Poon Hill Trek</a></li>
                                    <li><a href="#">Poon Hill- Annapurna Base Camp</a></li>
                                    <li><a href="#">Poon Hill- Annapurna Base Camp (Phedi)</a></li>
                                    <li><a href="#">Poon Hill -Muktinath Trek</a></li>
                                    <li><a href="#">Poon Hill -Phedi Trek</a></li>
                                    <li><a href="#">Mardi Himal Trek</a></li>
                                    <li><a href="#">Tilicho Trek</a></li>
                                    <li><a href="#">Nar-Phu trek</a></li>

                                </ul>
                            </li>
                            <li class="has-dropdown">
                                <a >langtang Area</a>
                                <ul class="dropdown">
                                    <li><a href="#">Langtang Valley Trekking</a></li>
                                    <li><a href="#">Langtang-Gosaikunda Trekking</a></li>
                                    <li><a href="#">Gosaikunda Trekking</a></li>
                                </ul>
                            </li>
                            <li<?php echo($current_url=="blog.php")?'class="active"':''?>><a href="blog.php">Blogs</a></li>
                            <li<?php echo($current_url=="review.php")?'class="active"':''?>><a href="review.php">Reviews</a></li>
                            <li<?php echo($current_url=="contact.php")?'class="active"':''?>><a href="contact.php">Contact</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <script src="assets/js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Waypointassets/s -->
    <script src="assets/js/jquery.waypoints.min.js"></script>
    <!-- Flexslider -->
    <script src="assets/js/jquery.flexslider-min.js"></script>
    <!-- Owl carousel -->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!-- Magnific Popup -->
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/magnific-popup-options.js"></script>
    <!-- Date Picker -->
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <!-- Stellar Parallax -->
    <script src="assets/js/jquery.stellar.min.js"></script>
    <!-- Main -->
    <script src="assets/js/main.js"></script>
    