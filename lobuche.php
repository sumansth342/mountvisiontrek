<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>LOBUCHE EAST PEAK CLIMB</h1>
            <p style="font-size:17px">“Lobuche East Peak a stunning looking peaks close to towering Mt. Everest
                One of the technical challenging peak to climb leads you to great experience
                Less climbed peak due to its tough section on route to the summit of Lobuche
                Approachable climb facing technical skill makes you feel in high spirits on the top
                A great adventure for all types of climbers leads you to exciting challenge
                Within high and scenic Khumbu valley with grand panorama of Mt. Everest
                Explore impressive Buddhist culture visiting Sherpa villages and monasteries”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘classic climb on striking Lobuche East Peak facing incredible panorama’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Lobuche East Peak Climb leads you at above 6,119 m and 20,070 ft high, near to its
                                    sister west Lobuche peak with closest views of mighty Mt. Everest that towers
                                    towards North-East direction from the summit of Lobuche East.
                                    Lobuche East Peak Climb truly a great striking peak gaining popular for hardy and
                                    tough mountaineers and as well for beginners in climb as this peak leads to tough
                                    ascent to reach the top facing exciting technical challenge where climbers feels in
                                    high spirit after a successful climb with great experience of a lifetime adventure.
                                    Lobuche East Peak Climb where the grade as TD a ‘Technical Difficule’ peak the
                                    climbing systems from French and Swiss Alpine Climbing Classification Systems which
                                    is widely used on this alpine style climb.
                                    A perfect peak for trekkers and mountaineers located on high Khumbu valley just
                                    south west of Everest and east of Mt. Cho-Oyu, where climb leads you to stunning
                                    panorama that keeps you breathless looking across array of world highest mountains
                                    as far to Makalu towards east horizon.
                                    We have designed the climb along with nature of this tough peak, allowing climbers
                                    with enough days to prepare as well having spare and free days to acclimatize
                                    includes practice climb before getting ready for the Big Haul on top Lobuche East
                                    Peak Climb.
                                    To make Lobuche East Peak Climb a great success we have included Everest Base Camp
                                    trekking with climb on top famous high Kalapathar Hill, which will add more
                                    experience and practice for Lobuche East Peak Climb.
                                    Where trek to Lobuche East Peak climb begins and ends at Lukla taking a scenic short
                                    flight in and out of Kathmandu and walking into lovely areas of Phadking-Namche
                                    Bazaar with beautiful Thyangboche Monastery.
                                    Exploring interesting village enriched with impressive cultures of Sherpa people in
                                    the harmony of pristine surrounding of world highest mountains on Lobuche East Peak
                                    Climb.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br> Hotels with camping facilities on walks and climb.<br>
                                    <b> Trek Grade: </b><br> Adventurous Walks.<br>
                                    <b> Climbing Grade:</b><br> TD ‘Technical Difficule’ a French / Swiss Alpine
                                    Climbing
                                    Systems.<br>
                                    <b> Area of Trek: </b><br> Nepal North Mid-East within Khumbu district of
                                    Everest.<br>
                                    <b> People and Culture:</b><br> Populated by Sherpa the highlanders of Everest
                                    enriched
                                    with colorful Buddhist religion and impressive culture.<br>
                                    <b> Trek and Climb: </b><br> 15 Nights and 16 Days (Lukla to Lukla).<br>
                                    <b> Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b> Total Trip: </b><br> 19 Nights and 20 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons: </b><br> April to May is good for trek and climb when wild-
                                    flowers in full bloom, where most of the days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to November another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of
                                    snow.

                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu and transfer to hotel.<br>
                                    <b>Day 02:</b> In Kathmandu preparation for the trek and climb.<br>
                                    <b> Day 03: </b>Flight to Lukla 2,818 m and trek to Phakding 2,655 m - 04 hrs.<br>
                                    <b> Day 04:</b> Trek to Namche Bazaar 3,440 m - 06 hrs.<br>
                                    <b> Day 05:</b> In Namche for rest to support acclimatization with short hike.<br>
                                    <b>Day 06:</b> Trek to Thyangboche 3,867 m - 05 hrs.<br>
                                    <b>Day 07:</b> Trek to Dingboche 4,365 m - 05 hrs.<br>
                                    <b>Day 08: </b>Another rest day at Dingboche for acclimatization and local hike.<br>
                                    <b>Day 09:</b> Trek to Lobuche 4,890 m - 05 hrs.<br>
                                    <b>Day 10:</b> Trek to Everest Base Camp 5,364 m back at Gorakshep 5,175 m.<br>
                                    <b>Day 11:</b> Climb Kalapathar 5,545 m walk to Lobuche base 4,865 m - 05 hrs.<br>
                                    <b>Day 12: </b> Trek and climb to Lobuche High Camp 5,600 m - 03 hrs.<br>
                                    <b>Day 13: </b> Climb Lobuche Peak at 6,119 m and descend to base camp - 08 hrs.<br>
                                    <b>Day 14: </b> Reserve and contingency day in case of unfavorable weather.<br>
                                    <b> Day 15:</b> Trek to Pangboche 3,900 m - 05 hrs.<br>
                                    <b>Day 16:</b> Trek to Namche Bazaar - 06 hrs.<br>
                                    <b>Day 17:</b> Trek to Lukla - 06 hrs.<br>
                                    <b>Day 18:</b> Fly back to Kathmandu and transfer to hotel.<br>
                                    <b>Day 19:</b> Reserve and contingency day in Kathmandu with individual
                                    activities.<br>
                                    <b>Day 20:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: Arrival in Kathmandu and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of Lobuche
                                    East Peak Climb.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks and climb local culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b> Day 02: In Kathmandu preparation for the trek and climb.</b><br>
                                    A free day preparation for Lobuche East Peak climb and treks around high Khumbu,
                                    with optional tour at places around Kathmandu world heritage sites of few hours,
                                    allows you getting ready with packing for next early morning flight to Lukla where
                                    our walk begins towards Lobuche East Peak Climb.<br><br>
                                    <b> Day 03: Flight to Lukla 2,818 m and trek to Phakding 2,655 m - 04 hrs.</b><br>
                                    Morning at the earliest before or after quick breakfast or one carry pack breakfast
                                    to save time reaching at Kathmandu domestic airport terminal, where a short scenic
                                    flight of 35 minutes reaches you at Lukla, a gateway Sherpa town at the height of
                                    2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage
                                    with porters to carry or on pack animals (Jopkyo a mix breed of normal cow and Yak).
                                    As walk leads past Lukla town heading down to Choplung small village and then around
                                    Dudh-Kosi River valley and gorge towards our first overnight stop at Phadking
                                    village near Dudh-Kosi a glacial river.<br><br>
                                    <b> Day 04: Trek to Namche Bazaar 3,440 m - 06 hrs.</b><br>
                                    Morning begins with gradual walk following Dudh Kosi river upstream past several
                                    Sherpa nice villages with short ups and down and then crossing a long suspension
                                    bridge to Monjo village, where Sagarmatha National Park starts entering into park
                                    areas past Jorsalle the last village before Namche Bazaar.
                                    From Jorsalle crossing bridge and into serene woodland of tall pines and
                                    rhododendron trees with climb on winding trail facing views of Kongde, Kusum
                                    Khanguru and Tharmasakru peaks as walk brings you into famous Namche Bazaar, which
                                    is more of a town rather than village with many good restaurants, hotels, lodge
                                    having number of shops and stores includes Bank and Post-Office.
                                    At Namche enjoy the comfort of nice cozy lodge, facing great views of snow capped
                                    peaks the closest lies Kongde peaks.<br><br>

                                    <b>Day 05: In Namche for rest to support acclimatization with short hike.</b><br>
                                    At Namche rest day for acclimatization before heading higher area, where short hike
                                    up to view point like towards Everest View Hotel takes nearly 3-4 hours both ways,
                                    offers excellent panorama of mountains with Everest and magnificent Amadablam
                                    In Namche visit museum where you can gain knowledge of Khumbu Sherpa, culture,
                                    flora-fauna as well of mountaineering expedition history with rest of the afternoon
                                    at leisure.<br><br>
                                    <b>Day 06: Trek to Thyangboche 3,867 m - 05 hrs.</b><br>
                                    With pleasant time at Namche walk leads to short climb and then on good wide trail
                                    facing views of Everest and Amadablam with array of peaks, as our route leads into
                                    woodland then descend towards Imjatse River to reach at a small place in
                                    Phunke-Tenga, possible lunch stop in the middle of a wood.
                                    Afternoon climb into forested areas facing views of Kangtenga-Tharmasarku and other
                                    peaks as walk brings at a wide open plateau in Thyangboche with its beautiful
                                    colorful Buddhist monastery, with time to visit the interior of the monastery where
                                    you can witness monks and priest on religious activities with prayer.
                                    A scenic spot surrounded by high peaks with views of Everest and magnificent looking
                                    Amadablam amidst forest of rhododendron-magnolia-oaks-juniper-birch and fir tree
                                    lines, truly a great place for overnight stop.<br><br>
                                    <b>Day 07: Trek to Dingboche 4,350m -06 hrs</b><br>
                                    With pleasant morning catching views of Everest head off to Debuche to cross a
                                    bridge over Imja Khola, walk leads through Mani (prayer) stones with views of
                                    Amadablam, and then trek further to reach at Dingboche with fantastic view of
                                    Northern Face of
                                    Amadablam. Dingboche located at scenic Imjatse wide valley with views of
                                    Lhotse-Nuptse with Island Peak also known as Imjatse peak.<br><br>
                                    <b>Day 08: Rest day at Dingboche for acclimatization and with short hike.</b><br>
                                    A necessary rest day in Dingboche for proper acclimatization with stunning views of
                                    Amadablam, Lhotse, Makalu, Cholatse, and Taboche and array of peaks rising at the
                                    head of Imjatse valley, Dingboche a popular stop for trekkers with miles of stone
                                    walls guarding the crops of barley and wheat from grazing animals and cold winds.<br><br>
                                    <b> Day 09: Trek to Lobuche 4,910m - 05 hrs.</b><br>
                                    After a hearty breakfast, walk follows to a higher ridge and then on gradual trail
                                    reaching at Thugla often spelled as Dughla, where massive Khumbu melts into a raging
                                    icy river.
                                    From Thugla with rest heading steep up to a lateral moraine and stone memorials and
                                    which heads off to Lobuche that has number of nice lodges, on the walk close-up view
                                    of Lobuche Peaks, Khumbutse, Lingtren, and Pumori within Mahalangur Himal range.<br><br>
                                    <b>Day 10: Trek to Everest Base Camp 5,365 m and at Gorakshep 5,145m.</b><br>
                                    Trail from Lobuche to Gorakshep leads further to reach over moraine of Khumbu
                                    Glacier, as the walk passes through glass pyramid sign post, with north ridge views
                                    of Everest, including Mt. Pumori, Mt. Mahalangur, Mt. Lingtern, Mt. Khumbutse,
                                    Mt. Nuptse and then reaching at Gorakshep, which was the original base camp of
                                    Everest in early days till 1970’s.
                                    After short rest stop walk leads further east on moraine above Khumbu glacier to
                                    reach at Everest Base Camp. Here enjoy the views of notorious Khumbu Ice Falls and
                                    glaciers with adjoin high peaks, in peak climbing expedition season this spot will
                                    be flooded with camping tents of all colors.
                                    After a great time achieving the highlight and the goal of the adventure return back
                                    to Gorakshep for overnight stop this will be the highest stop at 5,145 m high.<br><br>
                                    <b> Day 11: Hike to Kalapathar 5,545 m and at Lobuche base camp 4,865 m.</b><br>
                                    Morning a steep climb up to Kalapathar for great views of surrounding high peaks where you can
                                    witness Everest at close distance, this will be the highest spot of the adventure at
                                    5,545 m high. After catching the beautiful panorama of mountains descend back to
                                    Gorakshep and walk to Imja Khola Valley; on the land affected by the glacier
                                    moraines, and in the midst of mountains.
                                    During pleasant walk views of high snow capped mountain with majestic Amadablam and
                                    then walk to our main highlight and goal of the adventure towards Lobuche Peak East
                                    base camp.<br><br>
                                    <b>  Day 12: Climb to Lobuche High Camp 5,600m / 18,368 ft - 04 hrs.</b><br>
                                    Starting with gradual climb to set high camp, climbing almost four hours to reach
                                    our overnight and last camp for the climb, walk leads over rocky moraine path to
                                    reach High Camp, afternoon with final preparation for the climb, while our guide
                                    will scout the route of climbing and fixing ropes where necessary for next day
                                    ascent.<br><br>

                                    <b> Day 13: Climb the summit 6,119m / 20,070 ft and to Base Camp - 08 hrs.</b><br>
                                    The climb to the summit leads to steep haul; while our guide will fix rope where
                                    necessary on this tough technical ascent to reach the eastern summit of Lobuche
                                    peak, after a final push to the top with mind blowing panorama of mountains and
                                    valleys, certainly makes you feel on top of the world in high spirit.
                                    Enjoy view of Mt. Everest 8,848 m, Lhotse 8,501 m, Mt. Amadablam 6,856 m,
                                    Mt. Makalu 8, 463 m, Chamlang and Baruntse peaks in the far distance, after an
                                    adventure of life time experience descend on same route to base camp.<br><br>
                                    <b> Day 14: Reserve and Contingency day in case of bad weather.</b><br>
                                    Spare day as contingency in case of bad weather conditions, as Himalayan weather
                                    patterns sometimes changes even in good season, if all goes well using the extra day
                                    on the way back to Lukla with easier and short days on walks.<br><br>
                                    <b> Day 15: Trek to Pangboche 3,900 m - 05 hrs.</b><br>
                                    All climbers at base camp to clean up garbage and hand over to designated place or
                                    to SPPC personal (Sagarmatha Pollution Project Control) as well packing gears for
                                    Yaks to carry onward to Lukla. After getting organized our return route leads
                                    towards Imjatse valley with scenic views as walk proceeds towards Chukung to reach
                                    at Dingboche for lunch break, and then carry on trekking downhill to Pangboche
                                    village for overnight halt.<br><br>
                                    <b> Day 16: Trek to Namche Bazaar - 06 hrs.</b><br>
                                    On completing our memorable and exciting adventure Everest base camp and on top
                                    Kalapathar, an easy walk from here on without worry of high altitude sickness as
                                    route leads loosing elevation every hour of walks.

                                    From Pangboche reaching with a climb to beautiful Thyangboche and its colorful
                                    monastery reaching back into green woodland, from here a lovely walks of downs and
                                    ups and on nice scenic trail to Namche Bazaar for last night in Khumbu before Lukla.<br><br>

                                    <b>Day 17: Trek to Lukla - 06 hrs.</b><br>
                                    Our last final day of walks a longer treks to reach back at Lukla starting with long
                                    descend to reach a river valley and at Sagarmatha National Park entrance and exit to
                                    Monjo village, where walk continues past Phakding with last short climb to reach at
                                    last overnight stay in Lukla before flying back to Kathmandu.<br><br>

                                    <b> Day 18: Fly back to Kathmandu and transfer to hotel.</b><br>
                                    Depending upon flight time for Kathmandu, morning with last breakfast in Khumbu and
                                    Lukla transfer to air terminal named after Tenzing and Hillary, the first to conquer
                                    Mt. Everest in 1953.

                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to
                                    reach Kathmandu airport and then transfer back to your respective hotels.<br><br>

                                    <b> Day 19: Reserve and contingency day in Kathmandu for individual activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Lukla or in the beginning, where extra day
                                    allows you with time for yourself for individual activities or join in our exclusive
                                    tour or just relax after a great adventure on high Khumbu and Everest base camp.<br><br>

                                    <b>Day 20: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and experience on
                                    Lobuche East Peak Climb with last final day in Nepal where Mount Vision Trek staff
                                    and guide transfer you to Kathmandu international airport for your flight home ward
                                    bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



