<?php include('navigation.php');
?>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/kds.jpg); ">
                <div class="overlay"></div>

            </li>
        </ul>
    </div>
</aside>

<div class="vision-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="wrap-division">
                        <div class="panel">
                            <div class="panel-body">
                                <style>
                                    * {box-sizing: border-box}

                                    /* Set height of body and the document to 100% */
                                    body, html {
                                        height: 100%;
                                        margin: 0;
                                        font-family: Arial;
                                    }

                                    /* Style tab links */
                                    .tablink {
                                        background-color: whitesmoke;
                                        color: black;
                                        float: left;
                                        border: none;
                                        outline: none;
                                        cursor: pointer;
                                        padding: 14px 16px;
                                        font-size: 17px;
                                        width: 25%;
                                    }

                                    .tablink:hover {
                                        background-color: white;
                                    }

                                    /* Style the tab content (and add height:100% for full page content) */
                                    .tabcontent {
                                        color:black ;
                                        display: none;
                                        padding: 100px 20px;
                                        height: 100%;
                                    }
                                </style>
                                <button class="tablink" onclick="openPage('about', this, 'red')"id="defaultOpen">About Us</button>
                                <button class="tablink" onclick="openPage('teams', this, 'red')">Teams</button>
                                <button class="tablink" onclick="openPage('legal', this, 'red')">Legal Documents</button>
                                <button class="tablink" onclick="openPage('faq', this, 'red')">FAQS</button>
                                <div id="about" class="tabcontent">
                                    <h3>‘for boundless adventure and scenic cultural holidays’</h3>
                                    <p style="font-size: larger" >Welcome to Mount Vision Treks & Expeditions, introducing our company based in Nepal within commercial hub and capital of Nepal in Kathmandu.
                                        Mount Vision Treks & Expeditions a company of difference to other hundreds of local agents run and managed by expert staff and guides with more than decades of experience in tourism fields, running successful trips all around Nepal and adjoining Himalayan countries (Tibet-Bhutan and Northern India).
                                        Mount Vision Treks & Expeditions started less than ten years to provide one of the best quality services with money worth holidays services within Himalayan countries, offering wide range of trekking-climbing to cultural and historical tour around exotic destinations with fabulous and enjoyable times with us.
                                        Although a fresh and rising company of Nepal Himalaya where staff and guides are veteran and professional with decades of experience serving clients with best and high quality services in ever trip of ours that makes Mount Vision Treks & Expeditions one of the leading and renowned local company with well satisfied clients on completing trips with us.
                                        The specialty of Mount Vision Treks & Expeditions lies on dedicated and happy staff and guides making sure that every trip is a winner with great enjoyable treks and tours that we offer, where our guides are well versed in Eco and Responsible Tourism including great knowledge of flora-fauna, history, culture and much details to offer.
                                        Mount Vision Treks & Expeditions where Mr. Bharat Kumar Sherestha plans and operates the company managing every details for clients comforts, who himself has led countless of treks to various Himalayan destination across Nepal and neighboring countries.
                                        Besides managing the company as a successful tour operator, Mount Vision Treks & Expeditions also looks after needy rural farm hill villages raising charity to fund local schools for poor children’s education with educational materials, help and support to villagers to maintain hygiene and in saving its pristine environment and culture from fading.
                                        When you join with us you are not just walking in the shade of massive Himalaya peaks as well giving something worth to villagers on route trekking, where we provide volunteer programs also along with our wide range of treks and tours within Himalaya.
                                    </p>
                                </div>
                                <div id="teams" class="tabcontent">
                                    <p>
                                        <style>
                                            .card {
                                                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                                                transition: 0.3s;
                                                width: 30%;
                                            }

                                            .card:hover {
                                                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                                            }

                                            .container {
                                                padding: 2px 16px;
                                            }
                                        </style>
                                        <h2>Teams</h2>

                                        <div class="card">
                                            <img src="assets/images/suman.jpg" alt="Avatar" style="width:100%">
                                            <div class="container">
                                                <h4><b>Suman shrestha</b></h4>
                                    <p>Web Developer</p>
                                </div>
                            </div></p>
                                </div>
                                <div id="legal" class="tabcontent">
                                    <h3>Legal Documents</h3>
                                    <p></p>
                                </div>
                                <div id="faq" class="tabcontent">
                                    <h3>FAQS</h3>
                                    <p>
                                    <div class="container">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
                                                        <div class="right-arrow pull-right">+</div>
                                                        <a href="#">Collapsible Group 1</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse1" class="panel-collapse collapse">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
                                                        <div class="right-arrow pull-right">+</div>
                                                        <a href="#">Collapsible Group 2</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse2" class="panel-collapse collapse">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                        <div class="right-arrow pull-right">+</div>
                                                        <a href="#">Collapsible Group 3</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse3" class="panel-collapse collapse">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </p>
                                </div>
                                <script>
                                    function openPage(pageName,elmnt,color) {
                                        var i, tabcontent, tablinks;
                                        tabcontent = document.getElementsByClassName("tabcontent");
                                        for (i = 0; i < tabcontent.length; i++) {
                                            tabcontent[i].style.display = "none";
                                        }
                                        tablinks = document.getElementsByClassName("tablink");
                                        for (i = 0; i < tablinks.length; i++) {
                                            tablinks[i].style.backgroundColor = "";
                                        }
                                        document.getElementById(pageName).style.display = "block";
                                        elmnt.style.backgroundColor = color;
                                    }

                                    // Get the element with id="defaultOpen" and click on it
                                    document.getElementById("defaultOpen").click();
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>