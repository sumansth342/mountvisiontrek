<?php include('navigation.php');
require 'inc/connect.inc.php';
$slider = "";
$query_run = mysqli_query($con, "SELECT * FROM tb_slider");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['slider_title'];
        $ads_image = $row['ads_image'];
        $description = $row['slider_description'];

        $slider .= '<li style="background-image: url(' . $ads_image . ');">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                <img style=" width: 200px; height: 200px;margin-left:220px;margin-bottom:22px;float:middle;" src="assets/images/lodo2.png">
                                    <h2>' . $ads_title . '</h2>
                                    <h1>' . $description . '</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>';
    }
}
?>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <?php echo $slider ?>

        </ul>
    </div>
</aside>

<!-- <End of Slider> -->
<div id="vision-reservation">
    <!-- <div class="container"> -->
    <div class="row">
        <div class="search-wrap">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#flight"><i class="flaticon-plane"></i> Search Your
                            Trips</a></li>
                    <li><a data-toggle="tab" href="#hotel"><i class="flaticon-resort"></i> Customize Your Trips</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div id="flight" class="tab-pane fade in active">
                    <form method="post" class="vision-form" action="search.php">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-3">
                                <div class="form-group">
                                    <label for="date">Search:</label>
                                    <div class="form-field">
                                        <input type="text" id="location" class="form-control"
                                               placeholder="Search Location" name="search">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" name="submit" id="submit" value="Search"
                                       class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </form>
                </div>
                <div id="hotel" class="tab-pane fade">
                    <form method="post" class="vision-form">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-4">
                                <div class="form-group">
                                    <label for="date">Customize your trips:</label>
                                    <div class="form-field"
                                    <button><a href="customize.php" class="btn btn-primary btn-block">customize</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>

<br><br>
<div class="container">
    <div class="row">
        <div class="col-sm-12 vision-heading">
            <h1 style="text-align:center; font-family: " Quicksand", Arial, sans-serif"><b><h2>Welcome</h2></b></h1>
            <!-- <img src="images/page1_img1.jpg" alt="" class="img_inner fleft"> -->

            <p style="text-align:center; font-size:20px; font-family: " Quicksand", Arial, sans-serif">Welcome to Mount
            Vision Treks & Expeditions, introducing our company based in Nepal within commercial hub and capital of
            Nepal in Kathmandu.
            When you join with us you are not just walking in the shade of massive Himalaya peaks as well giving
            something worth to villagers on route trekking, where we provide volunteer programs also along with our wide
            range of treks and tours within Himalaya.
            </p>
        </div>

    </div>

</div>


<?php
$activities = "";
$query_run = mysqli_query($con, "SELECT * FROM tb_activities LIMIT 8");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image = $row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];
        $ads_disc = $row['ads_disc'];

        $activities .= '<a href="activities.php?id=' . $ads_id . '" class="tour-entry animate-box">
    <div class="tour-img" style="background-image: url(' . $ads_image . ');">
    </div>
    <span class="desc">
						<h2>' . $ads_title . '</h2>
						<span class="price">' . $price_range . '</span>
					</span>
</a>';
    }
}
?>


<div class="vision-tour vision-light-grey" style="padding-top: 1px;padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Popular Activities </h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries
                    Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
    </div>
    <div class="tour-wrap">
        <?php echo $activities; ?>
    </div>
</div>
<div class="vision-tour" style="padding-top: 1px;padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Quick Refreshment</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries
                    Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="f-tour">
                    <div class="row row-pb-md">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/sakhu.jpg);">
                                        <div class="desc">
                                            <h3>Sakhu-Nagarkot</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/nagarkot.jpg);">
                                        <div class="desc">
                                            <h3>ChaghuNarayan-Nagarkot</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/siva.jpg);">
                                        <div class="desc">
                                            <h3>Shivapuri National Park</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/chandra.jpg);">
                                        <div class="desc">
                                            <h3>Chandra Giri View Tower</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box">
                            <div class="desc">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Hiking</h3>
                                        <p>.</p><br>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="f-tour">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                            <div class="row">
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/dhampus.jpg);">
                                        <div class="desc">
                                            <h3>Chisopani-Nagarkot Trek -3 Days</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/tour-6.jpg);">
                                        <div class="desc">
                                            <h3>Dhampus-Sarangkot Trek - 3 Days</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/tour-7.jpg);">
                                        <div class="desc">
                                            <h3>Dhampus-Australian Camp -3 Days</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 animate-box">
                                    <a href="#" class="f-tour-img"
                                       style="background-image: url(assets/images/tour-8.jpg);">
                                        <div class="desc">
                                            <h3>Panchase trek -3 Days</h3>
                                            <p class="price"><span>$120</span>
                                                <small>/ person</small>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 animate-box col-md-pull-6 text-right">
                            <div class="desc">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Short Trekking</h3>
                                        <p>.</p><br>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Recommendation starts-->
<style>

    .text-block {
        position: absolute;
        font-size: 25px;
        top: 5px;
        right: -2px;
        background-color: red;
        font-weight: bold;
        color: white;
        padding-left: 10px;
        padding-right: 10px;
    }
</style>

<?php
$offers = "";
$query_run = mysqli_query($con, "SELECT * FROM tb_offers ");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $ads_title = $row['ads_title'];
        $ads_image = $row['ads_image'];
        $price_range = $row['price_range'];
        $ads_url = $row['ads_url'];
        $ads_disc = $row['ads_disc'];
        $discount = $row['discount'];


        $offers .= '<div class="item">
                    <div class="hotel-entry">
                       <a href=' . $ads_url . '  class="hotel-img" style="background-image:url(' . $ads_image . ');">
                            <div class="text-block">' . $discount . '</div>

                             <p class="price"><span>' . $price_range . '</span><small> /night</small></p>
                            </a>
                            <div class="desc">
                             <h3><a href=' . $ads_url . '>' . $ads_title . '</a></h3>
                             <p>' . $ads_disc . '</p>
                           
                            </div>
                            </div>
                            </div>';
    }
}
?>


<div id="vision-hotel" style="padding-top: 1px;padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Seasonal Offers </h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries
                    Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">

                    <?php echo $offers ?>
                </div>
            </div>
        </div>

    </div>
</div>


<div id="vision-intro" class="intro-img" style="background-image: url(assets/images/cover-img-1.jpg);"
     data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 animate-box">
                <div class="intro-desc">
                    <div class="text-salebox">
                        <div class="text-lefts">
                            <div class="sale-box">
                                <div class="sale-box-top">
                                    <h2 class="number">10</h2>
                                    <span class="sup-1">%</span>
                                    <span class="sup-2">Off</span>
                                </div>
                                <h2 class="text-sale">Opening offer !</h2>
                            </div>
                        </div>
                        <div class="text-rights">
                            <h3 class="title">Just hurry up limited offer!</h3>
                            <p>MOUNT VISION TREKS & EXPEDITIONS:<br>
                                "For Boundless Adventure & Scenic Cultural Holidays"
                            </p>
                            <p><a href="enquiry.php?id=Video Enquiry" class="btn btn-primary">Enquiry Now</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 animate-box">
                <div class="video-wrap">
                    <div class="video vision-video" style="background-image: url(assets/images/vdo2.jpg);">
                        <a href="assets/videos/vdo.mp4" class="popup-vimeo"><i class="icon-video"></i></a>
                        <div class="video-overlay"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <Happy Guests> -->
<?php
//$guests = "";
//$query_run = mysqli_query($con, "SELECT * FROM guests LIMIT 8");
//$ads_count = mysqli_num_rows($query_run);
//
//if ($ads_count > 0) {
//    while ($row = mysqli_fetch_array($query_run)) {
//
//        $ads_id = $row['id'];
//        $name = $row['name'];
//        $image=$row['image'];
//        $location = $row['location'];
//        $saying=$row['saying'];
//        $guests.='<div class="item">
//                        <div class="testimony text-center">
//                            <span class="img-user" style="background-image: url('.$image.');"></span>
//                            <span class="user">'.$name.'</span>
//                            <small>'.$location.'</small>
//                            <blockquote>
//                                <p>'.$saying.'</p>
//                            </blockquote>
//                        </div>
//                    </div>';
//    }
//}
//?>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .checked {
        color: orange;
    }
</style>
<?php
$guests = "";
$query_run = mysqli_query($con, "SELECT * FROM tb_review LIMIT 8");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $name = $row['name'];
        $image = $row['image'];
        $saying = $row['saying'];
        $location = $row['location'];
        $star = $row['star'];
        $guests .= '<div class="item">
                        <div class="testimony text-center">
                            <span class="img-user" style="background-image: url(' . $image . ');"></span>
                            <span class="user">' . $name . '</span>';
        for ($i = 0; $i < $star; $i++) {
            $guests .= '<span class="fa fa-star checked"></span>';
        }
        for ($i = 0; $i < 5 - $star; $i++) {
            $guests .= '<span class="fa fa-star"></span>';
        }


        $guests .= '<blockquote>
                                <p>' . $saying . '</p>
                                <p>' . $location . '</p>
                            </blockquote>
                        </div>
                    </div>';
    }
}

?>

<div id="vision-testimony" class="vision-light-grey" style="padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Our Satisfied Guests says</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries
                    Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 animate-box">
                <div class="owl-carousel2">
                    <?php echo $guests ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <blog> -->
<?php
$blog = "";
$query_run = mysqli_query($con, "SELECT * FROM blog ORDER BY id DESC LIMIT 4");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $title = $row['title'];
        $image = $row['image'];
        $date = $row['date'];
        $place = $row['place'];

        $blog .= '<div class="col-md-12 animate-box">
                        <a href="details_blog.php?id=' . $ads_id . '" class="blog-post">
                            <span class="img" style="background-image: url(' . $image . ');"></span>
                            <div class="desc">
                                <span class="date">' . $date . '</span>
                                <h3>' . $title . '</h3>
                                <span class="cat">' . $place . '</span>
                            </div>
                        </a>
                    </div>';
    }
}
?>
<div id="vision-blog" style="padding-top: 1px;padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Recent Blog</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries
                    Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        <div class="blog-flex">
            <div class="col-md-6">
                <article class="animate-box">
                    <div class="blog-img" style="background-image: url(assets/images/fewa.jpg);"></div>
                    <div class="desc">
                        <div class="meta">
                            <p>
                                <span>May 08, 2019</span>
                                <span>Pokhara  </span>

                            </p>
                        </div>
                        <h2><a href="details_blog1.php">A Best Place for your Holiday</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, qui quod ipsum harum id,
                            minima nesciunt! Saepe soluta, vitae quas expedita voluptatem voluptates placeat numquam
                            provident quis, atque nisi iure?</p>
                    </div>
                </article>
            </div>
            <div class="blog-entry aside-stretch-right">
                <div class="row">

                    <?php echo $blog ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>





