
           <div class="header">
               <h4>Blogs</h4>
               <p class="category">Information is here</p>
             </br>
           </div>
            <div class="col-md-3">
            <a href="insert-blog.php" class="btn btn-danger">Insert Here</a><br><br>
          </div>
           <div class="content table-responsive table-full-width">
            <div class="table-responsive">

             <table class="table table-responsive table-bordered " style="overflow-y: auto; height:300px; width: 100%; display: block">

               <thead>
                 <tr>
                   <th>Id</th>
                   <th>Title</th>
                    <th>Date</th>
                   <th>Location</th>
                   <th>Description</th>
                   <th>Image</th>
                   <th>Delete</th>
                   <th>Edit</th>
               </tr>
           </thead>
           <tbody>
             <?php 
             include "connect.ini.php";
             $sql = "SELECT * FROM blog order by id DESC";
             $result = mysqli_query($con, $sql);

             if (mysqli_num_rows($result) > 0) {
                                    // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                 ?>
                 <tr>
                   <td><?php echo $row['id']; ?></td>
                   <td><?php echo $row['title']; ?></td>
                   <td><?php echo $row['date']; ?></td>
                   <td><?php echo $row['place']; ?></td>
                   <td><?php echo $row['description']; ?></td>

                <td><img src="../<?php echo $row['image']; ?>" width="80" /></td>

                   
                   
               
                   
                   

                   <td><a href="deleteblog.php?action_about=delete_about&id=<?php echo $row["id"]; ?>" class="fa fa-trash-o" style="font-size:30px;color:red"></a></td>
                
                   <td><a href="edit-blog.php?id=<?php echo $row['id']; ?>" class="fa fa-edit" style="font-size:30px;color:red"></a></td>
               </tr>

                       <?php 

           }
       }
       ?>
       </tbody>
       </table>
    </div>
  </div>
