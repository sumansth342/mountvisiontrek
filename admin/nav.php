<ul class="nav">
        <li class="active">
            <a href="dashboard.php">
                <i class="ti-panel"></i>
                <p>Dashboard/Home</p>
            </a>
        </li>
        <li>
            <a href="blog.php">
                <i class="ti-comments"></i>
                <p>Blogs</p>
            </a>
        </li>
        <li>
            <a href="review.php">
                <i class="ti-user"></i>
                <p>Guests Review</p>
            </a>
        </li>
        <li>
            <a href="activities.php">
                <i class="ti-pencil-alt2"></i>
                <p>Activities</p>
            </a>
        </li>
        <li>
            <a href="offers.php">
                <i class="ti-desktop"></i>
                <p>Offers</p>
            </a>
        </li>

        <li>
            <a href="slider.php">
                <i class="ti-id-badge"></i>
                <p>Slider</p>
            </a>
        </li>
                                                <li>
                    <a href="user.php">
                        <i class="ti-user"></i>
                        <p>Admin Profile</p>
                    </a>
                </li>
       


    </ul>
</div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand" href="#">Dashboard</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                </li>
                  <li>
                    <a href="index.php">
                        <i class="ti-settings"></i>
                        <!-- class="fa">&#xf08e; -->
                        <p>Logout</p>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</nav>