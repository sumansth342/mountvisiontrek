<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>AROUND MASSIVE GANESH HIMAL TREKKING -17 Days</h1>
            <p style="font-size:17px">“Trek within massive Ganesh Himal views to be seen from the capital Kathmandu also
                Adventure around Nepal Far North-West Himalaya and close on route to Tibet border
                Walking with scenic views of peaks from the start to an end of this wonderful journey
                Crossing high Pansang Bhanjyang with awesome panorama views of surrounding mountains
                Into beautiful hill forest covered with rhododendron-oak-magnolia and pine trees
                Explore high Ganesh Himal impressive villages of great interesting culture and colorful custom
                On least visited trail away from main flow of trekkers into hidden pockets of Central Himalaya”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘adventure around Ganesh Himal between Mt. Manaslu and Langtang Himal
                                    Mountains’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Around Ganesh Himal Trekking, a great way to enjoy perfect wilderness of high
                                    mountains within remote traditional farm villages of great interest, where few
                                    trekkers often venture around this remote North Mid-West Himalaya.
                                    Around Ganesh Himal Trekking will be a perfect adventure for people bored with
                                    popular areas of mainstream trekking trails, where on this route where you will be
                                    in complete wilderness within pristine natural surroundings of massive Ganesh Himal
                                    and enchanting forest.
                                    Ganesh Himal region offers more than mountains where you will be walking into serene
                                    woodland of tall rhododendron-pines-oaks and some bamboo groves where rare and
                                    endangered Red Panda and other wild-life are often spotted on the marvelous walks.
                                    Walking into nice green meadows in the midst of forest and crossing high Pansang-la
                                    Bhanjyang ridge with mind-blowing views of towering peaks of Ganesh-Langtang-Manaslu
                                    and as far towards Annapurna in the west direction.
                                    Adventure leads you away from human settlement and villages for some days heading
                                    into complete wilderness in the harmony of mountains and high green hills within
                                    lovely woodland and then reaching to other side of Ganesh Himal near Langtang Himal
                                    within warmer areas of Trisuli town for drive back to Kathmandu after a great
                                    exciting and magnificent experience Around Ganesh Himal Trekking.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking: </b><br> Hotels and lodge with camping.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous.<br>
                                    <b> Area of Trek: </b><br> Nepal Far North Mid-West Himalaya around Ganesh Himal
                                    region.<br>
                                    <b> Highest Altitude Gain: </b><br> Crossing over Pansang Bhanjyang 3,850 m / 12,705
                                    ft high.<br>
                                    <b> People and Culture: </b><br> Populated by Tamang people of Tibetan origin with
                                    Buddhist religion and impressive colorful culture.
                                    Lower and Mid-Hill mixed tribes of Magar-Gurung and Hindu
                                    Brahmin and Chettries with various interesting cultures.<br>
                                    <b> Trek Duration: </b><br> 13 Nights and 14 Days (with drive both ways)<br>
                                    <b> Average walks: </b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip: </b><br> 16 Nights and 17 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br> March to May and September to November months.
                                    March to May when wild-flowers in full bloom, and day is clear
                                    for views, morning and night time as well in shade will be cold
                                    without wind-chill factor, October to November another best
                                    months where most of day is much clear, but short sunlight hours
                                    which falls in autumn and winter months will be very cold
                                    morning / night times with chances of snow sometimes.


                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b>Day 02:</b> In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b>Day 03:</b> Drive to Arughat town 630 m - 06 hrs.<br>
                                    <b>Day 04:</b> Trek to Manbu 1,300 m - 06 hrs.<br>
                                    <b>Day 05: </b>Trek to Lamo Dhunga 2,200 m - 06 hrs.<br>
                                    <b>Day 06: </b>Trek to Nauban Kharka 2,750 m - 06 hrs.<br>
                                    <b>Day 07: </b>Trek to Khading 2, 570 m - 05 hrs.<br>
                                    <b>Day 08:</b> Trek to Timila school 1,760 m ahead Tirigaon - 06 hrs.<br>
                                    <b>Day 09:</b> Trek to Chalisa 1920 m - 05 hrs.<br>
                                    <b>Day 10:</b> Trek to Marmelung Kharka 3,200 m - 06 hrs.<br>
                                    <b>Day 11:</b> Trek to Pangsang La 3,850 meters - 04 hrs.<br>
                                    <b>Day 12:</b> Rest day for short hike and exploration.<br>
                                    <b>Day 13: </b> Trek to Rupchet 3,045 m - 06 hrs.<br>
                                    <b>Day 14: </b> Trek to Saline 2,500m - 05 hrs.<br>
                                    <b>Day 15: </b> Trek to Deurali 1,500m - 05 hrs.<br>
                                    <b>Day 16: </b> Trek to Trisuli Bazaar and drive back to Kathmandu.<br>
                                    <b> Day 17:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Around
                                    Ganesh Himal Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b>Day 02: In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Manaslu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach Nepal
                                    Mid-West for Ganesh Himal trekking.<br><br>
                                    <b> Day 03: Drive to Arughat town 630 m - 06 hrs.</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal North Mid-West highway
                                    that connects to Dhading and Gorkha district, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following on winding road as drive
                                    heads further west to reach at nice large town in Arughat within Gorkha district for
                                    first overnight accommodation in best available lodge.<br><br>
                                    <b>Day 04: Trek to Manbu 1,300 m - 06 hrs.</b><br>
                                    Trek starts of the first day heading into forest walking past smaller villages then
                                    reaching a wide
                                    valley opposite of a large tributary stream that runs into Burigandaki River.
                                    Walking on gradual trail to reach at Arkhet village located at 1,200 m high, a nice
                                    farm village, where our route leads to cross over raging Buri Gandaki River, fed
                                    from the glacier of Manaslu and Ganesh Himal.
                                    As our route follows around Dhading area towards Manbu village taking short uphill
                                    with few bends as walk leads for two hours.
                                    Walk leads past Bahun-Gaon with climb to Manbu village for overnight stop facing
                                    view of Himal Chuli and Boudha Himal.<br><br>
                                    <b>Day 05: Trek to Lamo Dhunga 2,200 m - 06 hrs.</b><br>
                                    From Manbu our walk takes to Dhunchet on a nice gradual path for near two hours and
                                    then climb leads to many smaller villages and into forest then uphill for an hour to
                                    reach at Dhunchet for lunch, as walk continues with a climb for an hour to Lama
                                    Dhunga for overnight camp.<br><br>
                                    <b>Day 06: Trek to Nauban Kharka 2,750 m - 06 hrs.</b><br>
                                    After a pleasant overnight in Lama Dhunga, morning walk into forested area after
                                    leaving villages and then crossing over streams with gradual walk for few hours that
                                    leads to a climb at Nuaban Kharka for overnight camp facing views of Himal Chuli and
                                    Boudha Himal of Manaslu mountain range.<br><br>
                                    <b>Day 07: Trek to Yarsa via Khading 2, 570 m - 05 hrs.</b><br>
                                    Morning starts crossing over Nuaban Bhanjyang for an hour with views of snowcapped
                                    peaks, and then heading downhill to Khading after walk of 3 hours overlooking Ganesh
                                    Himal peaks Ganesh III and Ganesh Himal IV. After Khading walk for an hour then
                                    descend to Yarsa for overnight camp.<br><br>
                                    <b>Day 08: Trek to Timila school 1,760 m ahead Tirigaon - 06 hrs.</b><br>
                                    From this small village with great views of peaks, as walk leads for an hour with
                                    steep uphill to Kupchet and then downhill to Lapche, to cross a small stream to
                                    Kapor Gaon, after a nice stop walk continues to Timila school premises which is
                                    close distance from Tirigagoan village for overnight camp.<br><br>
                                    <b>Day 09: Trek to Chalisa 1920 m - 05 hrs.</b><br>
                                    After this small farm village, morning walk follows to Tirigaon which is about two
                                    hours of moderate walk passing a place with Tatopani ‘hot spring’. Here a pleasant
                                    stop to enjoy the hot spring.
                                    With refreshing break walk continues on winding road for an hour to Dudari Khola and
                                    then climb high up to Chalisa for overnight camp, offering grand vista of landscapes
                                    with snow capped mountains.<br><br>
                                    <b>Day 10: Trek to Marmelung Kharka 3,200 m – 06 hrs.</b><br>
                                    Morning with time visiting an old monastery, and then walk downhill to Tipling River
                                    valley gorge, and over a bridge with steep uphill to Tipling for two hours climb
                                    till Marmelung Kharka is reached for overnight stop.<br><br>
                                    <b> Day 11: Trek to Pangsang La 3,850 meters – 04 hrs.</b><br>
                                    With marvelous time at Marmelung Kharka, morning walk leads to a climb into forested
                                    ridge all the way at Pansang Bhanjyang which is also a high pass at 3,850 m / 12,705
                                    ft high.
                                    At the top offers superb panorama of Ganesh Himal, Manaslu and as far towards
                                    Annapurna Himalaya range, where we will have for lunch around this scenic spot for
                                    overnight stop along with wonderful views.<br><br>
                                    <b>Day 12: Rest day for short hike and exploration.</b><br>
                                    A rest day around this magnificent country facing breathtaking views of close peaks
                                    of Ganesh-Langtang Himal with Manaslu range of peaks, a pleasant place for rest
                                    where one can take a hike and explore the hidden areas of Ganesh Himal areas,
                                    chances of seeing some wild life as well.<br><br>
                                    <b>Day 13: Trek to Rupchet 3,045m - 06 hrs.</b><br>
                                    Leaving Bhanjyang behind which is not quite "all downhill" and the path does a
                                    couple of switch backs from one side of the ridge to the other, and always offering
                                    the promise of new views as reward for the effort we’ve put in. The trekking, the
                                    views, and the extraordinary feeling of walking on air-high above the world, has to
                                    be experienced to be believed, after a dramatic descent off the end of the higher
                                    Tiru Danda, and down the rocky gullies to reach Rupchet just above the tree-line and
                                    camp at Rupchet itself.<br><br>
                                    <b> Day 14: Trek to Saline 2,500 m - 05 hrs.</b><br>
                                    Today pass through the lush forests making a long descent toward the Salankhu Khola
                                    side. Once again back amongst the people and agricultural endeavor, passing through
                                    the farm lands and small villages eventually reaching at Saline village, camp on
                                    open terrace lands close to the village.<br><br>
                                    <b>Day 15: Trek to Deurali 1,540 m - 05 hrs.</b><br>
                                    Traversing through the terraced fields, crossing a steep little pass then into
                                    lovely forests to, reach at Boldugaon village, from here on leaving behind the
                                    village continue on zig-zag trails down towards picturesque agricultural fields at
                                    Deurali village for last overnight camp.<br><br>
                                    <b>Day 16: Trek to Trisuli Bazaar and drive back to Kathmandu.</b><br>
                                    Morning after breakfast with marvelous time around Ganesh Himal our last day walk
                                    takes you to low warmer farmland and villages with short downhill walk to reach at
                                    busy town at Trisuli Bazaar, here where our walks completes, taking a drive of few
                                    hours to reach back at hustle and bustle city life of Kathmandu, with afternoon free
                                    at leisure for individual activities
                                    and shopping souvenirs.<br><br>
                                    <b>Day 17: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after a great adventure and wonderful
                                    memories of Around Ganesh Himal Trekking, with your last final day in Nepal where
                                    Mount Vision Trek staff and guide transfer you to Kathmandu international airport
                                    for your flight home ward bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>
                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>
                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=AROUND MASSIVE GANESH HIMAL TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=AROUND MASSIVE GANESH HIMAL TREKKING" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



