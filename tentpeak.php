<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>TENT PEAK-THARPU CHULI CLIMB</h1>
            <p style="font-size:17px">“Adventure in the heart of massive Annapurna Himalaya range of mountains
                Enjoy the tough technical climb on top Tent Peak facing stunning panorama
                From low warm area to cooler hills and towards arctic zone of ice and glaciers
                Climb of striking Tent Peak lead to great technical experience to reach the top
                Explore interesting hill farm villages of Gurung the main tribe of Annapurna”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure within beautiful Annapurna Sanctuary and beyond’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Tent Peak -Tharpu Chuli Climb one of the most exciting adventure full of thrills to
                                    reach the summit of Tent-Peak also known as Tharpu Chuli rises at the height of
                                    5,663 m /18,575 ft high.
                                    Although less and below 6,000 m mountain but leads to great challenge all the way to
                                    its summit top, studying the nature of toughness where we have planned and set
                                    additional days for the climb.
                                    Extra free days helps you with full preparation as well acclimatization before the
                                    great summit day, where all climbers should be well equipped with proper gear for
                                    safe and enjoyable adventure to the top of Tent Peak.
                                    Tent Peak with NMA (Nepal Mountaineering Association) listed as Tharpu Chuli which
                                    is a local name for the peak located within beautiful Annapurna Sanctuary enclosed
                                    by series of mountains, truly a great place to experience with an adventure of
                                    life-time.
                                    The grade for Tent Peak -Tharpu Chuli climb is TD which means a mountain of
                                    ‘Technical Difficule’ as per French and Swiss Technical and Alpine Climbing Systems
                                    which is popular around for alpine style climb for a day or more.
                                    Walk leads after a drive to Pokhara from Kathmandu to reach past low warm areas
                                    towards high green cooler hills around interesting villages of Ghandruk and Chomrong
                                    inhabited by Gurung people the hill tribe of Annapurna Himalaya range.
                                    On leaving the last village of Chomrong walking into deep Modi River gorge covered
                                    within lush green forest of rhododendron-pines with bamboos then reaching on a wide
                                    valley within Annapurna Sanctuary to reach at Annapurna Base Camp.
                                    With last night in lodge, where our adventure leads beyond north into complete
                                    wilderness at Tent Peak / Tharpu Chuli base camp, here with rest days for
                                    preparation with some practice climb then heading to conquer the summit of Tent
                                    Peak.
                                    From the top enjoy spectacular panorama of mountains that surrounded you, feeling
                                    high and in superb spirits descend back towards Annapurna base camp and sanctuary to
                                    reach lower mid hills and then taking a short drive to beautiful Pokhara to complete
                                    our wonderful adventure on Tent Peak / Tharpu Chuli.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking: </b><br> Hotels with camping facilities on walks and
                                    climb.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous Walks<br>
                                    <b> Climbing Grade: </b><br> TD ‘Technical Difficule’ a French / Swiss Alpine
                                    Climbing
                                    Systems.<br>
                                    <b>Area of Trek and Climb:</b><br> Nepal North Mid-West within Annapurna
                                    Sanctuary.<br>
                                    <b> People and Culture:</b><br> Mainly populated by Gurung and enriched with
                                    Buddhist
                                    religions with impressive culture and traditional farm
                                    life.<br>
                                    <b> Trek and Climb: </b><br> 13 Nights and 14 Days (Pokhara to Pokhara with drives).<br>
                                    <b>Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b>Total Trip: </b><br> 18 Nights and 19 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons: </b><br> April to May best for trek and climb when wild-
                                    flowers in full bloom, most days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to November another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of


                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu and transfer to hotel.<br>
                                    <b> Day 02</b>: In Kathmandu optional sightseeing tour and trek preparations.<br>
                                    <b> Day 03:</b> Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by
                                    air).<br>
                                    <b> Day 04:</b> Drive past Bhirethati 1,100 m trek to Ghandruk 1,945 m - 05 hrs.<br>
                                    <b> Day 05: </b> Trek to Chomrong 2,170 m - 06 hrs.<br>
                                    <b>Day 06: </b> Trek to Dovan 2,630 m - 06 hrs.<br>
                                    <b>Day 07: </b> Trek to Machhapuchare Base Camp 3,810 m - 06 hrs.<br>
                                    <b>Day 08: </b> Trek to Annapurna Base Camp (ABC) at 4,130 m - 03 hrs.<br>
                                    <b> Day 09: </b> Trek to Tent peak base camp 4,450 m / 14,600 ft - 05 hrs.<br>
                                    <b>Day 10:</b> Rest day for acclimatization and climb preparation with practice.<br>
                                    <b>Day 11: </b> Trek to high camp 5,200 m / 17,056 ft - 05 hrs.<br>
                                    <b>Day 12: </b> Climb Tent Peak and return to base camp - 07 hrs.<br>
                                    <b>Day 13: </b>Spare, contingency day in case of unfavorable weather for the
                                    climb.<br>
                                    <b>Day 14: </b> Trek back to Annapurna Base Camp - 04 hrs.<br>
                                    <b> Day 15: </b> Trek to Bamboo 2,345 m - 06 hrs.<br>
                                    <b> Day 16 </b> Trek to Jhinu village 1,780m with Hot Spring - 06 hrs.<br>
                                    <b> Day 17:</b> Trek to road head and drive to Pokhara-05 hrs.<br>
                                    <b>Day 18:</b> Drive / fly to Kathmandu and transfer to hotel.<br>
                                    <b>Day 19:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: Arrival in Kathmandu and transfer to hotel.</b><br>
                                    On arrival received by our staff and guide and then transfer to your respective
                                    hotels in the centre of Kathmandu city, on checking your rooms and getting refreshed
                                    from jet-leg join with other members of Tent Peak-Tharpu Chuli Climb and Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, camping for few nights, with history and culture as
                                    well few important details.
                                    Evening include nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b>  Day 02: In Kathmandu with optional tour and preparation for climb and trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach at Pokhara
                                    around Nepal Mid-West region for Tent Peak / Tharpu Chuli Climb with Annapurna Base
                                    Camp trekking.<br><br>
                                    <b> Day 03: Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air).</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal main highway that
                                    connects with various parts of the country, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following Trisuli River for sometime
                                    as drive heads further west to reach at beautiful and scenic Pokhara city by its
                                    serene Phewa lake for overnight stop.
                                    Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate but
                                    with close views of towering and massive Annapurna Himalaya and majestic
                                    Machhapuchare Himal known as Fish Tail peak.
                                    Options by air to Pokhara take about 30 minutes of scenic air flight from Kathmandu.<br><br>
                                    <b>  Day 04: Drive past Bhirethati 1,100 m trek to Ghandruk village 1,945 m - 05 hrs.</b><br>
                                    Catching views of Annapurna peaks, morning a short drive of few hours heading past
                                    Pokhara city and its valley areas to reach at Nayapul and Bhirethati with fabulous
                                    scenery of Annapurna and green landscapes, on reaching nice Bhirethati village
                                    starting our first day walk following Modi River for sometimes.
                                    On leaving the low warm areas of paddy fields and Modi River, then our route leads
                                    to a climb past farm areas then reaching at large Ghandruk village, check into a
                                    fine lodge with afternoon free to relax and enjoy the views or visit its small
                                    museum of Gurung people, the tribe of the village as well spread all around
                                    Annapurna region.
                                    Gurung are farmers and cattle herders where most of the men-folk since last century
                                    a great demand as famous Gurkha Army in British and India military including
                                    Singapore and Brunei.<br><br>
                                    <b>Day 05: Trek to Chomrong 2,170 m - 06 hrs.</b><br>
                                    From Ghandruk morning take you up at the village top and then walk on gradual
                                    small path for short-while and then soon our climb to reach a hill top at Kimrong
                                    Danda (ridge) with some Tea-House and lodge, with short break on downhill walk
                                    into forest to reach a nice green valley.

                                    At Kimrong River with its nice farm village located in this peaceful valley, from
                                    here an hour more climb for an hour or more to reach at good winding gradual
                                    path.

                                    After walks on gradual path reaching our overnight stop at Chomrong, one of the
                                    largest Gurung villages similar to Ghandruk, this will be the last permanent village
                                    on route to Annapurna Base Camp and Sanctuary.<br><br>

                                    <b> Day 06: Trek to Dovan 2,630 m - 06 hrs.</b><br>
                                    After Chomrong downhill to cross over a bridge of Chomrong river then uphill to
                                    Sinuwa danda (ridge) with couple of Tea-House and shops, where walk leads into deep
                                    forested areas on winding ups and then descend into dense woodland with bamboo
                                    grooves to a place called also as Bamboo.

                                    A small settlement to cater the needs of the trekkers with many good lodges located
                                    in a river gorge surrounded by forest of rhododendron-pines and bamboo.

                                    From the Bamboo place walk is gradual into woods with easy ups to reach at Dovan for
                                    overnight stops in a small remote and isolated place with few lodges for travelers.<br><br>

                                    <b> Day 07: Trek to Machhapuchare Base Camp 3,810 m - 06 hrs.</b><br>
                                    Slowly our adventure leads closer to our aim and highlight as walk leads into
                                    winding ups into forest past a lonely place called Himalaya Hotel, located next to
                                    large overhanging cave Hinko which is directly beneath Annapurna South and
                                    Huin-Chuli peaks.

                                    From Himalaya Hotel downhill near Modi River then with short climb to reach at
                                    Deurali where tall tree lines stops for short alpine bushes, walk carries on uphill
                                    following Modi river right up to our overnight stop at Machhapuchare base camp
                                    located at the bottom of majestic North Face of Machhapuchare Himal ‘Fish Tail’.<br><br>

                                    <b> Day 08: Trek to Annapurna Base Camp (ABC) at 4,130 m - 03 hrs.</b><br>
                                    A short day walk to Annapurna base camp, from Machhapuchare where Annapurna
                                    Sanctuary starts right up to base camp, walk leads to short ups to reach a level
                                    ground on a huge plateau within scenic Sanctuary hidden within array of peaks where
                                    walk leads to a welcoming sign-post at Annapurna Base Camp.

                                    After few hours of good walk reaching our final destination and the highest spot at
                                    4,130 m high situated beneath towering Mt. Annapurna and range of other peaks of
                                    Vara-Shikar, Gangapurna with Annapurna South.

                                    Annapurna Base Camp, one and only base camp in whole Himalaya region with facility
                                    of nice and cozy lodge including nice food menu and rooms facing amazing views of
                                    Annapurna peaks.<br><br>


                                    <b> Day 09: Trek from Base Camp to Bamboo 2,345 m - 06 hrs.</b><br>
                                    Enjoying lovely and pleasant scenic times at Annapurna base camp a long walk of near
                                    six hours leads you downhill with short ups to reach at Bamboo and back into tall
                                    tree lines and lush vegetation, after being on arctic zone climate at Annapurna base
                                    camp and sanctuary.<br><br>

                                    <b>Day 10 Trek to Jhinu village 1,780m with Hot Spring - 06 hrs.</b><br>
                                    Return journey back to Chomrong with ups and downhill trail, from Chomrong hill top
                                    our route diverts to another fresh new trail to reach at nice hill village at Jhinu
                                    located on a ridge with grand scenery of rolling hills and mountains.

                                    Jhinu provides refreshing natural hot-spring for interested people which is a mere
                                    distance downhill of half hour to the hot spring with two nice warm pools close to
                                    raging Modi-River.<br><br>

                                    <b>  Day 11: Trek to road head and drive to Pokhara-05 hrs.</b><br>
                                    After a great time on high hills of Annapurna and at base camp, morning walks lead
                                    downhill back into warmer areas full of farm villages as walk follows Modi River all
                                    the way to a nice village by the road-head, and then with short drive of few hours
                                    to reach back at scenic and beautiful Pokhara for last overnight by its Phewa Lake
                                    side.<br><br>
                                    <b> Day 12: Drive or fly back to Kathmandu and transfer to hotel with free afternoon.</b><br>
                                    With enjoyable time in high Annapurna Himalaya where morning overland journey takes
                                    you on the same route to reach back at Kathmandu, after a wonderful time and
                                    experience on Annapurna Base Camp trekking with rest of the afternoon free at
                                    leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.<br><br>
                                    <b> Day 13: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and adventure on
                                    Annapurna Base Camp Trekking, with your last final day in Nepal where Mount Vision
                                    Trek staff and guide transfer you to Kathmandu international airport for your flight
                                    home ward bound.


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



