<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/gosaikunda1.JPG); ">


            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>LANGTANG HIMAL - HOLY GOSAINKUND TREKKING- 16 Days</h1>
            <p style="font-size:17px">“Trekking within first mountain National Parks of Nepal Himalaya ever to be
                established
                Glorious mountain panorama of Central Himalaya from beautiful Langtang Valley
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Adventure on high hills and valley with excellent views to holy Gosainkund pond
                Daily views of Langtang and Ganesh with Jugal Himal range high peaks on walks
                From low warm paddy fields to cooler alpine hills a great change of temperatures
                Explore traditional farm villages of Tamang people the indigenous hill tribes”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘on high hills and scenic ridges of Langtang Himal with adventure to holy lake of
                                    Gosainkund’</h3>
                                <p style="font-size: 17px;text-align: justify">Langtang Himal and Holy Gosainkund
                                    Trekking, an opportunity for travelers visiting the very best of Langtang and
                                    Central Himalaya that this beautiful scenic region has to offer with daily views of
                                    snow-clad mountains, cultural villages of Tamang and into sacred icy pond of
                                    Gosainkund.
                                    A magnificent adventure that leads you to both scenic areas of Langtang Himal,
                                    located within Nepal Central Himalaya, where you will have ample time soak into
                                    marvelous panorama of peaks and exploring local villages of great interest of
                                    ancient Buddhism religion and colorful culture.
                                    One of the most beautiful country and Himalayan range within close to the capital
                                    Kathmandu where high road joins with Nepal and Tibet -China border on an old Trans
                                    Himalayan Salt Trade Route which is still active to this day.
                                    Adventure to Langtang Himal and Holy Gosainkund Trekking begins with overland
                                    journey that takes you from warmer low areas to cooler high alpine hills filled with
                                    enchanting forest of tall rhododendron-pines and magnolia tree lines, where elusive
                                    wild-animals and rare Red Panda dwells.
                                    As walk continues after end of drive to reach our first main highlight destination
                                    of the adventure at Kyanjin within scenic Langtang valley, with time to acclimatize
                                    taking short hike to view tops facing incredible panorama of Central Himalaya and
                                    some peaks of Tibet side across the border beyond Langtang Himal range.
                                    Our next adventure follows a high trail heading past nice traditional farm villages
                                    and leaving green vegetation to sheer wilderness of arid zone at Gosainkund regarded
                                    as holiest of all holy sites for both Hindu and Buddhist followers, where great
                                    festival event takes place pilgrims comes in large number for a dip and bathe in
                                    this icy holy pond created by Lord Shiva.
                                    After an enjoyable and interesting time around holy Gosainkund where walk leads to
                                    cross the highest point of the adventure traversing Gosainkund Pass also called as
                                    East Laurabinaya-La at above 4,609 m and 15,100 ft high with grand views of
                                    landscapes and snow peaks.
                                    Journey continues with downhill and ups to reach a trail leading towards Kathmandu
                                    valley rim heading past nice farm villages and scenic hill top of Chisapani with
                                    eye-catching views of sunrise over chain of Himalayan peaks, as walk leads to a
                                    road-head close to the suburb of Kathmandu city, and then ending with short drive to
                                    reach into hustle and bustle city life of Kathmandu, after a grand and fantastic
                                    adventure on Langtang Himal and Holy Gosainkund Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Mode of Trekking:</b><br>Hotels and in lodge on walks.<br>
                                    <b>Trek Grade: </b><br>Moderate and Adventurous walks.<br>
                                    <b> Area of Trek:</b><br>Nepal Central Himalaya around Langtang region.<br>
                                    <b>Highest Altitude Gain:</b><br> Over Gosainkund pass / East Laurabinaya-La at
                                    4,609 m high.<br>
                                    <b>People and Culture:</b><br>Mainly populated by Tamang people enriched with
                                    Buddhist religion with impressive traditional culture.<br>
                                    <b>Trek Duration:</b><br> 12 Nights and 13 Days (with drive both ways).<br>
                                    <b>Average walks:</b><br>Minimum 4 hrs to Maximum 6-7 hrs.<br>
                                    <b>Total Trip:</b><br>15 Nights and 16 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br>All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.

                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01:</b><br> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b>Day 02:</b><br> In Kathmandu with optional sightseeing tour and preparation for
                                    the trek.<br>
                                    <b>Day 03:</b><br> Drive to Syabrubesi village 1,475 m via Dhunche town 1,945 m - 06
                                    hrs.<br>
                                    <b>Day 04:</b><br> Trek to place called Lama Hotel 2,280 m - 06 hrs.<br>
                                    <b>Day 05:</b><br> Trek to Langtang Village 3, 380 m - 06 hrs.<br>
                                    <b>Day 06:</b><br> Trek to Kyanjin 3,750 m - 04 hrs walks.<br>
                                    <b>Day 07:</b><br> At Kyanjin rest day for local hike with super panorama of
                                    mountain range.<br>
                                    <b>Day 08:</b><br> Trek to Lama Hotel - 06 hrs.<br>
                                    <b>Day 09:</b><br> Trek to Thulo Syabru village 2,115 m - 05 hrs.<br>
                                    <b>Day 10:</b><br> Trek to Chandanbari / Sing Gompa 3,240 m - 05 hrs.<br>
                                    <b>Day 11:</b><br> Trek to Holy Gosainkund and its glacial pond 4,380 m - 06
                                    hrs.<br>
                                    <b>Day 12:</b><br> Trek to Ghopte 3,560 m via Gosainkund pass- East Laurabinaya-La
                                    4,609 m.<br>
                                    <b>Day 13:</b><br> Trek to Kutumsang - 2,395 m hrs.<br>
                                    <b>Day 14:</b><br> Trek to Chisapani - 2,210 m - 7 hrs.<br>
                                    <b>Day 15:</b><br> Trek to Sundarijal and drive to Kathmandu and transfer to
                                    hotel<br>
                                    <b>Day 16:</b><br> Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                    hotel.</b><br>
                                On arrival where you will be well received by our staff and guide and then transfer to
                                your respective hotels in the centre of the city in Kathmandu, after checking into your
                                rooms getting refreshed from jet-leg join with other members of Langtang Himal and Holy
                                Gosainkund Trekking.
                                Our guide / leader will brief with information about the hotels-including detail of
                                trekking regarding local lodge, walks, view, history and culture with few important do’s
                                and don’ts while you are with us having enjoyable times.
                                Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                programs of all parts of the country to enlighten the environment along with your
                                dinner.<br><br>

                                <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.</b><br>
                                A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                sightseeing tour at places of great interest, exploring around Kathmandu world heritage
                                sites for
                                few hours and back at hotel, afternoon free for individual activities and preparations
                                for next morning overland journey to reach at Pokhara
                                around Nepal Mid-West region for Langtang Himal and Holy Gosainkund trekking.<br><br>

                                <b> Day 03: Drive to Syabrubesi village 1,475 m via Dhunche town 1,945 m - 06
                                    hrs.</b><br>
                                Morning after breakfast taking an interesting drive on Nepal North-West highway that
                                connects with Langtang Himal of Nuwakot and Rasuwa district as well to the border of
                                Tibet-China, where drive leads around warmer low areas past farm areas with villages and
                                towns, following higher winding road as drive heads further west to reach at nice large
                                town of Dhunche, headquarter of Rasuwa district.
                                From here on downhill winding drive to reach our overnight stop at Syabrubesi village
                                within home to large number of Tamang native tribe
                                of the region.<br><br>

                                <b> Day 04: Trek to place called Lama Hotel 2,280 m - 06 hrs.</b><br>
                                Trek starts of the first day heading to cross a bridge over Upper Trisuli River then
                                climb up towards a hill top with scattered farm houses and terraces, then enter into a
                                cool shade of forest within tall rhododendron-pines-oaks and fir tree lines.
                                As walk leads to more uphill in the forested path to reach a nice clearing where our
                                overnight stop located in a place called Lama Hotel
                                with few good lodges to stay.<br><br>

                                <b>Day 05: Trek to Langtang Village 3, 380 m - 06 hrs.</b><br>
                                From this peaceful small settlement in the midst of forest surrounding walk leads
                                following the river upstream entering into a gorge covered within thick green vegetation
                                and tree lines as our route leads climb to reach a nice spot of Ghoretabela, with few
                                houses serving as lodge and tea-house.
                                After a refreshing break walk continues into forested areas crossing few streams to
                                reach our overnight stop in Langtang Village, an
                                interesting place to explore around after a great walk of the day.<br><br>

                                <b> Day 06: Trek to Kyanjin 3,750 m - 04 hrs walks.</b><br>
                                With pleasant overnight at Langtang village, today a shortest walk of the journey where
                                route leads to our main highlight at Kyanjin with scenic Langtang valley where our route
                                leads to a short steep climb to enter a wide open valley to reach at Kyajin situated in
                                the midst of Langtang valley enclosed within high peaks of Langtang Himal.
                                At Kyanjin with time to relax inside a nice cozy lodge and explore around this scenic
                                areas with close views of glaciers towards north fed from Mt. Langtang Lirung and other
                                close adjoining peaks.

                                <b> Day 07: At Kyanjin rest day for local hike with super panorama of mountain
                                    range.</b><br>
                                A great scenic spot for full rest day with options of short hike to climb on top
                                view-point of Tsego or Kyanjin-Ri a nearby hill top
                                rewards you with fantastic panorama of Langtang range of peaks facing Langtang Himal,
                                Ganesh Himal (7,405 m), Langtang Lirung (7,234 m),
                                Ghengu Liru (Langtang II 6,571m), Kimshun (6,745 m) and Shalbachum(6,918m), Chimsedang,
                                Naya-Kanga or Ganja Chuli (5,846m), Gangchempo, T
                                ilman's beautiful Fluted Peak, Jugal Himal and ending towards eastern horizon at Dorje
                                Lakpa (6,980m), rest of the afternoon at leisure and
                                short walk around this dramatic and scenic Langtang Valley.<br><br>
                                Day 08: Trek to Lama Hotel - 06 hrs.</b><br>
                                Our return journey leads on the same path allowing you with more opportunity to witness
                                more of the country beautiful scenery and culture
                                around Tamang farm villages, as walk enters into deep forest and river gorge with
                                downhill to Lama Hotel for overnight stop.<br><br>

                                <b>Day 09: Trek to Thulo Syabru village 2,115 m - 05 hrs.</b><br>
                                Today our next adventure leads to another route towards high terrain at Gosainkund after
                                reaching Thulo Syabru village, as morning starts with slow downhill walk into forest to
                                reach a place called River-side with few huts serving as tea-house and refreshment.
                                From here walking past forested area with long climb to reach a ridge then on nice
                                pleasant trail to our overnight stop at Thulo or Upper
                                Syabrub village, a nice place with houses lined in a row like a locomotive with
                                monastery on top of the Tamang village.<br><br>

                                <b>Day 10: Trek to Chandanbari / Sing Gompa 3,240 m - 05 hrs.</b><br>
                                From this lovely Tamang village enriched with ancient heritage life-style morning climb
                                on winding steep slope within farm terrace and into cool shade of coniferous forest
                                filled with tall rhododendron-magnolia-oaks-pines and fir tree lines, heading past
                                cattle herders shelters within grazing field.

                                Walk continues with final climb, then on pleasant trail to reach at Sing Gompa also
                                called as Chandanbari (a place with sandal wood trees), at Sing Gompa check into a nice
                                lodge, with afternoon free visit its small cheese factory and old monastery situated on
                                top of the village.

                                <b> Day 11: Trek to Holy Gosainkund and its glacial pond 4,380 m - 06 hrs.</b><br>
                                With pleasant overnight stop at Chandanbari or Sing Gompa village, as morning walk leads
                                with views of Langtang and Ganesh-Himal with
                                Manaslu and Annapurna range in the far west, our route leads with higher climb for an
                                hour into rhododendron forest.

                                Walk into green forested areas and then reaching a small place at Laurabinaya, where
                                steep climb takes you much higher after leaving tree lines for dwarf rhododendron,
                                juniper bushes, walk unwinds uphill past few teahouses around small place of
                                Laurabinaya, where short downhill reaches the first lake Saraswati and the Bhairav-Kund
                                with short climb to Gosainkund for overnight stop within close to its calm pond.

                                At Gosainkund, a great religious festival takes place annually called ‘Janai Purnima’ in
                                Full Moon time of August, where great numbers of
                                Hindu and Buddhists pilgrims throng the lake for a holy bathe, as per Hindu popular
                                belief, a huge rock at the center of the pond at one time
                                had Shiva <br><br>

                                <b>Day 12: Trek to Ghopte 3,560 m via Gosainkund pass- East Laurabinaya-La 4,609
                                    m.</b><br>
                                After being blessed within its holy sites of Gosainkund, morning walk leads beyond the
                                pond then climb over rocks and boulders, as walk leads towards smaller ponds where trail
                                winds up to reach on top Gosainkund pass or East Laurabinaya La, the highest spot of the
                                adventure.
                                From the top with long downhill walk to reach at Phedi (bottom) a place with few
                                teahouses, here for possible lunch break and then continue walk heading east towards
                                Helambu region within green vegetations of junipers, rhododendron to reach at Ghopte
                                (meaning overhanging rocks) for overnight stop.

                                <b> Day 13: Trek to Kutumsang - 2,395 m hrs.</b><br>
                                From this isolated in the middle of nowhere in complete wilderness where morning walk
                                talks you to join Helambu and Kathmandu trails; a pleasant walk into forest with ups and
                                down for an hour with climb on top Thadepatti Hill located above 3,597m high facing
                                views of Jugal Himal range and Ganesh-Manaslu towards west.
                                From here our route descend to a nice grassy field, and then back into farm fields to
                                reach our overnight stop in Kutumsang, a nice
                                village with Sherpa, Hyalmo and Tamang hill people.<br><br>

                                <b>Day 14: Trek to Chisapani - 2,210 m - 7 hrs.</b><br>
                                Today will be one of the longest day walk to reach a high scenic hill at Chisapani,
                                where morning walk leads on the trail towards Kathmandu following winding path with some
                                ups and downhill, past farm areas and villages to reach another lovely village at
                                Gul-Bhanjyang.
                                Having refreshing stops walk continues on pleasant trail to Pati Bhanjyang village, from
                                here with short climb to reach our last overnight stops at Chisapani, a nice and scenic
                                small village with marvelous mountain views and close to north of Kathmandu Valley rim.
                                A lovely spot offers striking sunrise and sunset views over range of peaks from Jugal
                                Himal range towards north east and Ganesh
                                Himal-Manaslu and Annapurna range towards west.<br><br>

                                <b>Day 15: Trek to Sundarijal and drive to Kathmandu and transfer to hotel</b><br>
                                Morning with marvelous sunrise views over chain of Himalayan peaks, where our last day
                                walk enters into a nice cool forest and then past farmlands to reach a ridge top of
                                Burlang and Chepu Bhanjyang with views of Kathmandu valley.
                                From here a long descend to reach on the road head at Sundarijal (means beautiful water)
                                a lovely spot with cool patch of forest with monkeys and fresh streams.
                                On reaching at Sundarijal, a small farm town where drive takes you back into Kathmandu
                                after a great marvelous adventure and experience
                                on Langtang Himal and Holy Gosainkund Trekking, as drive leads past great Stupa of
                                Bouddhanath, then entering into a busy road then back at
                                your hotel in Kathmandu with rest of the afternoon free at leisure.<br><br>

                                <b> Day 16: Depart Kathmandu for international departure homeward bound.</b><br>
                                Finally approaching last day in Kathmandu after great memories and experience on
                                Langtang Himal and Holy Gosainkund Trekking, with your
                                last final day in Nepal where Mount Vision Trek staff and guide transfer you to
                                Kathmandu international airport for your flight home ward
                                bound.<br><br>

                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1500</strike></strong>
                    </h3>
                    <h3>
                        <p class="price " style="color:white;"><span>$1250</span>
                            <small>/ person</small>
                        </p>
                    </h3>


                    <a href="booking.php?id=LANGTANG HIMAL - HOLY GOSAINKUND TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=LANGTANG HIMAL - HOLY GOSAINKUND TREKKING"
                       class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



