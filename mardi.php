<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css" />

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">



            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>MARDI HIMAL TREKKING- 12 Days</h1>
            <p style="font-size:17px">“Glorious mountain panorama with stunning sunrise from scenic Poon Hill
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Daily views of Annapurna Himalaya with Dhaulagiri and Manaslu peaks
                From low warm paddy fields to cooler alpine hills a great change of temperatures
                Explore traditional farm villages of Poon-Magar and Gurung people the hill tribes”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen" >Overview</button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day Itinerary</button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include Excludes</button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure on the foot of towering Machhapuchare ‘Fish Tail’ peak and Mardi Himal’</h3>
                                <p style="font-size: 17px;text-align: justify">Mardi Himal Trekking one of the shortest and smoothest walks on high hills of Annapurna Himalaya region, where you will have glorious and memorable time and experience within pristine location beneath Mardi Himal and close to towering and majestic Machhapuchare Himal, popularly known as ‘Fish Tail’ peak due to its twin summit tops.
                                    An adventure to Mardi Himal Trekking a marvelous walks on high ridges in the shade of massive range of Annapurna Mountains, where you will enjoy daily views of the mountains all along the walks to Mardi Himal base camp.
                                    Mardi Himal, a great trekking adventure within week duration leads you beyond human settlement and villages as well of the beaten tracks away from mainstream trekking trails, where only few trekkers seldom use this remote and isolated trail.
                                    Walk follows after a short drive from scenic Pokhara city to reach at high hills of Dhampus with grand fabulous panorama of mountains, and then into serene cool woodland of tall rhododendron-pines-oaks and magnolia trees.
                                    As walk progress reaching much higher country where tree lines disappears for short bushes of Burberry and rhododendron known as azalea, exploring high areas of Mardi Himal within majestic Southern Face of Machhapuchare Himal, with great commanding views of Annapurna Himalaya.
                                    After a wonderful feeling reaching at the highest spot of the adventure at Mardi Himal base camp, where return journey leads on the same trail for some days and then on alternative route to the road head at warmer farm village then taking a short drive back at scenic Pokhara to conclude fabulous moments and great experience on Mardi Himal Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:	</b></br>	Hotels and in lodge on walks.</br>
                                    <b>Trek Grade:		</b></br>	Moderate to Adventurous.</br>
                                    <b>Area of Trek:		</b></br>	Nepal North Mid-West within Annapurna Himalaya region.</br>
                                    <b> Highest Altitude Gain:	</b></br>	At Mardi Himal Base Camp 4,485 m high.</br>
                                    <b>People and Culture:	</b></br>	Mainly populated by Gurung enriched with Buddhist religion and
                                    impressive culture.</br>
                                    <b> Trek Duration:		</b></br>	06 Nights and 07 Days (Pokhara to Pokhara)</br>
                                    <b> Average walks:	</b></br>	Minimum 4 hrs to Maximum 6 hrs.</br>
                                    <b> Total Trip:	</b></br>		11 Nights and 12 Days Kathmandu to Kathmandu.</br>
                                    <b> Seasons:		</b></br>	All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.



                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p >
                                    <b>   Day 01:</b>	Arrival in Kathmandu via respective international airline and transfer to hotel.</br>
                                    <b>  Day 02:</b>	In Kathmandu with optional sightseeing tour and preparation for the trek.</br>
                                    <b>  Day 03:</b>	Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)</br>
                                    <b>  Day 04:</b>	Drive to Dhampus 1,530 m and trek to Potana village 1,900 m - 05 hrs.</br>
                                    <b>  Day 05:</b>	Trek to Kokar Forest camp 2,515 m via Deurali 2, 050 m - 05 hrs.</br>
                                    <b>  Day 06:</b>	Trek to Low Camp 2,968 m - 05 hrs.</br>
                                    <b>  Day 07:</b>	Trek to High Camp 3,710 m - 05 hrs.</br>
                                    <b>  Day 08:</b>	At High Camp for hike to Mardi Himal base camp above 4,490 m.</br>
                                    <b>  Day 09:</b>	Trek to Siding village 1,680m  - 06 hrs.</br>
                                    <b>  Day 10:</b>	Trek to road head and drive to Pokhara-04 hrs.</br>
                                    <b>  Day 11:</b>	Drive or fly back to Kathmandu and transfer to hotel with free afternoon.</br>
                                    <b>  Day 12:</b>	Depart Kathmandu for international departure homeward bound.</br>
                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent"style="background-color: whitesmoke">
                                <p> <b>Day 01:	Arrival in Kathmandu via respective international airline and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer to your respective hotels in the centre of the city in Kathmandu, after checking into your rooms getting refreshed from jet-leg join with other members of Mardi Himal Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of trekking regarding local lodge, walks, view, history and culture with few important do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural programs of all parts of the country to enlighten the environment along with your dinner.<br><br>

                                    <b>Day 02:	In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional sightseeing tour at places of great interest,
                                    exploring around Kathmandu world heritage sites for few hours and back at hotel, afternoon free for individual activities and
                                    <br><br>preparations for next morning overland journey to reach at Pokhara around Nepal Mid-West region for Mardi Himal trekking.

                                    <b> Day 03:	Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal main highway that connects with various parts of the country, where drive leads around warmer low areas past farm areas with villages and towns, following Trisuli River for sometime as drive heads further west to reach at beautiful and scenic Pokhara city by its serene Phewa lake for overnight stop.
                                    Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate but with close views of towering and massive Annapurna Himalaya and majestic Machhapuchare Himal known as Fish Tail peak.
                                    Options by air to Pokhara take about 30 minutes of scenic air flight from Kathmandu.<br><br>

                                    <b>Day 04:	Drive to Dhampus 1,530 m and trek to Potana village 1,900 m - 05 hrs.</b><br>
                                    With wonderful views of Annapurna range of peaks, where morning leads to a short drive of few hours heading past Pokhara city and its valley areas to reach at Dhampus village with fabulous scenery of Annapurna and green landscapes, on reaching at nice Gurung village at Dhampus, located on high ridge with great panorama of Annapurna-Manaslu with Machhapuchare Himal ‘Fish Tail’ includes views of Dhaulagiri also.
                                    From Dhampus enjoying grand views where walk leads to a slow climb past farm areas and into cool shade of
                                    forest to reach at Potana for overnight stop, a fair size village, with views of stunning Machhapuchare and Mardi Himal.<br><br>

                                    <b> Day 05:	Trek to Kokar Forest camp 2,515 m via Deurali 2, 050 m - 05 hrs.</b><br>
                                    Catching morning views from Potana, our second day walk leads on main path with climb to a hill top called Deurali, offers grand views of landscapes with snow peaks.
                                    From Deurali our route heads north leaving the main trekking trails into sheer wilderness where walks
                                    leads along a ridgeline into serene forested area with cool temperature views of Annapurna I and South
                                    and Huin-Chuli, includes Annapurna IV -II and Lamjung Himal towards east after a good walk reaching our
                                    overnight stop in the midst of forest surrounding at Kokar Forest Camp with few basic lodges for trekkers.<br><br>

                                    <b> Day 06:	Trek to Low Camp 2,968 m - 05 hrs.</b><br>
                                    From Forest Camp, where morning walk heads to steep uphill path with steep climb to reach a
                                    pasture land for overnight stop on route Mardi-Fish Tail base camp with super views of surrounding landscapes and mountains at close range.<br><br>

                                    <b> Day 07:	Trek to High Camp 3,710 m - 05 hrs.</b><br>
                                    This scenic spot offers grand panorama of beautiful landscapes and snow-clad peaks in perfect tranquility heading higher to reach at High Camp as the air gets thinner and cooler with steep climb to reach at High Camp where tree lines fades to short rhododendron and juniper bushes.
                                    Here at High Camp with a lonely lodge for overnight accommodation with basic facilities commanding super views of Annapurna Himalaya.<br><br>

                                    <b> Day 08:	At High Camp for hike to Mardi Himal base camp above 4,490 m.</b><br>
                                    At High Camp with spare day to explore then unexplored areas of Mardi Himal close to Machhapuchare Himal or Fish Tail peak base camp, as morning hike leads to climb on a high ridge then reaching at the highest spot of the adventure at Mardi Himal base camp above 4,485 m high. After walking on a steep slope along ridgeline to reach within this hidden country on the foot of Mardi Himal and close within South Face of Mt. Machhapuchare; this area also known as the 'Other Sanctuary' named by early explorer and pioneer.
                                    After being on the high side of Mardi areas then return on the same route back to the lodge at High Camp
                                    for last overnight around tranquil areas of Mardi Himal.<br><br>

                                    <b>Day 09:	Trek to Siding village 1, 680 m - 06 hrs.</b><br>
                                    After a wonderful time on Mardi Himal with grand views then our walk leads back into
                                    forest of rhododendron and pines trees, retracing the journey back on same trail at Jungle
                                    Camp and beyond with more downhill walk to reach at Sidang village located at the junction of East
                                    and West route towards Mardi Himal areas, at Sidang farm village for overnight stop ending our long day nice walks.<br><br>

                                    <b> Day 10:	Drive back to Pokhara 860 m short walk with 2 hrs rides.</b><br>
                                    Morning catching views of snow peaks, our last and final day trek leads to the road head for a while
                                    and then with short drive back ride to reach at beautiful Pokhara, where our Mardi Himal trekking completes
                                    by the serene Phewa Lake for last overnight stop around Annapurna region.<br><br>

                                    <b> Day 11:	Drive or fly back to Kathmandu and transfer to hotel with free afternoon.</b><br>
                                    With enjoyable time in high Annapurna Himalaya where morning overland journey takes you on the same route to reach back at Kathmandu, after a wonderful time and experience on Mardi Himal trekking with rest of the afternoon free at leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from Pokhara.<br><br>

                                    <b> Day 12:	Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and adventure on Mardi Himal
                                    Trekking, with your last final day in Nepal where Mount Vision Trek staff and guide transfer you
                                    to Kathmandu international airport for your flight home ward bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg" style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg" style="height:200px;width: 200px;" />
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent"style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1" >
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box" >
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1500</strike></strong></h3>
                        <h3> <p class="price "style="color:white;"><span>$1250</span> <small>/ person</small></p></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png"  style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>   </center><br>



                    <a href="booking.php?id=MARDI HIMAL TREKKING " class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=MARDI HIMAL TREKKING" class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {font-family: Arial;}

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    function showImage(src){
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>



