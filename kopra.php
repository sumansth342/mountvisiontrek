<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">


            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>KOPRA RIDGE - ANNAPURNA TREKKING- 13 Days</h1>
            <p style="font-size:17px">“Around hidden and exclusive areas of Annapurna and Dhaulagiri on least visited
                areas
                On high and scenic Kopra ridge in between massive range of Annapurna and Dhaulagiri
                “Glorious panorama with stunning sunrise over chain of mountains from scenic Poon Hill
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Daily views of Annapurna Himalaya with close views of Dhaulagiri and Manaslu peaks
                From low warm paddy fields to cooler alpine hills with great change of temperatures
                Explore traditional farm villages of Poon-Magar and Gurung people the hill tribes”

            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure in between Annapurna and Dhaulagiri massive peaks facing incredible
                                    views’</h3>
                                <p style="font-size: 17px;text-align: justify">Kopra Ridge and Annapurna Trekking, leads
                                    you in between massive mountain range of Annapurna and Dhaulagiri within close to
                                    Vara-Shikar / Annapurna Fang around hidden pockets of Nepal North Mid-West Himalaya.
                                    An exclusive scenic destinations where you will walking away from main trails and
                                    villages for some days in complete wilderness of stunning high mountains and grand
                                    views, walking into serene woodland of tall
                                    rhododendron-magnolia-oaks-hemlocks-pines and fir tree lines.
                                    One of the few trekking areas reserved for interested travelers to be in off the
                                    beaten tracks within tranquil surroundings, as this adventure exactly leads you to
                                    beautiful corners of Annapurna Himalaya range on least explored and visited places.
                                    Starting our wonderful Kopra Ridge and Annapurna Trekking from the famous and
                                    popular tourist destination of Pokhara, where drive leads to follow the main trail
                                    for few days to reach at Ghorepani and Poon Hill, offers spectacular panorama of
                                    mountain views with striking sunrise.
                                    After Ghorepani enjoying grand views and comfort of local teahouse and lodge, then
                                    heading towards higher terrain on leaving the main trails of Annapurna, as walks
                                    leads into enchanting forest areas to reach at scenic and beautiful spot at Kopra
                                    ridge, surrounded by array of peaks.
                                    Staying for few days with optional excursion and hike to holy lake of Khair fed from
                                    the glaciers of Vara-Shikar and Annapurna peaks, regarded as holy where once a year
                                    in the month of August a great religious festival takes places with hundred of local
                                    pilgrims visits for dip and bathe in the holy lake of Khair.
                                    Enjoying lovely moments slowly our walk leads back to join the main trail at
                                    Tadapani and into large Gurung village at Ghandruk, with time to observe local
                                    immense culture and traditions of the hill people, and then taking a short drive to
                                    Pokhara after completing a marvelous adventure on Kopra Ridge and Annapurna
                                    Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking: </b><br>Hotels and in lodge on walks.<br>
                                    <b>Trek Grade: </b><br>Moderate and Adventurous.<br>
                                    <b>Area of Trek: </b><br>Nepal North Mid-West within Annapurna Himalaya region.<br>
                                    <b>Highest Altitude Gain: </b><br>On top Kopra Ridge 4,015 n with Khair Lake at
                                    4,590 m high.<br>
                                    <b> People and Culture:</b><br>Mainly populated by Gurung and Poon-Magar enriched
                                    with
                                    both Hindu and Buddhist religions with impressive culture.
                                    <b>Trek Duration: </b><br>07 Nights and 08 Days (Pokhara to Pokhara)<br>
                                    <b>Average walks: </b><br>Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b>Total Trip:</b><br>12 Nights and 13 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons: </b><br>March to May and September to December months<br>
                                    March to May good for treks when wild-flowers in full bloom,
                                    where most of the days are clear for views.
                                    Morning and night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.


                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b> Day 03:</b> Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)<br>
                                    <b> Day 04:</b> Drive to Bhirethati 1,100 m and walk to Tirkhedhunga village 1,465 m
                                    - 04 hrs.<br>
                                    <b> Day 05:</b> Trek to Ghorepani 2,875 m via Ulleri village - 06 hrs.<br>
                                    <b> Day 06:</b> Hike to Poon Hill 3,210 m and continue trek to Sauta village 2,210 m
                                    - 05 hrs.<br>
                                    <b> Day 07:</b> Trek to Kopra Ridge 4,015 m - 06 hrs.<br>
                                    <b> Day 08:</b> Free day at Kopra for optional hike to holy Lake of Khair 4,590
                                    m.<br>
                                    <b> Day 09:</b> Trek to Dobato 3,455 m - 06 hrs.<br>
                                    <b> Day 10:</b> Trek to Ghandruk village 1,945 m - 06 hrs.<br>
                                    <b> Day 11:</b> Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                    ride.<br>
                                    <b> Day 12:</b> Drive or fly back to Kathmandu and transfer to hotel with free
                                    afternoon.<br>
                                    <b> Day 13: </b>Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Kopra
                                    Ridge and Annapurna Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>

                                    <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring
                                    around Kathmandu world heritage sites for few hours and back at hotel, afternoon
                                    free for individual activities and preparations for next morning overland journey to
                                    reach at Pokhara around Nepal
                                    Mid-West region for Kopra Ridge and Annapurna trekking.<br><br>

                                    <b>Day 03: Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal main highway that
                                    connects with various parts of the country, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following Trisuli River for sometime
                                    as drive heads further west to reach at beautiful and scenic Pokhara city by its
                                    serene Phewa lake for overnight stop.
                                    Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate but
                                    with close views of towering and massive Annapurna Himalaya and majestic
                                    Machhapuchare Himal known as Fish Tail peak.
                                    Options by air to Pokhara take about 30 minutes of scenic air flight from Kathmandu.<br><br>

                                    <b>Day 04: Drive to Bhirethati 1,100 m and walk to Tirkhedhunga village 1,465 m - 04
                                        hrs.</b><br>
                                    With wonderful views of Annapurna range of peaks, where morning leads to a short
                                    drive of few
                                    hours heading past Pokhara city and its valley areas to reach at Nayapul and
                                    Bhirethati with fabulous
                                    scenery of Annapurna and green landscapes, on reaching nice Bhirethati village
                                    starting our first day
                                    walk following an old Trans-Himalayan Salt Trade Route of Nepal and Tibet with views
                                    of cascading waterfalls,
                                    and walking past farm areas and villages to reach our overnight stop at Tirkhedhunga
                                    close to nice waterfalls.<br><br>

                                    <b> Day 05: Trek to Ghorepani 2,875 m via Ulleri village - 06 hrs.</b><br>
                                    From Tirkhedhunga morning walk on steep stone steps with climb up to the village of
                                    Ulleri with views of Annapurna peaks, an interesting large village of Gurung and
                                    Magar people, from here a gradual walk leads you into enchanting forest of tall
                                    rhododendron-magnolia-oaks-pines and fir tree lines.
                                    As walk progress on winding up feel the change of temperatures with cooler mountain
                                    air as our day journey ends at nice Ghorepani village for overnight, located within
                                    woodland of rhododendron trees and views of Dhaulagiri mountain range.
                                    Ghorepani a famous and popular village for short trekking as well where hike leads
                                    to Poon Hill for stunning sunrise and mountain panorama.<br><br>

                                    <b> Day 06: Hike to Poon Hill 3,210 m and continue trek to Sauta village 2,210 m -
                                        05 hrs.</b><br>
                                    Early morning at the crack of dawn as weather permits, hike to Poon Hill with ups
                                    for an hour to reach the scenic top with a view tower at the height of 3,210 m one
                                    of the highest spot of the journey.
                                    From Poon Hill catch the striking sunrise views over chain of Himalayan Mountains
                                    that includes Dhaulagiri-Nilgiri’s-Annapurna with Machhapuchare Himal as far towards
                                    Lamjung and Manaslu range of peaks.
                                    After a glorious moment along with the views descend to Ghorepani, having breakfast
                                    where our route leads downhill to Chitre village as walk leads within rhododendron
                                    forest along with views of snow-clad mountain range then reaching at Chitre.
                                    From here our route leads away from main trekking trail heading into remote and
                                    isolated village at Sauta, a moderate size Poon-Magar farm village for overnight,
                                    facing views of Dhaulagiri range.
                                    Sauta will be the last village before reaching at Ghandruk, while other places are
                                    just temporary settlement.<br><br>

                                    <b> Day 07: Trek to Kopra Ridge 4,015 m - 06 hrs.</b><br>
                                    From here walking beyond the village and into forest areas with climb up to a small
                                    farm place of Sistibung,
                                    and then as walk continues where altitude gains slowly on leaving tree lines behind
                                    after few hours of scenic walk reaching at
                                    overnight stop at Khair ridge, a scenic spot with scenery of landscapes along with
                                    stunning views of Mt. Dhaulagiri massive range
                                    and Vara Shikar (Annapurna Fang), Annapurna South which towers directly from this
                                    wonderful spot.<br><br>

                                    <b> Day 08: Free day at Kopra for optional hike to holy Lake of Khair 4,590
                                        m.</b><br>
                                    Today a free day with options for hike to holy Lake of Khair at above 4,590 m, a
                                    long walk of 4-5 hrs both ways, but worth the views the site where religious
                                    festival held annually around the month of August and September. Villagers from the
                                    surrounding area assemble here to offer prayers to Lord Shiva and bathe in the icy
                                    waters, offers sacrifices of sheep and goats.
                                    At Khair Lake marvel the beautiful surrounding within close to towering Mt.
                                    Annapurna I, Annapurna South and Annapurna Fang or Vara Shikar,
                                    a pristine area, where few trekkers often venture around this amazing place.

                                    <b>Day 09: Trek to Dobato 3,455 m - 06 hrs.</b><br>
                                    After a great time at Khair Lake, walk back to Chistibung starting with a slow
                                    gradual morning walk on off beaten trail passing farm fields and scattered
                                    Sheppard’s huts, trekking downhill through enchanting rhododendron and bamboo
                                    forest, walking small stream to summer pastures at Chistibung, a small place with
                                    few small huts serving as tea-houses.
                                    From this isolated place walk leads ups and then down to within grazing lands
                                    reaching middle of rhododendron forest to our overnight stop at
                                    Dobato, a summer pasture with views of Annapurna South and Mt. Machhapuchare / Fish
                                    Tail peak at 6,993 meters.<br><br>

                                    <b>Day 10: Trek to Ghandruk village 1,945 m - 06 hrs.</b><br>
                                    Catching morning views of mountains, walk continues on downhill within forest of
                                    rhododendron, oak and magnolia, then reaching the main trekking route of Ghandruk
                                    and Ghorepani trail at Tadapani.
                                    From here after a short stop, downhill to Ghandruk for overnight in the comfort of
                                    nice and cozy lodge, with time to visit this large Gurung village of Annapurna
                                    region.
                                    Afternoon free to relax and enjoy the views or visit its small museum of Gurung
                                    people, the tribe of the village as well spread all around Annapurna region.
                                    Gurung are farmers and cattle herders where most of the men-folk since last century
                                    a great demand as famous Gurkha Army in British
                                    and India military including Singapore and Brunei.<br><br>

                                    <b> Day 11: Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                        ride.</b><br>
                                    After a great time on high hills of Annapurna, morning walks lead downhill back into
                                    warmer areas full of
                                    farm villages as walk follows Modi River all the way to a nice Bhirethati village
                                    for short drive to reach back at
                                    scenic and beautiful Pokhara for last overnight by its Phewa Lake side.<br><br>

                                    <b> Day 12: Drive or fly back to Kathmandu and transfer to hotel with free
                                        afternoon.</b><br>
                                    With enjoyable time in high Annapurna Himalaya where morning overland journey takes
                                    you on the same route to reach back at Kathmandu, after a wonderful time and
                                    experience on Annapurna Sunrise Panorama trekking with rest of the afternoon free at
                                    leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.<br><br>

                                    <b> Day 13: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and adventure on
                                    Khair Ridge and Annapurna Trekking,
                                    with your last final day in Nepal where Mount Vision Trek staff and guide transfer
                                    you to Kathmandu international airport
                                    for your flight home ward bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $1300</strike></strong>
                    </h3>
                    <h3>
                        <p class="price " style="color:white;"><span>$1236</span>
                            <small>/ person</small>
                        </p>
                    </h3>


                    <a href="booking.php?id=MARDI HIMAL TREKKING " class="btn btn-primary btn-outline btn-block">Book
                        Now</a></p>
                    <a href="enquiry.php?id=MARDI HIMAL TREKKING"
                       class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



