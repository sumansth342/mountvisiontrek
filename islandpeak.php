<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>Island PEAK CLIMB</h1>
            <p style="font-size:17px">“Island Peak a striking trekking peaks leads you to exciting adventure climb
                Enjoy grand views on walks and climb within high and scenic Khumbu valley
                Highest successful climbing record on Island Peak climb than other lesser peaks
                A straight-forwards climb of less technical skill with fabulous mountain panorama
                Within far east corners of Khumbu and Imjatse valley close to Mt. Lhotse area
                Explore impressive Buddhist culture visiting Sherpa villages and monasteries”

            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘enjoy the climb on stunning Island Peak in the midst of scenic Imjatse
                                    valley’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Island Peak Climb where you will be in the middle of scenic and beautiful Imjatse
                                    valley within high areas of Khumbu in the shade of mighty Mt. Everest and other high
                                    mountains.
                                    Island Peak Climb a great combination of climb and trek to visit number one Mt.
                                    Everest base camp and on top scenic Kalapathar at 5,545 m high rewards you with
                                    stunning panorama of surrounding peaks with Everest and Pumori at near distance.
                                    We have perfectly planned Island Peak Climb allowing you with more time to immerse
                                    into magnificent views as well for acclimatization before Island Peak Climb with
                                    much preparation having enough rest days to gather your strength required for the
                                    big haul on top Island Peak at above 6,189 m and 20,305 ft high.
                                    The grade of the climb to Island Peak summit a PD + used from famous French and
                                    Swiss Alpine Climbing Classification Systems which means Per Difficule that leads
                                    you to moraine-glaciers and ice with snow on climb a straight-forward climb but with
                                    some technical know-how required in some technical sections to reach the top.
                                    Where all climbers should be in perfect physical shape and in sound health to enjoy
                                    the adventure on Island Peak Climb requires adequate equipment for the climb as
                                    suggested by our expert climbing guides and the company to make your trip a great
                                    success.
                                    Starts with flight from Kathmandu to Lukla and then heading higher past nice Sherpa
                                    villages of Buddhist culture and religion that you can witness along the walks
                                    entering into serene green forest and then reaching famous Namche Bazaar with rest
                                    days to explore the scenic and cultural sites.
                                    From Namche walk continues high above beautiful Thyangboche Monastery visiting its
                                    impressive monastery, and then into sheer wilderness of high mountains and arid
                                    country to reach at Everest Base Camp and then towards Island Peak base camp for
                                    grand adventure Island Peak Climb.
                                    Island Peak also called as Imjatse Himal, due to its location in the midst of scenic
                                    Imjatse valley where climb takes you to the summit top facing outmost panorama of
                                    world’s highest mountains, and then heading back to Lukla for sweeping flight to
                                    Kathmandu, after a great experience of a lifetime adventure on Island Peak Climb.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br> Hotels with camping facilities on walks and Climb.<br>
                                    <b> Trek Grade:</b><br> Adventurous Walks.<br>
                                    <b> Climbing Grade: </b><br> PD + ‘Per Difficule’ a French / Swiss Alpine Climbing
                                    Systems.<br>
                                    <b> Area of Trek:</b><br> Nepal North Mid-East within Khumbu district of
                                    Everest.<br>
                                    <b> People and Culture:</b><br> Populated by Sherpa the highlanders of Everest
                                    enriched
                                    with colorful Buddhist religion and impressive culture.<br>
                                    <b> Trek and Climb: </b><br> 16 Nights and 17 Days (Lukla to Lukla).<br>
                                    <b> Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b> Total Trip: </b><br> 20 Nights and 21 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br> April to May is good for trek and climb when wild-
                                    flowers in full bloom, where most of the days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to November another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of
                                    snow.


                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu and transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu preparation for the trek and climb.<br>
                                    <b> Day 03:</b> Flight to Lukla 2,818 m and trek to Phakding 2,655 m - 04 hrs.<br>
                                    <b> Day 04:</b> Trek to Namche Bazaar 3,440 m - 06 hrs.<br>
                                    <b>Day 05:</b> In Namche for rest to support acclimatization with short hike.<br>
                                    <b>Day 06:</b> Trek to Thyangboche 3,867 m - 05 hrs.<br>
                                    <b>Day 07:</b> Trek to Pheriche 4,240 m - 05hrs.<br>
                                    <b>Day 08:</b> Rest day at Pheriche for acclimatization with short hike.<br>
                                    <b>Day 09:</b> Trek to Lobuche 4,890 m - 05 hrs.<br>
                                    <b>Day 10:</b> Trek to Everest Base Camp 5,364 m and at Gorakshep 5,175 m-06
                                    hrs.<br>
                                    <b>Day 11:</b> Climb on top Kalapathar 5,545 m walk to Chukung 4, 730 m- 06 hrs.<br>
                                    <b>Day 12:</b> Trek to Island Peak base camp in tented camps 4,970 m - 04 hrs.<br>
                                    <b>Day 13:</b> Trek Island Peak High / Advance Camp 5,400 m - 04 hrs.<br>
                                    <b>Day 14:</b> Climb Island Peak 6,160 m high and return to base camp - 08 hrs.<br>
                                    <b>Day 15:</b> Reserve and Contingency day in case of bad weather.<br>
                                    <b>Day 16:</b> Trek to Pangboche 3,900 m - 05 hrs.<br>
                                    <b> Day 17:</b> Trek to Namche Bazaar - 06 hrs.<br>
                                    <b>Day 18:</b> Trek to Lukla - 06 hrs.<br>
                                    <b>Day 19:</b> Fly back to Kathmandu and transfer to hotel.<br>
                                    <b>Day 20:</b> Reserve and contingency day in Kathmandu with individual
                                    activities.<br>
                                    <b> Day 21:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of Island
                                    Peak Climb.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks and climb local culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b>Day 02: In Kathmandu preparation for the trek and climb.</b><br>
                                    A free day preparation for Island Peak climb and treks around high Khumbu, with
                                    optional tour at places around Kathmandu world heritage sites of few hours, allows
                                    you getting ready with packing for next early morning flight to Lukla where our walk
                                    begins towards Island Peak Climb.<br><br>
                                    <b>Day 03: Flight to Lukla 2,818 m and trek to Phakding 2,655 m - 04 hrs.</b><br>
                                    Morning at the earliest before or after quick breakfast or one carry pack breakfast
                                    to save time reaching at Kathmandu domestic airport terminal, where a short scenic
                                    flight of 35 minutes reaches you at Lukla, a gateway Sherpa town at the height of
                                    2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage
                                    with porters to carry or on pack animals (Jopkyo a mix breed of normal cow and Yak).
                                    As walk leads past Lukla town heading down to Choplung small village and then around
                                    Dudh-Kosi River valley and gorge towards our first overnight stop at Phadking
                                    village near Dudh-Kosi a glacial river.<br><br>
                                    <b>Day 04: Trek to Namche Bazaar 3,440 m - 06 hrs.</b><br>
                                    Morning begins with gradual walk following Dudh Kosi river upstream past several
                                    Sherpa nice villages with short ups and down and then crossing a long suspension
                                    bridge to Monjo village, where Sagarmatha National Park starts entering into park
                                    areas past Jorsalle the last village before Namche Bazaar.
                                    From Jorsalle crossing bridge and into serene woodland of tall pines and
                                    rhododendron trees with climb on winding trail facing views of Kongde, Kusum
                                    Khanguru and Tharmasakru peaks as walk brings you into famous Namche Bazaar, which
                                    is more of a town rather than village with many good restaurants, hotels, lodge
                                    having number of shops and stores includes Bank and Post-Office.
                                    At Namche enjoy the comfort of nice cozy lodge, facing great views of snow capped
                                    peaks the closest lies Kongde peaks.<br><br>
                                    <b>Day 05: In Namche for rest to support acclimatization with short hike.</b><br>
                                    At Namche rest day for acclimatization before heading higher area, where short hike
                                    up to view point like towards Everest View Hotel takes nearly 3-4 hours both ways,
                                    offers excellent panorama of mountains with Everest and magnificent Amadablam
                                    In Namche visit museum where you can gain knowledge of Khumbu Sherpa, culture,
                                    flora-fauna as well of mountaineering expedition history with rest of the afternoon
                                    at leisure.<br><br>
                                    <b> Day 06: Trek to Thyangboche 3,867 m - 05 hrs.</b><br>
                                    With pleasant time at Namche walk leads to short climb and then on good wide trail
                                    facing views of Everest and Amadablam with array of peaks, as our route leads into
                                    woodland then descend towards Imjatse River to reach at a small place in
                                    Phunke-Tenga, possible lunch stop in the middle of a wood.
                                    Afternoon climb into forested areas facing views of Kangtenga-Tharmasarku and other
                                    peaks as walk brings at a wide open plateau in Thyangboche with its beautiful
                                    colorful Buddhist monastery, with time to visit the interior of the monastery where
                                    you can witness monks and priest on religious activities with prayer.
                                    A scenic spot surrounded by high peaks with views of Everest and magnificent looking
                                    Amadablam amidst forest of rhododendron-magnolia-oaks-juniper-birch and fir tree
                                    lines, truly a great place for overnight stop.<br><br>
                                    <b>Day 07: Trek to Pheriche 4,240 m - 05hrs.</b><br>
                                    From Thyangboche catching super panorama of mountains then head downhill within lush
                                    and green woodland to reach a metal bridge over Imjatse River, from here uphill to
                                    Pangboche, the last permanent Sherpa farm village before Island Peak and Everest
                                    base camp, as our walk leads to Somare for lunch with super views of Amadablam at
                                    close distance, and then heading on gradual path with short climb to reach at
                                    Pheriche windy valley for overnight stop.
                                    <b>  Day 08: Rest day at Pheriche for acclimatization and excursion.</b><br>
                                    At Pheriche located above 4,240 m within beautiful valley views of Amadablam
                                    including Lhotse and stunning Taboche and Cholatse peaks, Pheriche with many good
                                    fine lodges here you can visit HRA (Himalayan Rescue Association) office with a
                                    small medical post.

                                    A rest day for proper acclimatization here a short hike up to Nakarjung hills for
                                    better views of surrounding peaks with Mt. Makalu towards east adjoining.

                                    The high scenic hill above Imjatse valley offers super views of Island Peak, Lhotse
                                    Shar, Lhotse, Nuptse and Amadablam.<br><br>

                                    <b> Day 09: Trek to Lobuche 4,930 m - 05 hrs.</b><br>
                                    On leaving Pheriche walking on the valley end, and then with short climb to a ridge
                                    to reach at Thugla (Dugla), here trail from Pheriche valley and Dingboche joins
                                    towards Everest Base Camp.

                                    From Thugla stunning views of Taboche, Cholatse, Lobuche, Pumori, Amadablam, after a
                                    lunch break and rest climb leads up to terminal debris of Khumbu Glacier coming a
                                    across many memorials of unfortunate climbers died on mountain of Khumbu, from here
                                    continue walk to Lobuche for overnight stop.<br><br>

                                    <b> Day 10: Trek to Everest Base Camp 5364 m and at Gorakshep 5,180 m.</b><br>
                                    Morning adventure takes you at Everest Base Camp walking with short steep climb in
                                    between glacier and moraine, then reaching at Gorakshep, beneath lofty peak of
                                    Pumori and Kalapathar rocky hill.
                                    Walk continues towards Everest base camp, at base camp enjoy views of massive Khumbu
                                    Ice Fall and glaciers, after an exciting time with experience of a lifetime retrace
                                    the journey back to Gorakshep for overnight stop.<br><br>
                                    <b> Day 11: Hike to Kalapathar 5,545 m trek to Chukung 4,730 m - 05 hrs.</b><br>
                                    Early morning climb on top Kalapathar at above 5,545 m, highest point on trekking
                                    with magnificent views of mountains includes Mt. Everest at close distance, after a
                                    great time on top descend back to Gorakshep for breakfast and continue walk to
                                    Thugla, from here downhill to Imjatse valley with nice scenic walk towards the last
                                    human settlement at Chukung in the comfort of nice lodge.<br><br>

                                    <b> Day 12: Trek to Island Peak Base Camp 4,970m - 04 hrs.</b><br>
                                    After Chukung, from here onward in tents camping for few nights till climb is over,
                                    morning walk leads to our main highlight and goal to Island Peak base camp located
                                    in a scenic spot at Parshya Gab, here setting our camps with afternoon free to
                                    practice climbing with our expert guide.<br><br>
                                    <b> Day 13: Trek Island Peak High / Advance Camp 5,400 m - 04 hrs.</b><br>
                                    Great thrill and adventure begins after setting a high and advance camp above 5,400
                                    m high, to make the climb much easier and possible; leaving the base camp towards
                                    high camp climb above moraine towards an open gully, located between two well
                                    defined ridges to reach at high camp beneath a small hanging glacier above 5,400 m
                                    high.
                                    Here with time in the afternoon for final preparations for the big climb next early
                                    morning.<br><br>
                                    <b> Day 14: Climb Island Peak 6,160 m high and return to base camp - 08 hrs.</b><br>
                                    Climb starts early morning before dawn heading to the summit of Island peak at above
                                    6,160 m / 20,328 ft high, where the grade of the climb is PD + as per French and
                                    Swiss Alpine Climbing System, as our guide leads you across snow-covered glacier,
                                    avoiding serac and crevasses climb northwards over snow covered scree in the margin
                                    between a glaciers facing a top ridge.
                                    Climb continues crossing a gully with some stone with icefall danger, a steep snow
                                    and ice ramp leads you above 100 meters high (300 ft) to reach its main summit, as
                                    strenuous climb takes you on top our final and major highlight of the adventure.
                                    Enjoy views facing panorama of Mt. Everest 8,848 m, Lhotse 8,501 m, Mt. Amadablam
                                    6,856 m, Mt. Makalu 8, 463 m, Chamlang and Baruntse peaks, after a great climb
                                    feeling in high spirit descend safely on the same route back to base camp.<br><br>
                                    <b> Day 15: Reserve and Contingency day in case of bad weather.</b><br>
                                    Spare day as contingency in case of bad weather conditions, as Himalayan weather
                                    patterns sometimes changes even in good season, if all goes well using the extra day
                                    on the way back to Lukla with easier and short days on walks.<br><br>

                                    <b>Day 16: Trek to Pangboche 3,900 m - 05 hrs.</b><br>
                                    All climbers at base camp to clean up garbage and hand over to designated place or
                                    to SPPC personal (Sagarmatha Pollution Project Control) as well packing gears for
                                    Yaks to carry onward to Lukla.

                                    After getting organized our return route leads towards Imjatse valley with scenic
                                    views as walk proceeds towards Chukung to reach at Dingboche for lunch break, and
                                    then carry on trekking downhill to Pangboche village for overnight halt.<br><br>

                                    <b> Day 17: Trek to Namche Bazaar - 06 hrs.
                                    On completing our memorable and exciting </b><br>adventure Everest base camp and on top
                                    Kalapathar, an easy walk from here on without worry of high altitude sickness as
                                    route leads loosing elevation every hour of walks.

                                    From Pangboche reaching with a climb to beautiful Thyangboche and its colorful
                                    monastery reaching back into green woodland, from here a lovely walks of downs and
                                    ups and on nice scenic trail to Namche Bazaar for last night in Khumbu before Lukla.<br><br>

                                    <b> Day 18: Trek to Lukla - 06 hrs.</b><br>
                                    Our last final day of walks a longer treks to reach back at Lukla starting with long
                                    descend to reach a river valley and at Sagarmatha National Park entrance and exit to
                                    Monjo village, where walk continues past Phakding with last short climb to reach at
                                    last overnight stay in Lukla before flying back to Kathmandu.<br><br>
                                    <b>Day 19: Fly back to Kathmandu and transfer to hotel.</b><br>
                                    Depending upon flight time for Kathmandu, morning with last breakfast in Khumbu and
                                    Lukla transfer to air terminal named after Tenzing and Hillary, the first to conquer
                                    Mt. Everest in 1953.
                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to
                                    reach Kathmandu airport and then transfer back to your respective hotels.<br><br>
                                    <b> Day 20: Reserve and contingency day in Kathmandu for individual activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Lukla or in the beginning, where extra day
                                    allows you with time for yourself for individual activities or join in our exclusive
                                    tour or just relax after a great adventure on high Khumbu and Everest base camp.<br><br>
                                    <b> Day 21: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and experience on
                                    Island Peak Climb with last final day in Nepal where Mount Vision Trek staff and
                                    guide transfer you to Kathmandu international airport for your flight home ward
                                    bound.<br><br>

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>
                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>
                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



