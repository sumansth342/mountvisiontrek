<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1> Everest Base Camp -12 Days</h1>
            <p style="font-size:17px">“Walking at the foot of world highest Mt. Everest within Ice Falls and glaciers
                An adventure with daily scenic views of high peaks with impressive Sherpa culture
                Flying in and taking off on unique Lukla airstrip with sweeping mountain panorama
                Within World Heritage Sites of Sagarmatha National Park and its cool alpine woodland
                Explore native Sherpa villages adorned with interesting Buddhist monuments and monasteries
                Adventure in the heart of Mt. Everest with climb of high and scenic Kalapathar hill top”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘Enjoy the experience with an adventure to world highest base camp’</h3>
                                <p style="font-size: 17px;text-align: justify">Everest Base Camp Trekking, where your
                                    dreams and fantasy turns into reality joining with Mount Vision Treks in our scenic
                                    and cultural high altitude walks to world’s number one mountains base camp.
                                    Everest Base Camp Trekking offers great photogenic scenery with daily views of high
                                    snow capped peaks in the backdrop of local Sherpa impressive colorful custom and
                                    culture that fascinates you on the wonderful adventure.
                                    Mt. Everest listed as world’s highest mountains rises above 8,848 m and 29,028 ft
                                    high straddles the border between two countries with northern side in Tibet / China
                                    and Southern Face with Nepal, where most of the trek and expeditions are done from
                                    Nepalese side of Everest within high and beautiful Khumbu valley.
                                    Mt. Everest, a mountain of great respect and as well with several local names
                                    Sagarmatha in Nepal, while Sherpa and Tibetan call as Chomolungma meaning Great
                                    Mother in both native languages.
                                    History of Mt. Everest comes from British East India Company located in Kolkata of
                                    West Bengal state of India, where name was christened tribute to late Sir George
                                    Everest way back in mid 1940’s before it was known as Peak XIV.
                                    The first climb to Everest from Tibet side by British Expeditions in early 1921 led
                                    by Irving and Mallory whether they made or not remains as great mystery in
                                    mountaineering history, then later in May 29th 1953 where British Expedition led by
                                    late Sir John Hunt with legendry mountaineers Tenzing and Hillary conquered the
                                    summit of Mt. Everest.
                                    Since then Everest from Nepal side was highly exposed and made famous, as you will
                                    see on this walk visiting and staying in nice good lodges and villages on well
                                    established Everest Base Camp Trekking.
                                    Starting our adventure Everest Base Camp Trekking taking a scenic flight from
                                    Kathmandu to Lukla, where walk begins into high Khumbu valley as our route takes you
                                    past many interesting Sherpa villages in the shade of mighty Khumbu Himal peaks.
                                    Walking into Dudh Kosi River valley and gorge soon to enter large village and town
                                    of Namche Bazaar where trekkers can find many facilities required for the trek
                                    around Khumbu area, at Namche with rest day to support acclimatization before
                                    heading higher country.
                                    From Namche walk leads on scenic wide trail for some times facing views of snow-clad
                                    peaks with Everest and majestic Amadablam as our route leads downhill and then ups
                                    into serene alpine forest of tall rhododendron-oaks-magnolia-blue pines and silver
                                    fir to reach a small scenic plateau at Thyangboche.
                                    A beautiful spot with a colorful monastery worth a visit inside the religious sites
                                    surrounded by array of peaks with views of Mt. Everest and forest, where walk leads
                                    past tree lines into barren and windswept terrain as our route approach to high
                                    areas at Everest Base Camp.
                                    Enjoy terrific views of mighty peaks that surrounds at base camp situated close to
                                    Khumbu Ice-Falls and glaciers, after an exciting moment reaching our main highlight
                                    of the adventure then climb up to scenic Kalapathar view tops at above 5,545 m the
                                    highest point of the trek.
                                    From Kalapathar surrounded by great peaks with Mt. Pumori and Mt. Everest at mere
                                    distance and then head downhill towards Namche Bazaar as walk continues reaching
                                    back at Lukla for scenic air journey to Kathmandu, after a fabulous and memorable
                                    experience and adventure on Everest Base Camp Trekking.
                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br>Hotels and in lodge on walks.<br>
                                    <b>Trek Grade:</b> <br>Moderate to Adventurous due to high altitude terrain<br>
                                    <b>Area of Trek:</b><br>Nepal North Mid-East within Khumbu district of Everest<br>
                                    <b>Highest Altitude Gain:</b><br> On top Kalapathar 5,545 m and at Everest base camp
                                    5,365 m<br>
                                    <b>People and Culture: </b><br>Mainly populated by Sherpa the highlanders of Everest
                                    enriched with colorful Buddhist religion and impressive culture.<br>
                                    <b>Trek Duration:</b><br> 11 Nights and 12 Days (Lukla to Lukla)<br>
                                    <b>Average walks:</b><br> Minimum 4 hrs to Maximum 6 hrs or more.<br>
                                    <b>Total Trip:</b><br> 15 Nights and 16 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br>All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with chances of snow.
                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01:</b> Arrival in Kathmandu via respective international airline and transfer
                                    to hotel.<br>
                                    <b>Day 02:</b> In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b>Day 03:</b> Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m
                                    and trek to Phakding 2,655 m - 03 hrs.<br>
                                    <b>Day 04:</b> Trek to Namche Bazaar 3,440 m - 06 hrs.<br>
                                    <b>Day 05:</b> In Namche for rest to support acclimatization with short hike and
                                    excursion.<br>
                                    <b>Day 06:</b> Trek to Thyangboche 3,867 m - 05 hrs.<br>
                                    <b>Day 07:</b> Trek to Dingboche 4,365 m - 05 hrs.<br>
                                    <b>Day 08:</b> Another rest day at Dingboche for acclimatization and local hike.<br>
                                    <b>Day 09:</b> Trek to Lobuche 4,890 m - 05 hrs.<br>
                                    <b>Day 10:</b> Trek to Everest Base Camp 5,364 m back to Gorakshep 5,175 m - 06
                                    hrs<br>
                                    <b>Day 11:</b> Morning climb on top Kalapathar 5,545 m walk to Pangboche 3,900 m- 06
                                    hrs.<br>
                                    <b>Day 12:</b> Trek to Namche Bazaar - 06 hrs.<br>
                                    <b>Day 13:</b> Trek to Lukla - 06 hrs.<br>
                                    <b>Day 14:</b> Fly back to Kathmandu and transfer to hotel with free afternoon at
                                    leisure.<br>
                                    <b>Day 15:</b> Reserve and contingency day in Kathmandu with individual
                                    activities.<br>
                                    <b>Day 16:</b> Depart Kathmandu for international departure homeward bound.<br>
                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of Everest
                                    Base Camp Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>

                                    <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel.
                                    Afternoon at leisure and free for individual activities and getting ready with
                                    packing for next early morning flight to Lukla where our walk begins towards Everest
                                    base camp.<br><br>

                                    <b> Day 03: Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m and
                                        trek</b><br>
                                    to Phakding 2,655 m - 03 hrs.
                                    Morning at the earliest before or after quick breakfast or one carry pack breakfast
                                    to save time reaching at Kathmandu domestic airport terminal, where a short scenic
                                    flight of 35 minutes reaches you at Lukla, a gateway Sherpa town at the height of
                                    2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage
                                    with porters to carry or on pack animals (Jopkyo a mix breed of normal cow and Yak).
                                    As walk leads past Lukla town heading down to Choplung small village and then around
                                    Dudh-Kosi River valley and gorge towards our first overnight stop at Phadking
                                    village near Dudh-Kosi a glacial river.<br><br>

                                    <b> Day 04: Trek to Namche Bazaar 3,440 m - 06 hrs.</b><br>
                                    Morning begins with gradual walk following Dudh Kosi river upstream past several
                                    Sherpa nice villages with short ups and down and then crossing a long suspension
                                    bridge to Monjo village, where Sagarmatha National Park starts entering into park
                                    areas past Jorsalle the last village before Namche Bazaar.
                                    From Jorsalle crossing bridge and into serene woodland of tall pines and
                                    rhododendron trees with climb on winding trail facing views of Kongde, Kusum
                                    Khanguru and Tharmasakru peaks as walk brings you into famous Namche Bazaar, which
                                    is more of a town rather than village with many good restaurants, hotels, lodge
                                    having number of shops and stores includes Bank and Post-Office.
                                    At Namche enjoy the comfort of nice cozy lodge, facing great views of snow capped
                                    peaks the closest lies Kongde peaks.<br><br>

                                    <b> Day 05: In Namche for rest to support acclimatization with short hike and
                                        excursion.</b><br>
                                    At Namche rest day for acclimatization before heading higher area, where short hike
                                    up to view point like towards Everest View Hotel takes nearly 3-4 hours both ways,
                                    offers excellent panorama of mountains with Everest and magnificent Amadablam
                                    In Namche visit museum where you can gain knowledge of Khumbu Sherpa, culture,
                                    flora-fauna as well of mountaineering expedition history with rest of the afternoon
                                    at leisure.<br><br>

                                    <b> Day 06: Trek to Thyangboche 3,867 m - 05 hrs.</b><br>
                                    With pleasant time at Namche walk leads to short climb and then on good wide trail
                                    facing views of Everest and Amadablam with array of peaks, as our route leads into
                                    woodland then descend towards Imjatse River to reach at a small place in
                                    Phunke-Tenga, possible lunch stop in the middle of a wood.
                                    Afternoon climb into forested areas facing views of Kangtenga-Tharmasarku and other
                                    peaks as walk brings at a wide open plateau in Thyangboche with its beautiful
                                    colorful Buddhist monastery, with time to visit the interior of the monastery where
                                    you can witness monks and priest on religious activities with prayer.
                                    A scenic spot surrounded by high peaks with views of Everest and magnificent looking
                                    Amadablam amidst forest of rhododendron-magnolia-oaks-juniper-birch and fir tree
                                    lines, truly a great place for overnight stop.<br><br>

                                    <b> Day 07: Trek to Dingboche 4,365 m - 05 hrs.</b><br>
                                    Morning catching the views of surrounding peaks, where walk leads downhill past
                                    Deoboche within woodland to reach Imjatse River and then crossing over a steel
                                    bridge, from here end of green vegetation and tree lines as walk leads into
                                    windswept dry and arid country to reach Pangboche. The last permanent village on
                                    route Everest base camp where other settlement with lodge catering the needs of
                                    travelers and trekkers.
                                    Slowly walk leads past Pangboche heading higher with ups and down to a river, with
                                    last climb of the day for an hour to reach a scenic Imjatse valley at Dingboche
                                    facing North Side of Amadablam with views of Island Peak towards east direction.<br><br>

                                    <b> Day 08: Rest day at Dingboche for acclimatization and local hike.</b><br>
                                    Another rest day to support against high altitude symptoms with acclimatization day
                                    at Dingboche where one can hike further up towards Nakarjung Hill to get best
                                    panorama of surrounding peaks and of Imjatse valley.
                                    Afternoon rest and enjoy the views in the comfort of nice warm lodge with
                                    preparation for next day walks to Lobuche.<br><br>

                                    <b> Day 09: Trek to Lobuche 4,890 m - 05 hrs.</b><br>
                                    After a nice overnight and rest day at Dingboche, morning walk leads to a short
                                    climb to a ridge marked with Buddhist prayer flags and some monuments with views of
                                    high close peaks and then on good gradual path to walk.
                                    Our route leads on winding path above Pheriche valley with views of Cholatse and
                                    Taboche peaks as walk comes to a small place at Thugla, where massive Khumbu glacier
                                    melts into a raging river.
                                    After crossing a short bridge and climb to Thugla for possible lunch stop and rest,
                                    before heading higher up to Lobuche, having break and lunch start with slow climb to
                                    a ridge top lines with memorial of unfortunate mountaineers who risk their life
                                    climbing high mountains around Khumbu and Everest.
                                    From here a good walk with gradual up to reach at Lobuche located on the corner of a
                                    wide valley beneath peaks of Lobuche East and West.<br><br>

                                    <b> Day 10: Trek to Everest Base Camp 5,364 m back to Gorakshep 5,175 m - 06 hrs</b><br>
                                    Our adventure today leads to our main target and goal at Everest base camp, where
                                    morning walk follows a flat valley with slow climb to reach further and then a steep
                                    climb to reach a ridge near above Khumbu glacier.
                                    As walk continues over rocky moraines and ice to reach a scenic flat areas at
                                    Gorakshep located on the bottom of Kalapathar and Mt. Pumori on route Everest base
                                    camp, at Gorakshep with short break catching breathe in this high altitude with dry
                                    air.
                                    From Gorakshep following our lead guide heading further east to reach at much
                                    awaited destination Everest Base Camp with marvelous views of Khumbu Ice-Falls,
                                    glaciers looking above peaks of Nuptse, Everest and Lhotse, in high mountaineering
                                    season of April to May this grand spot flooded with tents of expeditions.
                                    After exciting time having achieved our goal walk back to Gorakshep for overnight
                                    stop before heading downhill back to Lukla with climb of Kalapathar.<br><br>

                                    <b> Day 11: Morning climb on top Kalapathar 5,545 m walk to Pangboche 3,900 m- 06
                                        hrs.</b><br>
                                    Starting early morning before breakfast climb to the top of rocky hill at above
                                    5,545 m the highest spot of the adventure, where you will be rewarded with
                                    overwhelming panorama of giant surrounding peaks with close views of Everest and Mt.
                                    Pumori.
                                    After a mesmerizing moment on top Kalapathar descend to Gorakshep for breakfast and
                                    the carry on trekking down to Pheriche valley and then further to reach at Pangboche
                                    village for overnight stop with great scenic walk of the day.<br><br>

                                    <b> Day 12: Trek to Namche Bazaar - 06 hrs.</b><br>
                                    On completing our memorable and exciting adventure Everest base camp and on top
                                    Kalapathar, an easy walk from here on without worry of high altitude sickness as
                                    route leads loosing elevation every hour of walks.
                                    From Pangboche reaching with a climb to beautiful Thyangboche and its colorful
                                    monastery reaching back into green woodland, from here a lovely walks of downs and
                                    ups and on nice scenic trail to Namche Bazaar for last night in Khumbu before Lukla.<br><br>

                                    <b> Day 13: Trek to Lukla - 06 hrs.</b><br>
                                    Our last final day of walks a longer treks to reach back at Lukla starting with long
                                    descend to reach a river valley and at Sagarmatha National Park entrance and exit to
                                    Monjo village, where walk continues past Phakding with last short climb to reach at
                                    last overnight stay in Lukla before flying back to Kathmandu.<br><br>

                                    <b> Day 14: Fly back to Kathmandu and transfer to hotel with free afternoon at
                                        leisure.</b><br>
                                    Depending upon flight time for Kathmandu, morning with last breakfast in Khumbu and
                                    Lukla transfer to air terminal named after Tenzing and Hillary, the first to conquer
                                    Mt. Everest in 1953.
                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to
                                    reach Kathmandu airport and then transfer back to your respective hotels.<br><br>

                                    <b> Day 15: Reserve and contingency day in Kathmandu with individual activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Lukla or in the beginning, where extra day
                                    allows you with time for yourself for individual activities or join in our exclusive
                                    tour or just relax after a great adventure on high Khumbu and Everest base camp.<br><br>

                                    <b> Day 16: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories of Everest Base Camp
                                    trekking, with last final day in Nepal where Mount Vision Trek staff and guide
                                    transfer you to Kathmandu international airport for your flight home ward bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/ebc2.JPG">
                                                        <img src="assets/images/ebc2.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/ebc3.JPG">
                                                        <img src="assets/images/ebc3.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/ebc4.jpg">
                                                        <img src="assets/images/ebc4.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/ebc5.jpg">
                                                        <img src="assets/images/ebc5.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $1800</strike></strong>
                        </h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1724</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                    </center>
                    <br>
                    <a href="booking.php?id=Everest Base Camp" class="btn btn-primary btn-outline btn-block">Book
                        Now</a></p>
                    <a href="enquiry.php?id=Everest Base Camp" class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



