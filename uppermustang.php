<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>UPPER MUSTANG JOURNEY -17 Days</h1>
            <p style="font-size:17px">“Scenic and cultural journey on Old Trans Himalaya Salt Trade Route of Nepal and
                Tibet
                Combination of panoramic flight, exciting drive and walks to beautiful Upper Mustang
                Explore hidden treasures around remote parts of Nepal Far North Western Himalaya
                Magnificent country with amazing scenery an extension of vast Tibet South Western plateau
                Around Upper Mustang traditional villages enriched with Buddhist and old Bon sect practice
                Journey into former walled kingdom of Lo-Manthang a forbidden country for many centuries”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘a scenic and cultural journey on foot and drive to former walled kingdom of
                                    Lo-Manthang’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Upper Mustang Journey takes you to most amazing country truly out of this world,
                                    within high arid terrain of moonscapes, where road also leads to border of Tibet /
                                    China beyond Upper Mustang through high Karo-La pass on this ancient Trans-Himalaya
                                    Salt Trade Route that is still active to this day.
                                    Upper Mustang Journey a combination of short scenic flight from beautiful Pokhara to
                                    Jomsom headquarter town of whole Mustang region, where walks and drive in four
                                    wheels completes this wonderful adventure exploring its impressive villages and
                                    scenic countryside.
                                    Upper Mustang, where in ancient times known as the land of Lo, hidden beyond high
                                    Himalaya of Dhaulagiri and Annapurna mountains where its territory extends to Dolpo
                                    areas of Nepal Far Western region, a great place to experience once in your
                                    life-time adventure on this marvelous Upper Mustang Journey.
                                    Upper Mustang, due to its close border with Tibet where you will witness interesting
                                    culture and custom of Tibetan heritage of both Buddhism religions with ancient
                                    practice of Bon sects which is slowly fading at present, similar traditions and cult
                                    to native North American Indians and Laplanders.
                                    The Upper Mustang area was completely forbidden and restricted for many centuries to
                                    outside world travelers, been opened since last three decades where you can witness
                                    its beautiful country enrich with impressive traditions and custom exploring its
                                    cultural villages and interesting monasteries.
                                    Starting our wonderful Upper Mustang Journey taking a sweeping flight from scenic
                                    and touristic spot Pokhara to reach a nice and lovely town in Jomsom, where walks
                                    leads past Kali-Gandaki River Valley on the road to reach higher areas of Upper
                                    Mustang.
                                    As walk and short drive leads past several interesting villages of Kagbeni, Chusang,
                                    Samar and Charang to reach our main highlight into former forbidden walled kingdom
                                    of Lo-Manthang the seat of many Mustang Raja / King, where you can visit its fort
                                    like palace and other interesting sites and impressive monasteries.
                                    After an enjoyable time walking towards remote parts of Upper Mustang at Yara with
                                    its ancient Luri monastery, then heading up through higher trail to reach at holy
                                    Muktinath a wonderful spot with many good lodges within this sacred place for both
                                    Hindu and Buddhist followers, offers superb views of Dhaulagiri and Nilgiri’s.
                                    From this holy place of Muktinath where our walk ends taking a drive back to Jomsom
                                    for scenic flight to land at picturesque Pokhara by its serene Phewa Lake, after a
                                    great memorable experience and exciting time in Upper Mustang Journey.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br> Hotels and lodge on walks and drives.<br>
                                    <b> Trip Grade: </b><br> Moderate to Adventurous.<br>
                                    <b> Area of Journey: </b><br> Nepal Far North West Himalaya around Mustang
                                    region.<br>
                                    <b>Highest Altitude Gain: </b><br> Crossing over Nyi La Pass above 3,950 m high.<br>
                                    <b>People and Culture: </b><br> Populated by Mustang people of Tibetan origin with
                                    Buddhist religion and ancient Bon sect of pre Buddhism with
                                    impressive colorful culture.
                                    Lower Mustang dominated by Thakali tribes with some Magar-
                                    Gurung people interwoven with Buddhism and Hindu religions
                                    with various interesting cultures.<br>
                                    <b>Trip Duration: </b><br> 12 Nights and 13 Days (with drive and walks both
                                    ways)<br>
                                    <b> Average walks:</b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip: </b><br> 17 Nights and 18 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br> April to November includes monsoon months of July and August
                                    as Upper Mustang falls within rain-shadow blocked by high
                                    Himalaya range of mountains.
                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: </b>Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02: </b>In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b> Day 03: </b>Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)<br>
                                    <b> Day 04:</b> Morning flight to Jomsom 2,715 m and trek to Kagbeni 2,830 m - 04
                                    hrs.<br>
                                    <b>Day 05:</b> Drive to Chusang 2,820 m and start walk to Samar 3,290 m - 05
                                    hrs.<br>
                                    <b>Day 06:</b> Trek to Ghiling 3,570 m - 06 hrs.<br>
                                    <b> Day 07:</b> Drive to Ghami and walk to Dhakmar 3,520 m - 05 hrs.<br>
                                    <b> Day 08:</b> Trek to Lo-Manthang 3,810 m - 05 hrs.<br>
                                    <b>Day 09:</b> At Lo-Manthang for local excursion and explore around its cultural
                                    sites.<br>
                                    <b>Day 10: </b>Trek to Yara 3,650 m - 06 hrs.<br>
                                    <b>Day 11: </b>At Yara explore around Luri Gompa.<br>
                                    <b>Day 12: </b>Trek to Tangbe 3,240 m - 06 hrs.<br>
                                    <b>Day 13: </b>Trek to Tetang 3,040 m - 06 hrs.<br>
                                    <b>Day 14:</b> Trek to Muktinath 3,795 m - 06 hrs.<br>
                                    <b> Day 15:</b> Drive to Jomsom 2,715 m- 02 hrs.<br>
                                    <b> Day 16: </b>Fly from Jomsom to Pokhara with rest of the day free at leisure.<br>
                                    <b> Day 17: </b>Drive or fly back to Kathmandu and transfer to hotel with free
                                    afternoon.<br>
                                    <b> Day 18: </b>Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                        On arrival where you will be well received by our staff and guide and then
                                        transfer to your respective hotels in the centre of the city in Kathmandu, after
                                        checking into your rooms getting refreshed from jet-leg join with other members
                                        of Upper Mustang Journey.
                                        Our guide / leader will brief with information about the hotels-including detail
                                        of trekking regarding local lodge, walks, view, history and culture with few
                                        important do’s and don’ts while you are with us having enjoyable times.
                                        Evening includes a nice dinner in a pleasant Nepalese restaurant with local
                                        cultural programs of all parts of the country to enlighten the environment along
                                        with your dinner.<br><br>
                                    <b>Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                        A free day to prepare for the upcoming treks to Upper Mustang, morning an
                                        optional sightseeing tour at places of great interest, exploring around
                                        Kathmandu world heritage sites for few hours and back at hotel, afternoon free
                                        for individual activities and preparations for next morning overland journey to
                                        reach Nepal Mid-West at Pokhara for Upper Mustang.<br><br>
                                    <b>Day 03: Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)</b><br>
                                        Morning after breakfast taking an interesting drive on Nepal main highway that
                                        connects with various parts of the country, where drive leads around warmer low
                                        areas past farm areas with villages and towns, following Trisuli River for
                                        sometime as drive heads further west to reach at beautiful and scenic Pokhara
                                        city by its serene Phewa lake for overnight stop.
                                        Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate
                                        but with close views of towering and massive Annapurna Himalaya and majestic
                                        Machhapuchare Himal known as Fish Tail peak.
                                        Options by air to Pokhara take about 30 minutes of scenic air flight from
                                        Kathmandu.<br><br>
                                    <b> Day 04: Morning flight to Jomsom 2,715 m and trek to Kagbeni 2,870 m - 04 hrs.</b><br>
                                        After overnight stop in Pokhara, early morning transfer to Pokhara airport for
                                        short scenic flight to reach at Jomsom, a large town as well headquarter and
                                        administrative town of whole Mustang area, during air borne close views of
                                        Annapurna and Dhaulagiri Himalaya range.

                                        On arrival at Jomsom with time around this interesting town which is famous for
                                        delicious apples and its products, Jomsom home to Thakali tribe interwoven with
                                        Buddhism religion and culture. After a break here walk heads towards
                                        Kali-Gandaki River valley, a good walk of 2-3 hours reaches you at Kagbeni
                                        village, where strong influence of Tibetan culture dominates from here onward.
                                        Kagbeni is the start and end point of Upper Mustang trekking and route to Tibet
                                        border by Karo-la pass.

                                        Kagbeni an interesting village of strong Tibetan influence in culture,
                                        traditions and way of daily life, afternoon time to explore this impressive
                                        village and to observe the local people and around its old monastery.<br><br>

                                    <b> Day 05: Drive to Chusang 2,820 m and start walk to Samar 3,290 m - 05 hrs.</b><br>
                                        From Kagbeni adventure begins taking a drive beyond Kagbeni village, towards
                                        uphill on leaving Kali-Gandaki River behind, as drive leads within arid and
                                        barren country with occasional stops, heading past small villages as ride
                                        progress leading beyond Tangbe, with few hour of good journey brings you at
                                        Chusang line up with few trees amidst the red earth surrounding. Chusang lies at
                                        the confluence of the Narsang stream and the Kali Gandaki River.
                                        From Chusang / Tsuang, starts our actual walk leads to Tshle or Chele village
                                        after crossing a stream through broken canyons and ruin castle walls on the
                                        surrounding cliffs, across the river from Chusang are some spectacular red
                                        eroded cliffs above the mouths of inaccessible caves.
                                        After crossing the small bridge an uphill walk for an hour brings you at Chele
                                        village, where walk gets steeper for an hour up at a ridge above 3,130m, from
                                        here great views of Gyagar across a huge canyon.
                                        Trek continues with a slow climb on the windswept terrain till the pass is
                                        reached at 3,540 m and then gradual descent leads you at Samar for overnight
                                        stop.<br><br>
                                    <b>Day 06: Trek to Ghiling 3,570 m - 06 hrs.</b><br>
                                        Morning walk leads to a short climb above Samar village to a ridge, with
                                        downhill heading to a large gorge past a Chorten painted red, black, yellow and
                                        white-all the colors are made from local rocks. The journey continues into
                                        another valley with juniper trees, after crossing the stream the walk follows
                                        uphill path to a ridge at 3,800m, the trail climbs over to another pass, from
                                        here the route descends to Shyangbochen, a small settlement with few tea shops
                                        at 3,710m. After a good break here, the trek continues to a gentle climb to a
                                        pass at 3,770m entering towards a huge valley, from here taking the right
                                        downhill path towards our overnight stop at Geling with its large fields of
                                        barley on the outskirt of the village.<br><br>
                                    <b>Day 07: Drive to Ghami and walk to Dhakmar 3,520 m - 05 hrs.</b><br>
                                        From Geling, our route climbs gradually unto fields to the centre of a valley,
                                        past below Tama Gung settlements walk continues with long climb across to a
                                        valley and then cross Nyi La pass 3,950m.
                                        The highest point of the trek as well the southern boundary of Lo. From here a
                                        gradual descend for half hour brings to junction; the right path leads direct to
                                        Tsarang while the left leads to Ghami village for overnight stop one of the main
                                        major of Lo region.<br><br>
                                    <b>Day 08: Trek to Lo-Manthang 3,810 m - 05 hrs.</b><br>
                                        Walk today leads to our major highlight and destination in walled former kingdom
                                        at
                                        Lo-Manthang, as morning begins with short descend crossing Charang-Chu River
                                        with steep climb to a ridge, then past Tholung valley. The trail diverts north
                                        with gentle climb as walk ascends to a large Chorten, which marks the boundary
                                        between Ghami and Tsarang with Lo.
                                        Walk follows crossing a stream, reaching a great wide windswept landscape,
                                        finally reaching on top of a hill at 3,850 m, offering grand view of walled
                                        former kingdom of Lo Mananthang, where short descent brings you to a stream that
                                        outlines the walled kingdom and a village, form here a short climb on a plateau
                                        that leads you to southern wall of Lo Manthang village.
                                        The only entrance to the village is from the northeast corner, so one has to
                                        circuit the wall to the main entrance gate sometimes.<br><br>
                                    <b>Day 09: At Lo-Manthang for local excursion and explore the village-town.</b><br>
                                        At Lo-Manthang with full day to explore around amazing place, which is a large
                                        village about 150 houses plus number of resident for the lamas, the houses are
                                        closely packed, and the palace and monasteries are in the bottom portion in this
                                        vertical part of the L shaped houses.
                                        The school, health post, police post and several important chhorten are located
                                        outside the walls to the north of the gate and east of the monastic part of the
                                        city. Despite the apparent squalor of Lo Manthang, the village is prosperous and
                                        maintains a strong sense of community.
                                        Although the local call them as Lobas, people from Lo, of strong Tibetan
                                        influence and practice similar culture. Before strong trade with Tibet was
                                        disrupted, all of the salt and wool trade on the Kali Gandaki passed through Lo
                                        Manthang, and this brought a substantial amount of money to this great village
                                        town. The Mustang raja's palace stands an imposing 4 storey building in the
                                        centre of the city. It is the home of late Raja, Jigme Parbal Bista, and the
                                        rani (queen), who was from the noble family of Lhasa.<br><br>
                                    <b>Day 10: Trek to Yara 3,650 m - 06 hrs.</b><br>
                                        After breakfast return journey using the same route for some hours with pleasant
                                        walk, where our trail diverts heading east-ward with steep descends leading to a
                                        small village of Dhi situated on the bank of the Phuyung Khola.
                                        The trail crosses the small wooden bridge over the Phuyung Khola and climbs
                                        steeply to reach the small village of Yara where the Buddhist player flags
                                        greets you, explore the incredible erosion landscape on the way to Yara village.<br><br>
                                    <b>Day 11: At Yara explore around Luri Gompa.</b><br>
                                        A rest day at Yara to explore around this amazing country where few hours of
                                        walk from Yara village brings to a small settlement of Luri Gompa and explore
                                        Luri Gompa and caves of full of Gompas (monastery and shrines).<br><br>

                                    <b>Day 12: Trek to Tangbe 3,240 m - 06 hrs.</b><br>
                                        After an interesting and fascinating time above high country of Upper Mustang
                                        and in Luri Gompa with historical and impressive phenomenal monastery, starting
                                        morning on a high trail through grass less and barren, arid terrain with just
                                        bare red eroded hills weathered by strong winds and snows, and then walk slowly
                                        encountering a small ridge top to head downhill to Tangbe village at 3,370 m
                                        high, this is a moderate size village of about 30 houses, the houses are closely
                                        attached.
                                        On the rooftops piles of dry big logs and woods stored for many years as status,
                                        which reflects prosperity of the society which is common traditions in this arid
                                        areas of Mustang and in Tibet where woods are very scarce.<br><br>

                                    <b> Day 13: Trek to Tetang 3,040 m - 06 hrs.</b><br>
                                        Walking on barren wilderness of red earth landscapes in complete ambiance the
                                        path is gentle for few hours with views of distant snow capped peaks and the
                                        heading downhill and up at Tetang a small village for overnight stop around Yak
                                        pasture field.<br><br>

                                    <b>Day 14: Trek to Muktinath 3,760 m - 06 hrs.</b><br>
                                        Walk from Tetang to Muktinath, heading towards Lower Mustang a long day before
                                        Jomsom, as the morning begins on a gradual winding trail where there are several
                                        routes to reach at holy Muktinath while other trails leads to Kagbeni.

                                        Our direction heads north east, past temporary huts and shelters of Yak herders
                                        facing super views of Nilgiri’s peaks and Dhaulagiri range, trail winds on the
                                        gradual with much wider trail to Muktinath for overnight stop, afternoon visit
                                        the holy Muktinath temple and its premises.

                                        Muktinath, a sacred pilgrimage area where people from Nepal and India visits
                                        here to offer prayer and to Liberate from bad deeds, as the Mukti means to be
                                        free, here natural elements are displayed in miraculous form-flickering blue
                                        flames of natural methane gas burn on water, stone and earth an offering said to
                                        have been first lighted by Brahma, the Hindu creator, this natural wonder have
                                        awed man from the beginning, for Muktinath sanctity goes back into antiquity of
                                        2,300 years ago.

                                        This area is equally holy for Hindu and Buddhist, around Kaligandaki and
                                        Muktinath pilgrims search for black shiny fossils called Shaligram (ammonite),
                                        found in abundance around this area.<br><br>

                                    <b>Day 15: Drive to Jomsom 2,715 m- 02 hrs.</b><br>
                                        From holy sites of Muktinath with views of Dhaulagiri range, morning where our
                                        walks ends with short overland ride to reach at lower area of Mustang, as
                                        motorable road has been built where trek is disturbed by movement of vehicles,
                                        so taking a drive instead of walking.
                                        Drive leads towards wide Kaligandaki River within its windswept landscapes to
                                        reach at large town in Jomsom, headquarter town of Mustang area, which is home
                                        of Thakali tribe been on this windswept country for hundreds of years.

                                        Jomsom located on old Trans-Himalayan Salt Trade Route of Nepal to Tibet, which
                                        extends towards Upper Mustang over Karo-La pass, Jomsom and Kaligandaki area
                                        famous for delicious apples and its product also.<br><br>

                                    <b>Day 16: Fly from Jomsom to Pokhara with rest of the day free at leisure.</b><br>
                                        After a great time on high hills of Annapurna, where early morning at Jomsom a
                                        short to its airport terminal for scenic smooth flight to land at picturesque
                                        Pokhara city for last overnight by its Phewa Lake side.<br><br>

                                    <b>Day 17: Drive or fly back to Kathmandu and transfer to hotel with free
                                        afternoon.</b><br>
                                        With enjoyable time in high both sides of Annapurna Himalaya where morning
                                        overland journey takes you on the same route to reach back at Kathmandu, after a
                                        wonderful time and experience on Upper Mustang within Nepal Far West with rest
                                        of the afternoon free at leisure.
                                        For people with options by air to Kathmandu 30 minutes of scenic flight from
                                        Pokhara.<br><br>
                                    <b> Day 18: Depart Kathmandu for international departure homeward bound.</b><br>
                                        Finally approaching last day in Kathmandu after a great adventure and wonderful
                                        experience on Upper Mustang Journey, with your last final day in Nepal where
                                        Mount Vision Trek staff and guide transfer you to Kathmandu international
                                        airport for your flight home ward bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=UPPER MUSTANG JOURNEY"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=UPPER MUSTANG JOURNEY" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



