<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css" />

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">



            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>TAMANG HERITAGE TRAILEKKING- 11 Days</h1>
            <p style="font-size:17px">“Trekking within cultural villages of Tamang of great age-old impressive heritage
                Glorious mountain panorama of Ganesh and Langtang mountain range on daily walks
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Explore scenic country on least trekked route around remote corners of Langtang Himal
                Visit and interact within traditional farm villages of Tamang the hill tribe of Nepal Himalaya”





            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen" >Overview</button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day Itinerary</button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include Excludes</button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘a great country walk around heritage villages with grand views of Langtang and Ganesh Himal’</h3>
                                <p style="font-size: 17px;text-align: justify">Tamang Heritage Trail where walk leads you high and cool alpine areas of Langtang Himal range close to massive Ganesh Himal, where local Tamang calls Yangra of Tibetan origin as Tamang an ancient tribe settled around high mountains of Nepal Himalaya since past thousand years of history.
                                    Tamang Heritage Trail a journey of marvelous walk exploring medvial time period farm villages where old heritage and colorful culture are lively as it was hundreds of centuries, where Tamang tribe drifted from inner Mongolia and Tibet, actually a horsemen and cavalry soldiers in early days, as the name Ta means horse at present living a farm life with great cultivated fields and terraces as well breeding horses and raising domestic animals.
                                    The Tamang are also skilled craftsman which you can witness on this pleasant and wonderful Tamang Heritage Trail where houses adorned with Buddhist symbols and intricate carving on doors and windows of their houses with colorful prayer flags outside their homes.
                                    Tamang Heritage Trail begins after a drive towards North of Kathmandu around Langtang Himal region, the closest mountain range from the capital and as well to Tibet-China border, where the our walk leads close to the frontier of Rasuwa-Gadhi.
                                    On reaching Dhunche and Syabrubesi, where walk leads North West on the off beaten tracks least traveled by other main flow of trekkers visiting Tamang lovely traditional farm villages of interesting cultures around Gatlang-Tatopani (a place with natural hot-spring) and Thuman.
                                    Walking into serene cool forest of rhododendron-pines and fir tree lines with tremendous scenery of beautiful landscapes and mountain views of Ganesh-Manaslu and Langtang Himal range, around Tamang villages with time to interact and immerse within its age-old heritage, then our journey completes driving back to Kathmandu, after a memorable experience and enjoyable time on Tamang Heritage Trail Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                   <b> Mode of Trekking:</b><br>Hotels and lodge with local Home Stay.<br>
                                    <b>Trek Grade:</b><br>Moderate with ups and downhill walks.<br>
                                    <b> Area of Trek:</b><br>	Nepal Central Himalaya around Langtang region.<br>
                                    <b> Highest Altitude Gain:</b><br>	At Nagthali above 3,154 m high.<br>
                                    <b> People and Culture:</b><br>	Mainly populated by Tamang people enriched with
                                    Buddhist religion with impressive traditional culture.<br>
                                    <b> Trek Duration:</b><br>		07 Nights and 08 Days (with drive both ways)<br>
                                    <b> Average walks:</b><br>	Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip:</b><br>10 Nights and 11 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br>All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.


                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                   <b> Day 01:</b>	Arrival in Kathmandu via respective international airline and transfer to hotel.<br>
                                    <b> Day 02:	</b>	In Kathmandu with optional sightseeing tour and preparation for the trek.<br>
                                    <b> Day 03:</b>		Drive to Syabrubesi village 1,475 m via Dhunche town 1,945 m - 06 hrs.<br>
                                    <b> Day 04: </b>	 	Trek to Gatlang 2,240 m -05 hrs.<br>
                                    <b> Day 05: </b>		Trek to Tatopani 2,595 m - 05 hrs.<br>
                                    <b> Day 06:</b>		Trek to Thuman 2,350 m via Nagthali 3,154 m - 04 hrs.<br>
                                    <b> Day 07:</b>	 	Trek to Briddim 2,229 m -05 hrs.<br>
                                    <b> Day 08:</b>	 	Trek to Lama Hotel 2,470 m - 06 hrs.<br>
                                    <b> Day 09:</b>		Trek to Syabrubesi 1,475 m - 06 hrs.<br>
                                    <b> Day 10:</b>		Drive to Kathmandu and transfer to hotel with free afternoon.<br>
                                    <b> Day 11:</b>		Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent"style="background-color: whitesmoke">
                                <p><b>Day 01:	Arrival in Kathmandu via respective international airline and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer to your respective hotels in the centre of the city in Kathmandu, after checking into your rooms getting refreshed from jet-leg join with other members of Tamang Heritage Trail Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of trekking regarding local lodge, walks, view, history and culture with few important do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural programs of all parts of the country to enlighten the environment along with your dinner.<br><br>

                                    <b>Day 02:	In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional sightseeing tour at places of great interest, exploring around Kathmandu world heritage sites for few hours and back at
                                    hotel, afternoon free for individual activities and preparations for next morning overland journey to reach at Pokhara around Nepal Mid-West region for Tamang Heritage Trail around Langtang
                                    Himal.<br><br>

                                    <b>Day 03:	Drive to Syabrubesi village 1,475 m via Dhunche town 1,945 m - 06 hrs.</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal North-West highway that connects with Langtang Himal of Nuwakot and Rasuwa district as well to the border of Tibet-China, where drive leads around warmer low areas past farm areas with villages and towns, following higher winding road as drive heads further west to reach at nice large town of Dhunche, headquarter of Rasuwa district.
                                    From here on downhill winding drive to reach our overnight stop at Syabrubesi village within home to large number of Tamang native tribe of the region.<br><br>

                                    <b>Day 04:  	Trek to Gatlang 2,240 m -05 hrs.</b><br>
                                    Trek starts of the first day heading due west avoiding the motorable road, using the old and original walking path, and then climb up to a hill with scattered farm houses and terraces, then enter into cool shade of forest within tall rhododendron-pines-oaks and fir tree lines.
                                    As walk leads to more uphill in the forested path to reach a nice clearing within a large village at Gatlang, where you will be greeted in traditional Tamang Buddhism custom and then
                                    transfer to your lodge or in village community home-stay huts, with time to immerse around this nice village full of lively culture.<br><br>

                                    <b> Day 05: 	Trek to Tatopani 2,595 m - 05 hrs.</b><br>
                                    Morning begins leaving interesting Gatlang village heading North West walking on neat stone paved steps for a while, past terraced farm fields of millets and barley with ever present views of massive Ganesh Himal with Manaslu and Langtang Himal.
                                    Walk continues to reach our overnight stop at Tatopani village, a place with hot natural spring, means Tato-Pani in Nepalese where village named after.

                                    Here enjoy leisure afternoon visiting its nice Tamang houses of intricate carving on windows and doors and then refresh yourself in the hot spring located near to the village.<br><br>

                                    <b> Day 06:	Trek to Thuman 2,350 m via Nagthali 3,154 m - 04 hrs.</b><br>
                                    From Tato-Pani after enjoying its warm pools village hospitality, as morning walk leads on the trail towards Tibet border for few hours to reach at Thuman village via high Nagthali ridge.

                                    A gentle walk follows on gradual winding path towards north and then with climb for some hours encountering few short descend overlooking views of Ganesh Himal and Langtang Himal, as walk leads enters into cool shade of alpine woodland then reaching at Nagthali hill top the highest point of the journey.

                                    Enjoying marvelous views of peaks and landscapes, and then walk continues to our overnight stop at Thuman village with ample time to explore around this nice remote farm village of immense
                                    culture and age-old heritage.<br><br>

                                    <b>Day 07: 	Trek to Briddim 2,229 m -05 hrs.</b><br>
                                    After a great time around this farm village with amazing views of snow peaks, morning walk starts with a climb to a ridge with more super scenery of surrounding green landscapes dotted with
                                    villages and of mountains.

                                    As walk progress heading into nice forest full of rhododendron, pines and oaks trees and then slowly our day comes to an end on reaching at Briddim village for overnight stop.<br><br>

                                    <b>Day 08: 	Trek to Lama Hotel 2,470 m - 06 hrs.</b><br>
                                    Morning walks begins after leaving Briddim village heading back to Syabrubesi as our route leads on quiet path into deep forested areas within rich vegetation with chances of spotting some wild-life around this areas, famous for elusive and endangered Red Panda that habitats.
                                    As walk progress walk leads to short ups and downhill to reach a main trail of Langtang at Lama Hotel located by the upper Trisuli River known as Langtang Khola (stream).<br><br>
                                    <b>Day 09:	Trek to Syabrubesi 1,475 m - 06 hrs.</b><br>
                                    End of our lovely walk today to reach road-head at Syabrubesi village, as morning starts with slow downhill walk into forest to reach a place called River-side with few huts serving as tea-house and refreshment.
                                    From here walking past forested area with long descend to reach a bridge, after crossing over on nice pleasant trail to Syabrubesi for last overnight stop around Langtang Himal region.<br><br>
                                    <b> Day 10:	Drive to Kathmandu and transfer to hotel with free afternoon.</b><br>
                                    With enjoyable time in high mountain areas of Langtang Himal where morning taking an overland journey to reach back at Kathmandu, after a wonderful time and experience on Langtang Himal Panorama trekking with rest of the afternoon free at leisure.<br><br>
                                    <b>Day 11:	Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memorable time and experience on Tamang Heritage Trail and with your last final day in Nepal where Mount Vision Trek staff and guide
                                    transfer you to Kathmandu international airport for your flight home ward bound.<br><br>

                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg" style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg" style="height:200px;width: 200px;" />
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent"style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1" >
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box" >
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1500</strike></strong></h3>
                        <h3> <p class="price "style="color:white;"><span>$1250</span> <small>/ person</small></p></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png"  style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>   </center><br>



                    <a href="booking.php?id=Tamang Heritage" class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Tamang Heritage"class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {font-family: Arial;}

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    function showImage(src){
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>



