<?php
require "inc/connect.inc.php";
include('navigation.php');
?>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/untitled1.png); ">
                <div class="overlay"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                            <div class="slider-text-inner text-center">
                                <h2>by vision.com</h2>
                                <h1>Find Hotel</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>

<div class="vision-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="wrap-division">

                        <div class="panel">
                            <div class="panel-body">


                                <link rel="stylesheet"
                                      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                                <style>
                                    .checked {
                                        color: orange;
                                    }
                                </style>

                                <?php
                                require 'inc/connect.inc.php';
                                $guests = "";
                                $query_run = mysqli_query($con, "SELECT * FROM tb_review LIMIT 8");
                                $ads_count = mysqli_num_rows($query_run);

                                if ($ads_count > 0) {
                                    while ($row = mysqli_fetch_array($query_run)) {

                                        $ads_id = $row['id'];
                                        $name = $row['name'];
                                        $image = $row['image'];
                                        $saying = $row['saying'];
                                        $star = $row['star'];
                                        $guests .= '<div class="item">
                        <div class="testimony text-center">
                            <span class="img-user" style="background-image: url(' . $image . ');"></span>
                            <span class="user">' . $name . '</span>';
                                        for($i=0;$i<$star;$i++) {
                                            $guests .='<span class="fa fa-star checked"></span>';
                                                }
                                        for($i=0;$i<5-$star;$i++) {
                                            $guests .='<span class="fa fa-star"></span>';
                                        }

 

                            $guests .='<blockquote>
                                <p>' . $saying . '</p>
                            </blockquote>
                        </div>
                    </div>';
                                            }
                                }

                                ?>
                                <div id="vision-testimo" class="vision-light-gr" style="padding-bottom: 1px">
                                    <div class="container">
                                        <div class="row">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2 animate-box">
                                                <div class="owl-carous2">
                                                    <?php echo $guests ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-md-offset-5">
                                        <ul class="pagination">
                                            <li class="disabled"><a href="#">&laquo;</a></li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>



<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>


<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>


