<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">


            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>TSUM VALLY TREKKING- 17 Days</h1>
            <p style="font-size:17px">“Trek between massive Ganesh Himal and Manaslu peaks around remote Gorkha district
                Adventure around Nepal Far North-West Himalaya and close on route to Tibet border
                Walking with scenic views of peaks from the start to an end of this wonderful journey
                Explore high country of Tsum known as valley of happiness with great scenic views
                From low warm areas to cooler alpine hills with alpine green woodland to arctic dry country
                Visit impressive Tsum villages with interesting culture and custom of ancient heritage
                On newly opened areas of Gorkha district to reach far and remote beautiful Tsum Valley”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘an adventure within Mt. Manaslu and massive Ganesh Himal around hidden valley of
                                    Tsum’</h3>
                                <p style="font-size: 17px;text-align: justify">Tsum Valley Trekking, an adventure into
                                    wild North Mid-West of Nepal Himalaya within historic Gorkha district where walk
                                    leads you from low warm sub-tropical area to reach at high and beautiful Tsum around
                                    Valley of Happiness where the name comes from ancient Tibetan languages known as
                                    ‘Beyul Kyimolung’.
                                    Truly a most unique country located far north on old Trans Himalayan Salt Trade
                                    Route of Nepal and Tibet, where border situated beyond upper Tsum Valley above Mu
                                    Gompa within arid and dry terrain.
                                    Tsum Valley one of the last hidden Shangri-la within Nepal high Himalaya range where
                                    it was forbidden to enter for foreign travelers due to its closeness with Tibetan
                                    frontier, now the door is open to witness its beautiful country and hidden
                                    traditional villages of great interest.
                                    Tsum Valley Trekking an interesting fresh Himalayan country for trekkers seeking new
                                    destinations around high Himalaya, this will be a marvelous walks and adventure
                                    around undisturbed and pristine wilderness, where very few trekkers and adventurer
                                    often venture this newly areas of Nepal North Mid-West.
                                    Beginning Tsum Valley Trekking with overland drive to reach low warmer areas of
                                    Nepal Mid-West around Gorkha district, where walk leads on main Manaslu trekking for
                                    some days and then our route diverts away from mainstream trail to reach high Tsum
                                    Valley with fantastic views of Sringi, Manaslu and Ganesh Himal range of mountains.
                                    Walk continues reaching remote and isolated villages around hidden areas of Tsum at
                                    Nyle and Mu-Gompa with its interesting old monastery, with rare ancient artifact of
                                    Buddhism treasures and beautiful religious paintings.
                                    After a fabulous time with great experience in remote wilderness of mountains and
                                    medvial period villages, as our return walk leads back on the same route with
                                    interesting time around Mu and Rachen monasteries areas then reaching warmer low
                                    areas to the road head for scenic drive to Kathmandu, with outmost adventure and
                                    enjoyable moment on Tsum Valley Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking: </b><br> Hotels and in lodge or Home stay n walks.<br>
                                    <b>Trek Grade:</b><br>Moderate to Adventurous.<br>
                                    <b>Area of Trek:</b><br>Nepal Far North Mid-West Himalaya around Gorkha
                                    district.<br>
                                    <b>Highest Altitude Gain: </b><br>At Mu Gompa (monastery) 3,695 m high.<br>
                                    <b> People and Culture:</b><br>Higher areas populated by Tsum people of Tibetan
                                    origin with
                                    Buddhist religion and impressive colorful culture.
                                    Lower and Mid-Hill mixed tribes of Magar-Gurung and Hindu
                                    Brahmin and Chettries with various interesting cultures.<br>
                                    <b> Trek Duration: </b><br>16 Nights and 17 Days (with drive both ways)<br>
                                    <b> Average walks:</b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b>Total Trip:</b><br>19 Nights and 20 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br>March to May and September to November months.<br>
                                    March to May when wild-flowers in full bloom, and day is clear
                                    for views, morning and night time as well in shade will be cold
                                    without wind-chill factor, October to November another best
                                    months where most of day is much clear, but short sunlight hours
                                    which falls in autumn and winter months will be very cold
                                    morning / night times with chances of snow sometimes.
                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b>Day 02:</b> In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b>Day 03:</b> Drive to Soti-Khola village 755 m via Arughat town 630 m - 06
                                    hrs.<br>
                                    <b>Day 04: </b> Trek to Machha-Khola 940 m - 06 hrs.<br>
                                    <b>Day 05:</b> Trek to Dobhan 1,075 m via Tatopani (Hot Spring) - 05 hrs.<br>
                                    <b>Day 06:</b> Trek to Jagat 1,450 m - 05 hrs.<br>
                                    <b>Day 07: </b> Trek to Lokpa 2,185 m via Philim 1,570 m - 06 hrs.<br>
                                    <b>Day 08: </b> Trek to Chumling 2,385 m - 06 hrs.<br>
                                    <b>Day 09: </b> Trek to Chokan-Paro 3,015 m - 05 hrs.<br>
                                    <b> Day 10: </b> Trek to Nyle 3,361m via Milereppa cave - 07 hrs.<br>
                                    <b> Day 11:</b> Rest day at Nyle to explore its beautiful country.<br>
                                    <b> Day 12:</b> Trek to Mu-Gompa 3,695 m - 04 hrs.<br>
                                    <b>Day 13: </b> Trek to Rachen Gompa 3,240 m - 05 hrs.<br>
                                    <b>Day 14: </b> Trek back to Chumling 2,385 m - 06 hrs.<br>
                                    <b>Day 15:</b> Trek to Lokpa 2,185 m - 05 hrs.<br>
                                    <b>Day 16:</b> Trek to Philim village 1,570 m - 05 hrs.<br>
                                    <b>Day 17: </b> Trek to Tatopani village with Hot Spring-06 hrs.<br>
                                    <b>Day 18: </b> Trek to Soti Khola 755 m - 05 hrs.<br>
                                    <b>Day 19:</b> Drive back to Kathmandu and transfer to hotel with free
                                    afternoon.<br>
                                    <b>Day 20:</b> Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Tsum
                                    Valley Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>

                                    <b>Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Tsum Valley, morning an
                                    optional sightseeing tour at places of great interest, exploring around Kathmandu
                                    world heritage sites for few hours and back
                                    at hotel, afternoon free for individual activities and preparations for next morning
                                    overland journey to reach Nepal Mid-West for Tsum Valley
                                    trekking.<br><br>

                                    <b> Day 03: Drive to Soti-Khola village 755 m via Arughat town 630 m - 06
                                        hrs.</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal North Mid-West highway
                                    that connects to Dhading and Gorkha district, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following on winding road as drive
                                    heads further west to reach at nice large town in Arughat within Gorkha district.
                                    From here following Buri-Gandaki River on winding road d to reach our overnight stop
                                    at Soti-Khola village within warm farm area.<br><br>

                                    <b> Day 04: Trek to Machha Khola 940 m - 06 hrs.</b><br>
                                    Trek starts of the first day heading into forest walking past smaller villages then
                                    reaching a wide valley opposite of a large tributary stream that runs into
                                    Burigandaki River.
                                    Nice views of cultivated terrace of Lapbesi upper village as walk leads downhill to
                                    a wide, sandy riverbed following a path that meanders below a steep, craggy valley
                                    side.
                                    As walk progress leading to a climb towards Machha-Khola for overnight stop, a
                                    village with number of teashops with small and simple lodge
                                    where Maccha Khola means a Fish Stream.<br><br>

                                    <b> Day 05: Trek to Dobhan 1,075 m via Tatopani (Hot Spring) - 05 hrs.</b><br>
                                    From here onward encountering many interesting farm villages and neat cultivated
                                    fields as vegetation changes slowly due to altitude and climate wise, as our morning
                                    walk leads to cross over a stream where Machha Khola (Fish stream) name comes from,
                                    and then walk upstream towards Khola-Bensi village with Tatopani a place with
                                    natural 'hot spring'.
                                    As walk continues where valley becomes much narrow and steeper, where our trail
                                    switches to left bank of the river and then crossing
                                    a suspension bridge, after a short period of walk entering patch of forest and then
                                    reaching at Doban located on the high shelf of Burigandaki
                                    River.<br><br>

                                    <b> Day 06: Trek to Jagat 1,450 - 05 hrs.</b><br>
                                    Morning walk leads across a broad gravel field towards Lauri stream and then
                                    crossing suspension bridge which leads to a climb on high trails and then descending
                                    into open valley of Yara Khola, where our journey continues on for another hour to
                                    the village of Jagat.
                                    Jagat a nice looking farm village of moderate size after Arughat; people here with
                                    mix race Magar, Chettries, Brahman, Manaslu Bhotia
                                    (Tibetan looking tribes) and some Newar people a lodge owner and
                                    shopkeepers.<br><br>

                                    <b> Day 07: Trek to Lokpa 2,185 m via Philim 1,570 m - 06 hrs.</b><br>
                                    From Jagat morning walks down-hill within a long stone steps to reach a river, where
                                    path
                                    climbs on terraced hillside at Saguleri, with distance view of Sringi Himal 7,187m,
                                    walk continue
                                    with ups and down as route reach a bridge over Buri-Gandaki.
                                    After crossing over a bridge where our trail winds up to reach at Philim village for
                                    short break and to observe this nice farm village with rich cultivated fields and
                                    terraces.
                                    After a refreshing stop at Philim, walk from here leads higher past scattered farm
                                    villages with climb through terraces
                                    of golden fields of wheat’s and barley, to reach a rocky ridge top covered within
                                    bamboo forest area towards Eklai Bhatti means
                                    lonely Inn, at present few more shops has come up.

                                    From here rest of the walk leads near to Manaslu route, where our route divert
                                    toward North Eest to reach our overnight
                                    stop at Lokpa a small nice village of Tibetan influence in culture and way of life
                                    on leaving the main trail of Manaslu trail.<br><br>

                                    <b> Day 08: Trek to Chumling 2,385 m - 06 hours.</b><br>
                                    From entering into fresh new areas, away from main trekking trails of Manaslu
                                    heading into Tsum, valley of happiness from here
                                    walk proceeds past few small farm lands and scattered villages with views of Ganesh
                                    and Manaslu Himal, as our day walk ends
                                    entering a nice village at Chumling for overnight halt.<br><br>

                                    <b>Day 09: Trek to Chokan-Paro 3,015 m - 05 hours.</b><br>
                                    A peaceful environment with fresh clean and cool mountain air, where walk leads much
                                    higher gaining elevation slowly as
                                    our adventure progress into inner Tsum valley, where our route leads to a descend
                                    and over a bridge towards Smatiking Khola,
                                    with views of Ganesh Himal also called Yangra in local language of Tibetan origin,
                                    and then reaching a fair size village at
                                    Chokan-Paro for overnight stop.<br><br>

                                    <b> Day 10: Trek to Nyle 3,361m via Milereppa cave - 07 hrs.</b><br>
                                    From here onward walk leads within Tsum valley, as morning lead towards highest
                                    areas at Nyle village within well exposed an open wide valley, a dramatic arid and
                                    dry country similar to Tibetan landscapes.

                                    Where tree disappears for short juniper and pine trees, as walk carries on facing
                                    views of massive Ganesh and Manaslu
                                    Himal mountain range, after a scenic and tough, good trek finally ending the day at
                                    Nyle, on route exploring
                                    Guru Milereppa meditation cave a great Buddhist saint with history of 11th century,
                                    Milereppa a poet, singer as well
                                    tantric guru where his folk stories, good words and songs are still remembered by
                                    large number of Tibetans to this day.<br><br>

                                    <b>Day 11: Rest day at Nyle to explore its beautiful country.</b><br>
                                    A full day rest in Nyle where one can witness local culture exploring traditional
                                    and fascinating villages with strong
                                    influence of Tibetan culture of similar Buddhism religion and traditional way life.

                                    If not visited interesting Milereppa holy cave, where on this day can hike to the
                                    site on this free day at leisure.<br><br>

                                    <b>Day 12: Trek to Mu Gompa 3,695 m - 04 hours.</b><br>
                                    Today a short morning walk to reach the end of Tsum Valley area at Mu Gompa, an old
                                    interesting monastery of the region.
                                    As walk leads to a climb within high and dry country facing views of surrounding
                                    high peaks within North, East and West direction, and then slowly walk completes
                                    reaching Mu Gompa for overnight stop.

                                    Afternoon enjoy exploring around least visited areas by other trekkers and
                                    westerners where you will certainly have peaceful
                                    time and moments around its traditional village with impressive ancient monastery of
                                    Mu Gompa, treasures antique important
                                    idols, statue and relics.<br><br>

                                    <b>Day 13: Trek to Rachen Gompa 3,240 m - 05 hours.</b><br>
                                    With interesting time and moment around high Tsum valley, walk leads to next
                                    monastery of great interest at Rachen Gompa,
                                    where walk leads on gradual trail with short ups and downs with lovely panorama of
                                    snow capped peaks, heading through yak
                                    herders summer pasture to reach Rachen Gompa and its moderate size village for
                                    overnight stop.<br><br>

                                    <b> Day 14: Trek back to Chumling 2,385 m - 06 hrs.</b><br>
                                    Return walks after an excellent time around high Tsum valley, where walk leads lower
                                    areas to reach at Chumling where our
                                    route leads back within tree lines and green vegetation at Chumling village, for
                                    last and final overnight in Tsum Valley area.<br><br>

                                    <b> Day 15: Trek to Lokpa 2,185 m - 05 hrs.</b><br>
                                    From here retrace the journey back to Lokpa to join Manaslu valley route, where walk
                                    leads further down leaving behind Tsum the
                                    valley of happiness, as good walk continues facing grand views of mountains and
                                    surrounding dramatic landscapes all the way
                                    to Lokpa for overnight stop.<br><br>

                                    <b> Day 16: Trek to Philim village 1,590 m - 06 hrs.</b><br>
                                    Morning walk leads with some down section entering into sheer cliff walls, after
                                    leaving the dramatic areas walk continues to
                                    reach at main trail of Manaslu, as our route leads further south direction to reach
                                    at warmer farm village at Philim, an interesting
                                    place for overnight stop, a village with spread out houses with immense farming
                                    crops and vegetables.<br><br>
                                    <b> Day 17: Trek to Tatopani village with Hot Spring-06 hrs.</b><br>
                                    After a lovely overnight at Philm village, where morning walk leads following
                                    Burigandaki river downstream, on the same trail
                                    of previous past days on the way up to Tsum as our route leads to further descend
                                    reaching low sub-tropical area at Tatopani
                                    a village with natural fresh hot-spring.
                                    Here enjoy refreshing moments on natural hot-spring with time to explore around the
                                    farm villages.<br><br>
                                    <b> Day 18: Trek to Soti Khola 730 m - 06 hrs.</b><br>
                                    From Tatopani with refreshing time our last day continues to near level trail
                                    following raging river downstream encountering many
                                    farm villages and into patch of forest of oaks and Sal trees to reach our last
                                    overnight stop and then end of the walk at Soti-Khola
                                    village<br><br>
                                    <b>Day 19: Drive to Kathmandu and transfer to hotel.</b><br>
                                    With enjoyable time in high mountains around remote North Mid-West Himalaya where
                                    morning takes you back to Kathmandu from Soti-Khola
                                    on the same route of earlier days, as drive leads towards warmer areas of Gorkha
                                    district at Arughat town and then on good road
                                    towards Kathmandu, after a wonderful time and experience on Tsum Valley Trekking
                                    with rest of the afternoon free at leisure.<br><br>
                                    <b>Day 20: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after a great adventure and wonderful
                                    experience of Tsum Valley Trekking, with your l
                                    ast final day in Nepal where Mount Vision Trek staff and guide transfer you to
                                    Kathmandu international airport for your flight home
                                    ward bound.<br><br>

                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>


                    <a href="booking.php?id=LANGTANG HIMAL - HOLY GOSAINKUND TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=LANGTANG HIMAL - HOLY GOSAINKUND TREKKING"
                       class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



