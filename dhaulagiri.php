<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>DHAULAGIRI BASE CAMP-HIDDEN VALLEY TREKKING -16 Days</h1>
            <p style="font-size:17px">“Adventure to the foot of world’s 7th highest Mt. Dhaulagiri and its massive range
                of peaks
                Glorious mountain views of Dhaulagiri and Annapurna range of peaks on daily exciting walks
                Walk into enchanting alpine forest covered with rhododendron-oaks, pine and fir trees
                From low warm paddy fields to cooler alpine hills then reaching at arctic zone of ice and glaciers
                Explore traditional Magar villages hill tribes of Dhaulagiri region with interesting cultures
                Crossing high French Pass to wide and beautiful Hidden Valley enclosed within mountains
                Starting from Mygdi district to reach charming villages of Marpha around Mustang area”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘adventure on the foot of world’s 7th highest Mt. Dhaulagiri and exciting scenic
                                    Hidden Valley’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Dhaulagiri Base Camp and Hidden Valley Trekking lead you to complete adventure
                                    within high mountains of massive Dhaulagiri range, where this trail is least
                                    explored or trekked by other adventurer and by main flow of trekkers.
                                    Dhaulagiri Base Camp, located Far North West Nepal which is close to Mustang and
                                    Dolpo district, the last 8,000 meters mountain range around Western Nepal, where
                                    walk leads you to follow raging Mygdi River right up to its glacial source at
                                    Dhaulagiri base camp, one of the few treks in Himalaya that leads you right at the
                                    beginning of river system.
                                    An adventure into pure wilderness of mountains, moraine and glaciers enclosed with
                                    high peaks of massif Dhaulagiri range and as well close to Annapurna Himalaya in the
                                    opposite side of Kaligandaki River valley.
                                    A fascinating walk for more than ten days where you will be away from main villages
                                    and human settlement for a week in total isolation in the harmony of nature and high
                                    giant mountains till the trek ends at Mustang area in lovely Marpha village and then
                                    taking a swift flight from Jomsom back to scenic Pokhara.
                                    This wonderful adventure to Dhaulagiri base camp and Hidden valley starts from
                                    touristic and beautiful Pokhara blessed with natural wonders of mountains and lakes,
                                    where drive leads to remote corners of Nepal Far West around Mygdi district.
                                    As walk begins entering into farm villages and well maintained terraces to reach the
                                    last village of the Mygdi area at Bagar, from here on in complete wilderness within
                                    enchanting forest of rhododendron and pines, which soon fades as our route leads
                                    towards barren country of ice and glaciers at Dhaulagiri base camp right at the
                                    bottom of world 7th highest mountain that towers high at 8,167 m / 26,795 ft.
                                    The mountain was first climbed by Swiss expeditions in May 13th 1960, at present few
                                    mountaineers’ often visits for the climb to its summit of Mt. Dhaulagiri; from here
                                    our route leads to the highest point of the adventure to cross over scenic French
                                    Pass at above 5,360 m to reach at beautiful Hidden Valley enclosed with array of
                                    peaks.
                                    After a wonderful time at base camp and Hidden valley our adventure comes to an end
                                    on crossing Dhampus / Thapa pass with excellent panorama of Annapurna-Dhaulagiri
                                    range of peaks as walk leads downhill at Mustang district in charming Marpha
                                    village, where next morning a short drive for flight back to picturesque Pokhara on
                                    completing one of the most incredible adventure on Dhaulagiri Base Camp and Hidden
                                    Valley Trekking.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking: </b><br> Hotels and lodge with camping on walks.<br>
                                    <b> Trek Grade: </b><br> Adventurous to Challenging in snow conditions.<br>
                                    <b> Area of Trek: </b><br> Nepal Far North Mid-West within Dhaulagiri Himalaya
                                    region.<br>
                                    <b>Highest Altitude Gain: </b><br> Crossing French Pass 5,360 m and Thapa / Dhampus
                                    pass at
                                    5,250 m high.<br>
                                    <b>People and Culture: </b><br> Mainly populated by Magar hill tribes with some
                                    Gurung and
                                    Thakali people around Manang areas enriched with Buddhist
                                    religion and impressive culture.<br>
                                    <b>Trek Duration: </b><br> 10 Nights and 11 Days (Pokhara to Pokhara).<br>
                                    <b>Average walks: </b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b>Total Trip: </b><br> 15 Nights and 16 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br> April to May and September to November months April to May
                                        leads to nice walks when wild- flowers are in full bloom, where
                                        most of the days are clear for views. Morning and Night time as
                                        well in shade will be cold without wind-chill factor, October to
                                        November another best months for trek when day is clear, but
                                        with short sunlight hours due to autumn season cold morning /
                                        night times with chances of snow.

                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.<br>
                                    <b> Day 03:</b> Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by
                                    air).<br>
                                    <b>Day 04:</b> Drive to Darbang 1,200 m via Beni 900 m - 05 hrs.<br>
                                    <b>Day 05: </b> Trek to Riverside 1,700 m - 04 hrs.<br>
                                    <b>Day 06: </b> Trek to Bagar 2,375m -05 hrs.<br>
                                    <b>Day 07:</b> Trek to Dovan 2,600m-06 hours.<br>
                                    <b> Day 08: </b> Trek to Chartare / Bhainsi Kharka 3,000m -05 hrs.<br>
                                    <b> Day 09: </b> Trek to Pakhaban (Japanese Camp.) 3,750m -05 hrs.<br>
                                    <b>Day 10: </b> Trek to Dhaulagiri Base Camp 4,750m - 06 hrs.<br>
                                    <b>Day 11: </b> Trek to Hidden Valley 4,900m via French Pass 5,360m.<br>
                                    <b>Day 12: </b> Trek to Yak Kharka 4,915m via Dhampus / Thapa Pass - 05 hrs.<br>
                                    <b>Day 13: </b> Trek to Marpha 2, 670 - 04 hours.<br>
                                    <b>Day 14:</b> Drive to Jomsom and fly back to Pokhara and transfer to hotel.<br>
                                    <b>Day 15:</b> Drive / Fly to Kathmandu and transfer to respective hotels.<br>
                                    <b>Day 16:</b> Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: Arrival in Kathmandu via respective international airline and transfer
                                        to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Dhaulagiri
                                    Base Camp and Hidden Valley Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                   <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Dhaulagiri Himalaya morning an
                                    optional sightseeing tour at places of great interest, exploring around Kathmandu
                                    world heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach at Pokhara
                                    around Nepal Far West for Dhaulagiri Base Camp trekking.<br><br>
                                    <b> Day 03: Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air).</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal main highway that
                                    connects with various parts of the country, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following Trisuli River for sometime
                                    as drive heads further west to reach at beautiful and scenic Pokhara city by its
                                    serene Phewa lake for overnight stop.
                                    Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate but
                                    with close views of towering and massive Annapurna Himalaya and majestic
                                    Machhapuchare Himal known as Fish Tail peak.
                                    Options by air to Pokhara take about 30 minutes of scenic air flight from Kathmandu.<br><br>
                                    <b>Day 04: Drive to Darbang 1,200 m via Beni 900 m - 05 hrs.</b><br>
                                    With wonderful views of Annapurna range, where morning leads to an exciting drive
                                    heading past Pokhara valley to reach at Beni headquarter town of Mygdi district
                                    within Dhaulagiri zone, from here drive continues on rough dirt road past farm
                                    villages following Mygdi River to reach at Darbang, a nice farm village for
                                    overnight stop.
                                    Darbang an important village as it is a junctions for several parts of western
                                    Nepal, Dolpo-Dhorpatan as well towards Dhaulagiri base camp.<br><br>
                                    <b>Day 05: Trek to Riverside 1,700 m - 04 hrs.</b><br>
                                    From here our first day walk begins on following Mygdi River an interesting walk for
                                    few hours with short ups and down with much gradual trail along the bank of the
                                    raging river, walking past scattered small farm villages to reach a nice area on the
                                    shore of Mygdi River for overnight stop in tents with full camping facilities.<br><br>
                                    <b>Day 06: Trek to Bagar 2,375m - 05 hrs.</b><br>
                                    Starting our second day of treks around farm areas and then after lunch in the
                                    afternoon where steep climb leads to a high ridge covered within rhododendron,
                                    magnolia, oaks and pine forest, on reaching cooler area and then heading downhill
                                    into forested trail to reach at Bagar.
                                    Bagar a moderate size farm village, this will be the last village and human
                                    settlement from here onward till our route joins at Marpha village after walking
                                    past Dhaulagiri base camp, French Pass and Hidden Valley.<br><br>
                                    <b>Day 07: Trek to Dovan 2,600m - 06 hours.</b><br>
                                    On leaving Bagar village, morning walk follows past farm fields and terraces and
                                    then entering into a forest after an hour walks, and then a climb starts within cool
                                    woodland of bamboo, oaks and rhododendron trees.
                                    After a steep climb where our route leads to a long descend on quite narrow path
                                    above the Mygdi River to reach at Dovan for overnight camp, a small temporary place
                                    and settlement with few tea-houses serving as shelter for porters includes small
                                    shops and stores in the middle of dense forest surrounding.<br><br>
                                    <b> Day 08: Trek to Chartare / Bhainsi Kharka 3,000m -05 hrs.</b><br>
                                    After Dovan morning leads to cross over an unstable wooden bridge over a fast stream
                                    which is one of the confluences of Mygdi River, after the bridge walk into forested
                                    area all the way to overnight stop at Chartare also known as Bhainse Kharka.<br><br>
                                    <b>Day 09: Trek to Pakhaban (Japanese Camp) 3,750 m - 05 hrs.</b><br>
                                    With pleasant overnight stop in the middle of dense forest at Pakhaban, morning walk
                                    leads into complete wilderness after leaving rich green vegetation and tree lines
                                    behind for somedays.
                                    Morning walk leads closer to Mt. Dhaulagiri South Face flank, with views of Putha
                                    Huinchuli, Manapathi and Dhaulagiri south flank, after good hours of walk reaching a
                                    glaciated area on a clearing named as Italian camp.
                                    From here adventure begins as our route leads over glacier and moraine with climb
                                    over rocky ridge to reach at Pakhaban also called as Japanese camp.
                                    This camp offers super views of South West Face of Dhaulagiri and Manapathi, Puttha
                                    Hiunchuli with huge ice wall of Tsaurabong Peak.<br><br>
                                    <b>Day 10: Trek to Dhaulagiri Base Camp 4,750m - 06 hrs.</b><br>
                                    Morning walk follows on faint path towards moraine and iceberg in some sections of
                                    the trek, as walk and climb where one needs to follow the lead guides, porters and
                                    fellow members to be on the safe side of the trail over glaciated sections.
                                    After a tough walk over glacial section reaching a rocky open area at Dhaulagiri
                                    base camp with awesome views of mountain range that enclose at base camp.
                                    Overnight camp at base camp the highlight of this adventure with closest view of
                                    world 7th highest Mt. Dhaulagiri I at 8,167 m, includes view of tumbling icefalls,
                                    glacier and Tukuche peak with West peaks of Dhaulagiri II (7,751 m.), Dhaulagiri III
                                    (7,715 m.) and Dhaulagiri IV (7,618 m.).<br><br>
                                    <b>Day 11: Trek to Hidden Valley 4,900m via French Pass 5,360m.</b><br>
                                    From this scenic and amazing campsites morning leads to the highest point of an
                                    adventure over French pass at 5,360 meters.
                                    After the high pass a short descend to reach a wide grassy meadow a huge field at
                                    hidden valley for overnight camp.
                                    Offering views of Sita Chuchura, Mukut Himal, and Tashi Kang 5,386 meters, Tukche
                                    Peak and Dhaulagiri I.<br><br>
                                    <b>Day 12: Trek to Yak Kharka 4,915m via Thapa / Dhampus pass 5,250 m - 05 hrs.</b><br>
                                    After a pleasant rest, morning starts on a gentle path for few hours to reach last
                                    pass of this adventure at Thapa also called Dhampus pass with 5,250 meters high.
                                    On reaching the pass with grand rare views of Annapurna Himal, Nilgiri’s peaks, and
                                    then our walk descend to Yak Kharka for overnight camp and back into green
                                    vegetation of Juniper, Burberry and rhododendron bushes.<br><br>
                                    <b>Day 13: Trek to Marpha 2,630 m - 04 hrs.</b><br>
                                    Morning walk takes you to long descend to reach back into villages, after being in
                                    complete wilderness for several days.
                                    Walking into forest of firs, pines and rhododendron trees, after few hours of easy
                                    descend reaching at charming village of Marpha where houses are painted white, a gem
                                    of a village with an interesting monastery, this area famous for delicious apples
                                    and its products.<br><br>
                                    <b> Day 14: Morning short drive to Jomsom airport 2,715 m and fly to Pokhara.</b><br>
                                    From Marpha one of the most attractive village of Kali-Gandaki and Mustang areas,
                                    where early morning drive takes you at Jomsom airport for scenic short flight to
                                    land at Pokhara, one of the most beautiful city, blessed with verdant valley, high
                                    mountains and serene lakes.<br><br>

                                    <b>Day 15: Drive or fly back to Kathmandu and transfer to hotel with free afternoon.</b><br>
                                    After an enjoyable time in high Himalaya where morning overland journey takes you on
                                    the same route to reach back at Kathmandu, after a wonderful time and experience on
                                    Dhaulagiri Base Camp trekking with rest of the afternoon free at leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.<br><br>
                                    <b>Day 16: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memorable experience and
                                    adventure on Dhaulagiri Base Camp and Hidden Valley Trekking, with last final day in
                                    Nepal where Mount Vision Trek staff and guide transfer you to Kathmandu
                                    international airport for your flight home ward bound.


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>
                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>
                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=DHAULAGIRI BASE CAMP-HIDDEN VALLEY TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=DHAULAGIRI BASE CAMP-HIDDEN VALLEY TREKKING" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



