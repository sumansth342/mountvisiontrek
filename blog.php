<?php include'navigation.php';
require 'inc/connect.inc.php' ?>
<div class="vision-loader"></div>
    <aside id="vision-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(assets/images/untitled1.png); ">
                </li>
            </ul>
        </div>
    </aside>
<?php
$blog = "";
$query_run = mysqli_query($con, "SELECT * FROM blog ORDER BY id DESC LIMIT 4");
$ads_count = mysqli_num_rows($query_run);

if ($ads_count > 0) {
    while ($row = mysqli_fetch_array($query_run)) {

        $ads_id = $row['id'];
        $title = $row['title'];
        $image=$row['image'];
        $date = $row['date'];
        $place=$row['place'];

        $blog.='<div class="col-md-12 animate-box">
                        <a href="details_blog.php?id='.$ads_id.'" class="blog-post">
                            <span class="img" style="background-image: url('.$image.');"></span>
                            <div class="desc">
                                <span class="date">'.$date.'</span>
                                <h3>'.$title.'</h3>
                                <span class="cat">'.$place.'</span>
                            </div>
                        </a>
                    </div>';
    }
}
?>


<div id="vision-blog" style="padding-top: 1px;padding-bottom: 1px">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center vision-heading animate-box">
                <h2>Recent Blog</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        <div class="blog-flex">
            <div class="col-md-6" >
                <article class="animate-box">
                    <div class="blog-img" style="background-image: url(assets/images/blog-1.jpg);"></div>
                    <div class="desc">
                        <div class="meta">
                            <p>
                                <span>Feb 24, 2018 </span>
                                <span>Cultural Visits </span>

                            </p>
                        </div>
                        <h2><a href="#">A Definitive Guide to the Best Dining</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, qui quod ipsum harum id, minima nesciunt! Saepe soluta, vitae quas expedita voluptatem voluptates placeat numquam provident quis, atque nisi iure?</p>
                    </div>
                </article>
            </div>
            <div class="blog-entry aside-stretch-right">
                <div class="row">

                    <?php echo $blog ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include'footer.php'?>