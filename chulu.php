<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>CHULU EAST PEAK CLIMB</h1>
            <p style="font-size:17px">“Exciting climb and experience to the summit of stunning Chulu East Peak
                Marvelous adventure visiting both sides of massive Annapurna Mountains Range
                Around Manang, Mustang regarded as world’s best adventure destinations
                Walk and climb on top Nepal highest trekking peaks with fabulous scenery
                Grand views of peaks on daily walks within scenic Manang valley and villages
                Explore colorful culture and custom of Buddhist religion of Manang villages
                From warm paddy fields to cooler alpine hill and at arctic zone of ice and glaciers”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> ‘adventure on top Nepal highest trekking peaks within scenic Manang valley’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Chulu East Peak Climb an adventure that takes you to highest trekking peaks of Nepal
                                    as recorded with NMA (Nepal Mountaineering Association) that controls peak climbing
                                    and other lesser mountains as per Nepal Government regulation.
                                    Chulu East Peak Climb, where walk leads you around famous and popular Annapurna
                                    Circuit route beyond massif Annapurna Himalaya and around Chulus range of peaks with
                                    Chulu East, West, Mid and Far East includes Pisang Peak towards east and Damodar
                                    Himal in the North of Manang valley.
                                    A great scenic adventure in the harmony of local Buddhist culture of Manang people
                                    and its nice villages of Dharapani, Chame town, Pisang and Manang and heading over
                                    high Thorang-la pass to Mustang district at Muktinath and Jomsom after a great climb
                                    on Chulu East peak.
                                    Climb leads to toughest among Chulus group of peaks due to its location and altitude
                                    at above 6,584 m / 21,727 ft that makes highest trekking peaks of the country and
                                    within all Himalaya range.
                                    The grade used as PD + comes from French and Swiss Alpine Climbing Classification
                                    Systems which means ‘more Per Difficule with added plus), a straight-forward climb
                                    with some technical sections to negotiate to reach its summit top.
                                    Where all climber needs to be well equipped as per the climbing list our expert
                                    climbing guide will accompany and guide you to the top and brings you safely back
                                    after great success on reaching the summit of Chulu East Peak.
                                    We have designed Chulu East Peak Climb with flexible itinerary days with enough
                                    space for practice and to get acclimatized with the altitude and high terrain, where
                                    walks leads to Chulu East Peak Climb after diverting off from the main Annapurna
                                    circuit route for some days.
                                    After the climb of Chulu East Peak back on the trail to Manang with traverse over
                                    high Thorang-la pass to reach at Muktinath and to Kaligandaki Valley in Jomsom town
                                    for scenic short flight to beautiful Pokhara to complete our remarkable adventure on
                                    Chulu East Peak Climb.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br> Hotels with camping facilities on walks and climb.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous Walks.<br>
                                    Climbing Grade:    </b><br> PD + ‘Per Difficule’ a French / Swiss Alpine Climbing
                                    Systems.<br>
                                    <b> Area of Trek and Climb:</b><br> Nepal North Mid-West within Manang district.<br>
                                    <b>People and Culture: </b><br>Mainly populated by Manang, Thakali, Gurung and
                                    Magar enriched with Buddhist religions with impressive
                                    culture and traditional way of farm life.<br>
                                    <b> Trek and Climb: </b><br>15 Nights and 16 Days (with drive and fight to Pokhara).<br>
                                    <b> Average walk and climb:</b><br> Minimum 4 hrs to Maximum 6 hrs to 8 hrs.<br>
                                    <b> Total Trip: </b><br> 20 Nights and 21 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons: </b><br> April to May is good for trek and climb when wild-
                                    flowers in full bloom, where most of the days are clear
                                    for views. Morning and Night time as well in shade will
                                    be cold without wind-chill factor.
                                    September to November another best months to
                                    trek and climb where days are clear, but with short
                                    sun or day light hours due to autumn and winter months
                                    will be very cold morning / night times with chances of
                                    snow.

                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: </b> Arrival in Kathmandu and transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu preparation for the trek and climb.<br>
                                    <b>Day 03:</b> Drive to Jagat via Besisar 980 m - 06 hrs journeys.<br>
                                    <b>Day 04: </b>Trek to Dharapani village 1,915 m - 06 hrs.<br>
                                    <b> Day 05: </b>Trek to Chame town 2,675 m - 05 hrs.<br>
                                    <b> Day 06:</b> Trek to Pisang village 3,120 m - 05 hrs.<br>
                                    <b>Day 07: </b> Trek to Julu 3,900m - 06 hrs.<br>
                                    <b> Day 08: </b> At Julu for acclimatization.<br>
                                    <b> Day 09: </b> Trek to Chulu Base Camp at 5,300m - 04 hrs.<br>
                                    <b> Day 10: </b> Rest day at Base Camp preparation and acclimatization.<br>
                                    <b>Day 11: </b> Climb of Chulu East and return to base camp.<br>
                                    <b>Day 12: </b> Reserve day in case of unfavorable bad weather condition.<br>
                                    <b>Day 13:</b> Trek to main Manang village 3,450 m - 05 hrs.<br>
                                    <b> Day 14:</b> Trek to Yak Kharka 4,110 m - 04 hrs.<br>
                                    <b> Day 15:</b> Trek to Thorang High Camp 4,495 m - 04 hrs.<br>
                                    <b> Day 16:</b> Over Thorang-la Pass 5,416 m down to Muktinath 3,795 m - 06 hrs.<br>
                                    <b> Day 17:</b> Drive to Jomsom 2,715 m - 02 hrs.<br>
                                    <b>Day 18:</b> Fly / Drive to Pokhara 22 mins by air and 5-6 hrs drives.<br>
                                    <b>Day 19:</b> Drive to Kathmandu and transfer to hotel.<br>
                                    <b>Day 20:</b> Reserve and contingency day in Kathmandu with individual
                                    activities.<br>
                                    <b> Day 21:</b> Depart Kathmandu for international departure homeward bound.<br>


                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01: Arrival in Kathmandu and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join to meet other members of Chulu
                                    East Peak Climb.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks and climb local culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b> Day 02: In Kathmandu preparation for the trek and climb.</b><br>
                                    A free day preparation for Chulu East Peak climb and treks around high Khumbu, with
                                    optional tour at places around Kathmandu world heritage sites of few hours, allows
                                    you getting ready with packing for next early morning flight to Lukla where our walk
                                    begins towards Lobuche East Peak Climb.<br><br>
                                    <b> Day 03: Drive to Jagat via Besisar 980 m - 06 hrs journeys.</b><br>
                                    Around Annapurna adventure starts taking an interesting overland journey towards
                                    Nepal Mid West, leading to warmer areas at Besisar town (headquarter of Lamjung
                                    district). From here transfer to another vehicle and continue drive on dirt road
                                    past Khudi, Bubule and Ngadi villages following along Masrsyangdi River upstream.
                                    Drive continues through rice terraces fields and villages to Syange a small village
                                    and then short uphill drive to reach a nice farm village in Jagat for overnight
                                    stop.<br><br>

                                    <b>  Day 04: Trek to Dharapani village 1,915 m - 06 hrs.</b><br>
                                    From Jagat our walk begins with slow climb above Masrsyangdi River as our route
                                    leads on winding path to Chamje, and back at Masrsyangdi River and then crossing to
                                    its east side within forested area, with views of Annapurna and Manaslu mountain
                                    ranges.

                                    As walk continues reaching nice village of Tal Besi, from here another three hours
                                    walk leads to our overnight stop at Dharapani village, this nice village located on
                                    the junction of Annapurna circuit and Manaslu trails.<br><br>

                                    <b> Day 05: Trek to Chame town 2,675 m - 05 hrs.</b><br>
                                    After Dharapani walk leads into cool forest of rhododendron and pine trees and
                                    heading towards Bagarchap and Danque villages to reach at Timang where you will
                                    notice more strong Buddhist culture with views of Mt. Manaslu 8,163m / 26,781ft.
                                    From Timang village few hours walk leads to Koto and then at Chame town for
                                    overnight stop, Chame a large and major town of Manang district which is also head
                                    quarter the area.<br><br>

                                    <b>Day 06: Trek to Pisang village 3,120 m - 05 hrs.</b><br>
                                    From Chame onward heading closer towards mountains as walk leads through alpine
                                    forest following Masrsyangdi River upstream, with short ups and down to reach
                                    Bhratang village, after a short stop walk follows uphill into woodland to Dhukure
                                    Pokhari a small place on top of a hill with few teahouses and lodge facing views of
                                    Chulus and Pisang peaks, with possible lunch stop, then descend towards lower Pisang
                                    village for overnight stop.<br><br>

                                    <b> Day 07: Trek to Julu 3,900 m - 06 hrs.</b><br>
                                    From Pisang village, we will be away from Annapurna circuit trail for few days, as
                                    the walk leads to complete barren wilderness, as the walk continues higher above
                                    Nagawa village, leading towards North West into more arid and dry terrain to reach
                                    at Jhulu small village for first camping of this trip.<br><br>

                                    <b>Day 08: At Julu for acclimatization.</b><br>
                                    A rest day necessary and important for acclimatization and preparation for the
                                    adventurous climb to the summit of Chulu East peak, take a hike around and explore
                                    Julu traditional village with grand scenery of mountains with view of Chulu’s peaks.<br><br>

                                    <b>Day 09: Trek to Chulu Base Camp at 5,300m.</b><br>
                                    After pleasant rest at Julu, morning trek heads towards remote and wilderness area
                                    with views of Chulus and Annapurna range of mountains, climb leads above scenic
                                    Manang valley towards Chulu East and Far East Peaks area, after a hard high altitude
                                    walk reaching a rocky moraines on ridges to camp for overnight, this perfect spot
                                    will be the base camp and starting point to Chulu East Peak climb.<br><br>

                                    <b> Day 10: Rest day at Base Camp.</b><br>
                                    Another important acclimatization day, and as well for final preparation of the
                                    climb with time to check climbing gear for Chulu East Peak climb, here our guide
                                    will check the route to make the climb more approachable and successful.<br><br>
                                    <b> Day 11: Climb of Chulu East Peak and descend to Base Camp.</b><br>
                                    Our main highlight of the adventure we will be starting early before dawn and then
                                    climb over a snowy slopes and ridge with quite steep includes some technical skill
                                    in few sections to reach the summit of Chulu East; our expert guide will set the
                                    route with fix ropes around necessary to reach the top summit.

                                    After a final climb reaching at the top summit feeling high with adventure of a life
                                    time experience, on top enjoy the surrounding magnificent panorama of mountains and
                                    descend back to base camp.<br><br>

                                    <b>Day 12: Reserve day in case of unfavorable bad weather condition.</b><br>
                                    A spare contingency day in case bad weather obstructs our routine days for the climb
                                    on top Chulu East Peak summit as well for safe climbing.

                                    If all goes as per the standard itinerary days using this extra days on the way back
                                    to Jomsom with leisure walks enjoying time and views.<br><br>

                                    <b>Day 13: Trek to main Manang village 3,450 m - 05 hrs.</b><br>
                                    Morning catch views of mountains, from here onward altitude gains slowly as walk
                                    leads into forest with short uphill facing excellent views of Annapurna II and III,
                                    Gangapurna, Chulus and Pisang peaks.

                                    From the top ridge downhill to reach a flat valley at Hongde a village with small
                                    airstrip, from here onward tree lines fades for barren, arid landscapes with cold,
                                    dry arctic climate.

                                    Walking into main village of proper Manang for overnight stop, before Manang an
                                    interesting tour of old Braga monastery enriched with ancient Buddhist relics, wall
                                    painting and frescos includes stunning views of Annapurna Himalaya.<br><br>

                                    <b>Day 14: Trek to Yak Kharka 4,110 m - 04 hrs.</b><br>
                                    After a pleasant time at Manang, morning leads on main path of Annapurna circuit on
                                    the road to Thorang-la, where walk leads to short uphill to reach at Yak Kharka, an
                                    interesting walk with views of snowcapped mountains, on crossing a small stream
                                    reaching Yak Kharka for overnight stop, a small settlement with views of Gangapurna,
                                    Annapurna III and Chulu Peaks.<br><br>

                                    <b> Day 15: Trek to Thorang High Camp 4,495 m - 04 hrs.</b><br>
                                    Morning walk lead to higher elevation and getting closer at the base of Thorang-la,
                                    on crossing a bridge and then climb along a river bank, finally walk leads to
                                    Thorang Phedi, here depending upon the physical conditions of the clients, if all
                                    goes well an hour steep climb to Thorang High Camp for overnight stop.<br><br>

                                    <b> Day 16: Traverse Thorang-la Pass 5,416 m down to Muktinath 3,795 m - 06 hrs.</b><br>
                                    One of the highlight the adventure, after early breakfast a long climb leads you to
                                    Thorang-la walking on steep winding path to reach at Thorang La Pass above 5,416 m /
                                    17,700 ft.

                                    The highest point of the adventure with views of snowcapped peaks, and then with
                                    long descends to reach at holy Muktinath around Lower Mustang area.
                                    On reaching Muktinath for overnight stop, this is a holy spot for both Hindus and
                                    Buddhists the word Muktinath literally means ‘the place for Nirvana or Liberation’.<br><br>

                                    <b>  Day 17: Drive to Jomsom 2,715 m- 02 hrs.</b><br>
                                    From holy sites of Muktinath with views of Dhaulagiri range, morning where our walks
                                    ends with short overland ride to reach at lower area of Mustang, as motorable road
                                    has been built where trek is disturbed by movement of vehicles, so taking a drive
                                    instead of walking.

                                    Drive leads towards wide Kaligandaki River within its windswept landscapes to reach
                                    at large town in Jomsom, headquarter town of Mustang area, which is home of Thakali
                                    tribe been on this windswept country for hundreds of years.
                                    Jomsom located on old Trans-Himalayan Salt Trade Route of Nepal to Tibet, which
                                    extends towards Upper Mustang over Karo-La pass, Jomsom and Kaligandaki area famous
                                    for delicious apples and its product also.<br><br>

                                    <b> Day 18: Fly from Jomsom to Pokhara with rest of the day free at leisure.</b><br>
                                    After a great time on high hills of Annapurna, where early morning at Jomsom a short
                                    to its airport terminal for scenic smooth flight to land at picturesque Pokhara city
                                    for last overnight by its Phewa Lake side.<br><br>

                                    <b> Day 19: Drive or fly back to Kathmandu and transfer to hotel.</b><br>
                                    With enjoyable time in high both sides of Annapurna Himalaya where morning overland
                                    journey takes you on the same route to reach back at Kathmandu, after a wonderful
                                    time and experience on Chulu East Peak Climb with walk around Annapurna rest of the
                                    afternoon free at leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.<br><br>
                                    <b>  Day 20: Reserve and contingency day in Kathmandu for individual activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad
                                    and unfavorable weather condition at Jomsom, where extra day allows you with time
                                    for yourself for individual activities or join in our exclusive tour or just relax
                                    after a great adventure on high Chulu East Peak Climb.<br><br>

                                    <b> Day 21: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and experience on
                                    Chulu East Peak Climb with last final day in Nepal where Mount Vision Trek staff and
                                    guide transfer you to Kathmandu international airport for your flight home ward
                                    bound.


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Arrival/departure airport transfers<br>
                                    <i class="icon-tick"></i></a></li>Hotel in Kathmandu with breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Guided Kathmandu city tour with entrance fees by
                                    private vehicle.<br>
                                    <i class="icon-tick"></i></a></li>All your standard Meals (breakfast, lunch and
                                    dinner) during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Teahouse accommodation during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Round Trip flight fare Kathmandu –Lukla –
                                    Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required trek entry permits (Sagarmatha National
                                    Park fee and TIMS card fee)<br>
                                    <i class="icon-tick"></i></a></li> Mount Vision treks duffel bag, jacket and
                                    sleeping bag to use during the trek (upon request)<br>
                                    <i class="icon-tick"></i></a></li> Farewell dinner in Kathmandu<br>
                                    <i class="icon-tick"></i></a></li> Trek completion certificate (upon request)<br>
                                    <i class="icon-tick"></i></a></li> All our government taxes and official expenses.
                                </p><br><br>
                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during your stay in
                                    Kathmandu.<br>
                                    <i class="icon-cross"></i></a></li>Personal expenses (laundry, telephone calls,
                                    sweets, snacks, battery charging etc.,<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance (Essential).<br>
                                    <i class="icon-cross"></i></a></li> Personal trekking equipment except stated above.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> Any loss arising due to unforeseen circumstances
                                    that is beyond Mosaic Adventure control.<br></p>
                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=Mera Peak Climbing"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=Mera Peak Climbing class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



