<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css" />

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1> THYANGBOCHE EVEREST PANORAMA TREKKING - 12 Days</h1>
            <p style="font-size:17px">“Walking to scenic and holy spot at beautiful Thyangboche and its impressive monastery
                A moderate adventure with scenic views of high peaks with interesting Sherpa culture
                Flying in and taking off on unique Lukla airstrip with sweeping mountain panorama
                Within World Heritage Sites of Sagarmatha National Park and its cool alpine woodland
                Explore native Sherpa villages adorned with interesting Buddhist monuments and monasteries
                Adventure into high and magnificent country of high Khumbu in the shade of Mt. Everest”

            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen" >Overview</button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day Itinerary</button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include Excludes</button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘a cultural and scenic walk to Thyangboche Monastery enclosed with stunning mountain views’</h3>
                                <p style="font-size: 17px;"> Thyangboche Everest Panorama Trekking, a suitable scenic and cultural Himalayan destination for all age groups and interested travelers where you will soak with scenic views of high snow capped mountains from the start to an end of the journey.
                                    Thyangboche Everest Panorama Trekking will be a short and memorable adventure holiday with exciting moments within the shade of mighty Mt. Everest and in the shade of local Sherpa colorful culture and age-old traditions custom of Buddhist religion.
                                    A perfect walk with touch of high altitude especially on route Namche and beyond to Thyangboche Monastery, where you will be well acclimatized with rest days and short hike to view points to make you fit and in good health to reach our destination at beautiful Thyangboche for excellent panorama of Everest and adjoining peaks.
                                    Thyangboche Everest Panorama Trekking commence from Kathmandu taking a short panoramic flight to reach at Lukla, a Sherpa town and a gateway to various adventure around high Khumbu within Everest region.
                                    Where walk from Lukla leads you on pleasant and scenic trail encountering many Sherpa villages with neat tended farm fields and terraces walking around prayer monument called Mani-Wall and then slowly walk climbs higher at famous Namche Bazaar.
                                    Namche Bazaar hub of Khumbu Sherpa, as well for trekkers where many good restaurants, bakeries, shops, stores includes banks and ATM facilities having rest day to help with acclimatization and then walking towards our main goal and destinations at Thyangboche Monastery.
                                    A picturesque spot surrounded by green woodland and might peaks facing Mt. Everest towards North with views of Amadablam-Kangtenga-Tharmasarku-Kongde with Lhotse and other chain of peaks.
                                    Visit of the monastery of great interest with beautiful interior witness fine religious painting and great statue of Buddha and other Buddhist Guru and saints, after an enjoyable time at Thyangboche and its impressive monastery, walk leads back to Lukla.
                                    Walking towards Lukla with visit of traditional Sherpa village of Khumjung and Khunde located on a scenic glacial valley before ending our marvelous walks at Lukla, where short smooth flight brings you back at Kathmandu, with everlasting memories and delightful adventure on Thyangboche Everest Panorama Trekking.

                                </p>

                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trekking:</b><br>Hotels and in lodge on walks.<br>
                                    <b>Trek Grade:</b>	<br>Moderate with touch of high altitude<br>
                                    <b>Area of Trek:</b><br>Nepal North Mid-East within Khumbu district of Everest<br>
                                    <b>Highest Altitude Gain:</b><br>	3,867 m at Thyangboche Monastery.<br>
                                    <b>People and Culture:	</b><br>Mainly populated by Sherpa the highlanders of Everest
                                    enriched with colorful Buddhist religion and impressive culture.
                                    <br>
                                    <b>Trek Duration:</b><br>07 Nights and 08 Days (Lukla to Lukla)<br>
                                    <b>Average walks:</b><br>	Minimum 4 hrs to Maximum 6 hrs or more.<br>
                                    <b>Total Trip:</b><br>11 Nights and 12 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br>All months of the year except monsoon times from June to
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.

                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p ><b>Day 01:</b>	Arrival in Kathmandu via respective international airline and transfer to hotel.<br>
                                    <b>Day 02:</b>	In Kathmandu with optional sightseeing tour and preparation for the trek.<br>
                                    <b>Day 03:</b>	Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m and trek
                                    to Phakding 2,655 m - 03 hrs.
                                    <br>
                                    <b>Day 04:</b>Trek to Namche Bazaar 3,440 m - 06 hrs.<br>
                                    <b>Day 05:</b>In Namche for rest to support acclimatization with short hike and excursion.<br>
                                    <b>Day 06:</b>Trek to Thyangboche 3,867 m - 05 hrs.<br>
                                    <b>Day 07:</b>	Trek to Khumjung village 3,645 m - 05 hrs.<br>
                                    <b>Day 08:</b>	Trek to Monjo 2,875 m - 05 hrs. <br>
                                    <b>Day 09:</b>	Trek to Lukla 2,818 m - 04 hrs.<br>
                                    <b>Day 10:</b>Fly back to Kathmandu and transfer to hotel with free afternoon at leisure.<br>
                                    <b>Day 11:</b>	Reserve and contingency day in Kathmandu with individual activities.<br>
                                    <b>Day 12:</b>	Depart Kathmandu for international departure homeward bound.<br>
                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent"style="background-color: whitesmoke">
                                <p> <b>Day 01:	Arrival in Kathmandu via respective international airline and transfer to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer to your respective hotels in the centre of the city in Kathmandu, after checking into your rooms getting refreshed from jet-leg join to meet other members of Thyangboche Everest Panorama Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of trekking regarding local lodge, walks, view, history and culture with few important do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural programs of all parts of the country to enlighten the environment along with your dinner.<br>

                                    <b> Day 02:In Kathmandu with optional sightseeing tour and preparation for the trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional sightseeing tour at places of great interest, exploring around Kathmandu world heritage sites for few hours and back at hotel.
                                    Afternoon at leisure and free for individual activities and getting ready with packing for next early morning flight to Lukla where our walk begins towards Thyangboche Everest Panorama Trekking.<br>

                                    <b>  Day 03:Transfer to Kathmandu domestic airport for flight to Lukla 2,818 m and trek to Phakding 2,655 m - 03 hrs.</b><br>
                                    Morning at the earliest before or after quick breakfast or one carry pack breakfast to save time reaching at Kathmandu domestic airport terminal, where a short scenic flight of 35 minutes reaches you at Lukla, a gateway Sherpa town at the height of 2,820 m high facing views of Kongde peak and Naulekh Himal.
                                    With refreshing stop at Lukla then follow our lead guide after adjusting baggage with porters to carry or on pack animals (Jopkyo a mix breed of normal cow and Yak).
                                    As walk leads past Lukla town heading down to Choplung small village and then around Dudh-Kosi River valley and gorge towards our first overnight stop at Phadking village near Dudh-Kosi a glacial river.<br>

                                     <b> Day 04:	Trek to Namche Bazaar 3,440 m - 06 hrs.</b><br>
                                    Morning begins with gradual walk following Dudh Kosi river upstream past several Sherpa nice villages with short ups and down and then crossing a long suspension bridge to Monjo village, where Sagarmatha National Park starts entering into park areas past Jorsalle the last village before Namche Bazaar.
                                    From Jorsalle crossing bridge and into serene woodland of tall pines and rhododendron trees with climb on winding trail facing views of Kongde, Kusum Khanguru and Tharmasakru peaks as walk brings you into famous Namche Bazaar, which is more of a town rather than village with many good restaurants, hotels, lodge having number of shops and stores includes Bank and Post-Office.
                                    At Namche enjoy the comfort of nice cozy lodge, facing great views of snow capped peaks the closest lies Kongde peaks.<br>

                                     <b> Day 05:	In Namche for rest to support acclimatization with short hike and excursion.</b><br>
                                    At Namche rest day for acclimatization before heading higher area, where short hike up to view point like towards Everest View Hotel takes nearly 3-4 hours both ways, offers excellent panorama of mountains with Everest and magnificent Amadablam
                                    In Namche visit museum where you can gain knowledge of Khumbu Sherpa, culture, flora-fauna as well of mountaineering expedition history with rest of the afternoon at leisure.<br>

                                    <b> Day 06:	Trek to Thyangboche 3,867 m - 05 hrs.</b><br>
                                    With pleasant time at Namche walk leads to short climb and then on good wide trail facing views of Everest and Amadablam with array of peaks, as our route leads into woodland then descend towards Imjatse River to reach at a small place in Phunke-Tenga, possible lunch stop in the middle of a wood.
                                    Afternoon climb into forested areas facing views of Kangtenga-Tharmasarku and other peaks as walk brings at a wide open plateau in Thyangboche with its beautiful colorful Buddhist monastery, with time to visit the interior of the monastery where you can witness monks and priest on religious activities with prayer.
                                    A scenic spot surrounded by high peaks with views of Everest and magnificent looking Amadablam amidst forest of rhododendron-magnolia-oaks-juniper-birch and fir tree lines, truly a great place for overnight stop.<br>

                                    <b> Day 07:	Trek to Khumjung village 3,645 m - 05 hrs.</b><br>
                                    With enjoyable time at scenic Thyangboche, where once a year a religious colorful festival called Mani-Rimdu held in the month of October or November as per Buddhist Luna calendar.
                                    From here on the same trail for few hours after downhill and short climb to Shanasa village where our route diverts with gradual ups to reach a wide scenic glacial valley where Khumjung village located for overnight stops.
                                    A nice large village different to Namche, where this place with more interesting local culture and custom, with time here to visit its old monastery treasures a scalp believed to be of Yeti kept in a safe glass box, where visitors can witness with some donation.<br>

                                     <b> Day 08:	Trek to Monjo 2,875 m - 05 hrs.</b><br>
                                    From Khumjung a short walk to nearby other village Khunde the only place with medical facilities and a hospital, built by late legendry Edmund Hillary who first reached the summit of Everest with Tenzing Sherpa.
                                    After an interesting visit around Khunde and its hospital a slow walk with downhill with aerial views of Namche Bazaar on reaching Namche with brief stop then continue walk downhill to Dudh Kosi River and at Sagarmatha National Park entrance and exit, from here a short walk to Monjo village for overnight stop.<br>

                                    <b> Day 09:	Trek to Lukla 2,818 m - 04 hrs.</b><br>
                                    Our last day walk to reach at Lukla, where trail leads across a long bridge to reach Toc-Toc and at Phakding for short break, and then continue trek on pleasant winding trail with last short climb to reach at Lukla for final and last overnight stop before flying back to Kathmandu.<br>

                                     <b> Day 10:	Fly back to Kathmandu and transfer to hotel with free afternoon at leisure.</b><br>
                                    Depending upon flight time for Kathmandu, morning with last breakfast in Khumbu and Lukla transfer to air terminal named after Tenzing and Hillary, the first to conquer Mt. Everest in 1953.
                                    As per flight schedule board in a smaller aircraft for sweeping short air journey to reach Kathmandu airport and then transfer back to your respective hotels.<br>

                                    <b> Day 11:	Reserve and contingency day in Kathmandu with individual activities.</b><br>
                                    Reserved as contingency day in Kathmandu in case of flight problem related with bad and unfavorable weather condition at Lukla or in the beginning, where extra day allows you with time for yourself for individual activities or join in our exclusive tour or just relax after a great adventure on high Khumbu and Everest base camp.<br>

                                    <b> Day 12:	Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories of Thyangboche Everest Panorama trekking, with last final day in Nepal where Mount Vision Trek staff and guide transfer you to Kathmandu international airport for your flight home ward bound.<br>


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent"style="background-color: whitesmoke">
                                <p>flgdlfjgk;dfjgodfpogfpid.</p>
                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg" style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg" style="height:200px;width: 200px;" />
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent"style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1" >
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box" >
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $ 1500</strike></strong></h3>
                        <h3> <p class="price "style="color:white;"><span>$1250</span> <small>/ person</small></p></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png"  style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>   </center><br>



                    <a href="booking.php?id=THYANGBOCHE EVEREST PANORAMA " class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=THYANGBOCHE EVEREST PANORAMA " class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {font-family: Arial;}

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {transform:scale(0)}
        to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal-content {
            width: 100%;
        }
    }
    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    function showImage(src){
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>



