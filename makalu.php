<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>MAKALU BASE CAMP TREKKING -16 Days</h1>
            <p style="font-size:17px">“Adventure at Nepal Far East around scenic Arun and Barun valley within amazing
                scenery
                On least visited region of Eastern Nepal where you will be walking in complete wilderness
                Glorious mountain views of Eastern mountain range with Makalu and Kanchenjunga
                Walk into enchanting alpine forest covered with oaks, pine, spruce and fir tree lines
                From low warm southern country to cooler alpine hills then reaching into arctic zone
                Explore traditional Rai and Sherpa villages of Eastern Himalaya within interesting culture
                Crossing high Kongma and Shipton-la to reach Barun Valley enclosed within high mountains
                Trekking towards World’s fifth highest Mt. Makalu with views of Everest rare East Face”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘adventure around Nepal Far East within Arun and Barun valley in the shade of high
                                    Makalu’ </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Makalu Base Camp Trekking, located towards Nepal Far North-East Himalaya further
                                    beyond Everest and west of world 3rd highest Mt. Kanchenjunga, Makalu towers high as
                                    fifth highest mountains on this planet at above 8,463 m / 27,766 ft high, which was
                                    climbed to its summit in May 15th 1955 by French Expeditions regarded as one of the
                                    most challenging and technical peak to climb.
                                    On this Makalu Base Camp Trekking where you will come across few trekkers,
                                    adventurer and mountaineers than other main stream trekking trail around Nepal
                                    Himalaya, where you will have enjoyable thrill walking to its base beneath towering
                                    black rocky pyramid of ice and snow Mt. Makalu, situated at the west end Barun
                                    valley.
                                    One of the least explored region of Nepal Far Mid-East Himalaya where you will have
                                    the opportunity to enjoy its serene wilderness walking around beautiful Arun and
                                    Barun Valley, where route leads you from sub-tropical temperature to cooler mid
                                    hills to high arctic zone of ice and glaciers.
                                    Makalu Base Camp Trekking starts with scenic short flight to reach at warmer low
                                    areas of Arum Valley in Tumlingtar town, located on the shelf of Arun River and then
                                    journey heads higher to begin our marvelous walk to world fifth highest Makalu base
                                    camp.
                                    Walking through remote and isolated of Num-Seduwa and Tashi-Gaon and then higher
                                    into lovely cool alpine forest to reach upper ridge of Makalu facing marvelous
                                    scenery of Makalu-Kanchenjunga-Baruntse and Chamlang, where route leads over few
                                    passes of Kongma-Shipton-la to reach at scenic Barun Valley.
                                    From Barun valley walk gets better with slow climb on valley floor with views of
                                    snow clad peaks to reach at Shersong and unto our final target at Makalu base camp
                                    with tremendous views of Makalu and other closer peaks with rare views of Mt.
                                    Everest known as Khangsung East Face only seen from this beautiful spot.
                                    After a great adventure return journey on the same route to reach back at Num and
                                    towards warmer farm areas at Tumlingtar for scenic sweeping flight to Kathmandu with
                                    memorable experience and wonderful adventure on Makalu Base Camp Trekking.


                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking:</b><br> Hotels and lodge with camping on walks.<br>
                                    <b> Trek Grade: </b><br> Moderate to Adventurous.<br>
                                    <b> Area of Trek: </b><br> Nepal Far North East within Sanka-Shava district.<br>
                                    <b> Highest Altitude Gain: </b><br> At Makalu Base Camp 4,814 m high.<br>
                                    <b> People and Culture: </b><br> Mainly populated with Kirat Rai-Magar and Sherpa of
                                    varied
                                    culture and religion of Hindu and Buddhism religions.<br>
                                    <b> Trek Duration: </b><br> 15 Nights and 16 Days (Tumlingtar to Tumlingtar with
                                    drives).<br>
                                    <b> Average walks:</b><br> Minimum 4 hrs to Maximum 6 hrs.<br>
                                    <b> Total Trip: </b><br> 18 Nights and 19 Days Kathmandu to Kathmandu.<br>
                                    <b>Seasons:</b><br> April to May and September to November months.
                                    April to May nice for walks when wild-flowers are in full bloom,
                                    where most of the days are clear for views. Morning and Night
                                    time as well in shade will be cold without wind-chill factor,
                                    October to November another best months for trek when day is
                                    clear, but with short sunlight hours due to autumn season cold
                                    morning and night times with chances of snow.
                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02: </b> Fly to Tumlingtar 476 m and drive to Num 1,490 m - 04 hrs with 45
                                    mins on air.<br>
                                    <b> Day 03:</b> Trek to Seduwa 1,540 m - 05 hrs.<br>
                                    <b> Day 04:</b> Trek to Tashi Gaon 2,135m - 04 hrs.<br>
                                    <b> Day 05:</b> Trek to Kongma Danda (Ridge) 3,556 m - 05 hrs.<br>
                                    <b> Day 06:</b> Trek to Mumbuk 3,553 m - 06 hrs.<br>
                                    <b> Day 07:</b> Trek to Nghe (Nehe) Kharka 3,760 m - 05 hrs.<br>
                                    <b> Day 08:</b> Trek to Shersong 4, 572 m - 05 hrs.<br>
                                    <b> Day 09: </b>Trek to Makalu Base Camp 4,812 m - 04 hrs.<br>
                                    <b>Day 10: </b>Rest day at base camp for local exploration and hike.<br>
                                    <b>Day 11: </b>Trek to Yangle Kharka 3, 598 m - 07 hours.<br>
                                    <b>Day 12:</b> Trek to Mumbuk - 05 hours.<br>
                                    <b>Day 13: </b>Trek to Kongma Ridge - 05 hours.<br>
                                    <b> Day 14: </b>Trek to Narbu Gaon 1,310 m via Tashi Gaon - 05 hours.<br>
                                    <b> Day 15: </b>Trek to Num via Seduwa - 05 hours.<br>
                                    <b> Day 16: </b>Drive back to Tumlingtar and transfer to local lodge.<br>
                                    <b>Day 17: </b> Fly back to Kathmandu and transfer to respective hotels.<br>
                                    <b>Day 18:</b> Spare and contingency days in case of flight delay at Tumlingtar.<br>
                                    <b>Day 19:</b> International departure for home ward bound.<br>
                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                        On arrival where you will be well received by our staff and guide and then
                                        transfer to your respective hotels in the centre of the city in Kathmandu, after
                                        checking into your rooms getting refreshed from jet-leg join with other members
                                        of Makalu Base Camp Trekking.
                                        Our guide / leader will brief with information about the hotels-including detail
                                        of trekking regarding local lodge, walks, view, history and culture with few
                                        important do’s and don’ts while you are with us having enjoyable times.
                                        Evening includes a nice dinner in a pleasant Nepalese restaurant with local
                                        cultural programs of all parts of the country to enlighten the environment along
                                        with your dinner.<br><br>
                                    <b> Day 02: Fly to Tumlingtar 476 m (45 mins) drive to Num 1,485 m - 04 hrs.</b><br>
                                        Morning transfer to Kathmandu domestic airport where flight takes you to Nepal
                                        Far East region at Tumlingtar within warm Arun valley, from here our Makalu base
                                        camp adventure begins, a scenic flight from Kathmandu takes you to land at
                                        warmer areas at Tumlingtar located on the shelf of Arun River.
                                        Tumlingtar, an interesting town with time for stroll and explore the traditional
                                        life of local hill people of eastern Nepal, from here transfer to local vehicle
                                        where drive leads towards mid hills past farm villages and town of Khandbari,
                                        headquarter of Sankha Sabha district of Makalu region.

                                        Drive continues on steep winding hills past Chinchela farm village with downhill
                                        to reach Num village for overnight in a nice local nice lodge with views of
                                        beautiful landscapes includes Jalajala Himal and Makalu peaks.<br><br>

                                    <b> Day 03: Trek to Seduwa 1,540 m- 05 hrs.</b><br>
                                        First day starts with long descend for an hour to cross over a bridge above Arun
                                        River, and then uphill to Seduwa village, walking past Runmura a small farm.<br><br>
                                    <b> Day 04: Trek to Tashi Gaon 2,135m - 04 hours.</b><br>
                                        From Seduwa village short day walk today morning walk follows a winding path to
                                        Tashigaon a moderate size farm village of Sherpa community, which is the last
                                        village on route to Makalu base camp, walking into farm area and then reaching
                                        at Tashigaon for lunch and overnight camp.
                                        Here with ample time to explore the village of great interest around to witness
                                        Sherpa Buddhist culture and age-old traditional life.<br><br>
                                    <b> Day 05: Trek to Kongma Danda 3,556 m - 05 hours.</b><br>
                                        After Tashi Gaon which be last human settlement for a week, as morning leads to
                                        gradual walk within dense forest of rhododendron, oaks and pines trees past
                                        several cattle herder’s shelters and shades.
                                        Walking for few hours on good moderate trail where afternoon walk leads to a
                                        climb for few hours to reach on top Kongma ridge, where tall trees fades to
                                        small rhododendron and juniper bushes.
                                        Overnight camp is set overlooking views of world third high Mt. Kanchenjunga
                                        with Mt. Makalu and adjoining peaks.<br><br>
                                    <b>  Day 06: Trek to Mumbuk 3,553 m - 06 hours.</b><br>
                                        From Kongma walk leads to complete wilderness following a ridge with few ups and
                                        down to reach at glacial ponds of Thulo Pokhari (large) with Sano
                                        Pokhari(small), and then cross over Tutu-la and Shipton la above 4, 453 m high
                                        passes, with downhill heading back into dense forest to our overnight camp in
                                        the midst of tree lines at place called Mumbuk.<br><br>
                                    <b>Day 07: Trek to Nehe Kharka 3,760 m - 05 hours.</b><br>
                                        Morning walks heading down towards scenic Barun valley, a place famous for
                                        unique rare species of vegetation- plants that’s almost extinct on this world,
                                        where medicinal herbs and mountain animals are found around.
                                        On reaching Barun valley, walk leads to gradual up with views of peaks on both
                                        sides of the valley and then encountering several Yak herder shelters and a
                                        small Buddhist Gompa (monastery), after a nice good walk reaching to our
                                        overnight camp at Nehe Kharka with scenic Barun valley.<br><br>
                                    <b>Day 08: Trek to Shersong 4, 572 m - 05 hours.</b><br>
                                        From Barun valley trek to Shersong which is at end of Barun valley towards
                                        Makalu base camp as walk lead to slow uphill then crossing streams with views of
                                        Mt. Makalu and Baruntse peaks, after a good walk of the day reaching at Sherson
                                        for overnight stop.<br><br>
                                    <b>Day 09: Trek to Makalu Base Camp 4,812 m - 04 hrs.</b><br>
                                        After Sherson heading to our main highlight and destination at Makalu Base Camp,
                                        where walk leads on high path over moraine and ice within rocky areas to reach
                                        at Makalu base camp, located close to a glacial icy pond next to Baruntse Himal.
                                        Our overnight camp set beneath towering South Face of Mt. Makalu, with time to
                                        explore and enjoy views of surrounding high peaks.<br><br>
                                    <b> Day 10: Rest day at base camp for local exploration and hike.</b><br>
                                        Rest day at Makalu base camp with views of Mt. Makalu, Baruntse Himal and peak
                                        IV, options for hike for views of Mt. Everest with its rare East Face known as
                                        Kangshung with Mt. Lhotse.<br><br>
                                    <b> Day 11: Trek to Yangle Kharka 3, 598 m - 06 hours.</b><br>
                                        From Makalu base camp retrace the walk back towards lower Barun valley to reach
                                        at Yangle Kharka for overnight stop enclosed within high rocky cliffs, offering
                                        views of waterfalls.<br><br>
                                    <b> Day 12: Trek to Mumbuk - 05 hours.</b><br>
                                        After a grand time around Barun valley, walk leads towards the end of Barun
                                        valley and then climb back into forest at Mumbuk for overnight camp.<br><br>
                                    <b> Day 13: Trek to Kongma Danda / Ridge - 05 hours.</b><br>
                                        Morning heading back on the same scenic trail to Kongma ridge, starting with a
                                        climb for an hour into forest areas to reach a ridge after leaving tree lines,
                                        as walk leads past the glacial ponds and over Tutu la and Shipton la to Kongma
                                        hill for overnight camp.<br><br>
                                    <b> Day 14: Trek to Narbu Gaon 1,310 m via Tashi Gaon - 05 hours.</b><br>
                                        Ending our high latitude trek as morning walk leads to long downhill into forest
                                        of tall tree lines to reach at Tashi Gaon, from here on route Nawa or Narbu gaon
                                        village, as our route leads into shade of forest and farm terraces to reach a
                                        large Sherpa village of Nawa Gaon on for overnight stop, with afternoon free to
                                        explore this nice farm village.<br><br>
                                    <b> Day 15: Trek to Num via Seduwa - 05 hrs.</b><br>
                                        Our last walk of the great adventure ends today following a trail to Seduwa
                                        village, from this village a long descend on steep path to reach a bridge over
                                        Arun River, after crossing the bridge steep climb for few hours to reach back at
                                        Num village for last overnight around high hills of Makalu Himalaya.
                                    <b>  Day 16: Drive to Tumlingtar for overnight in local Lodge.</b><br>
                                        Adventure complete this morning on taking exciting drive back to Tumlingtar past
                                        farm land of Chinchila, Manebhanjyang and large town of Khandbari and then back
                                        at Tumlingtar with time to browse around village to observe local culture and
                                        custom of the eastern Nepalese.<br><br>

                                    <b> Day 17: Fly back to Kathmandu and transfer to hotel.</b><br>
                                        Morning after overnight in a local lodge at Tumlingtar, then transfer towards
                                        airstrip for scenic short flight to land at Kathmandu, on reaching Kathmandu
                                        transfer to your respective hotels with free afternoon for individual
                                        activities.<br><br>
                                    <b>Day 18: Spare and contingency days in case of flight delay at Jhupal.</b><br>
                                        We have set this day as free as contingency with time to enjoy extra leisure day
                                        in Kathmandu after scenic trek on Makalu Base Camp, this day free for individual
                                        activities and shopping souvenirs, or join in for another exciting tour around
                                        Kathmandu-Patan and Bhaktapur at places of great interest.<br><br>

                                    <b>Day 19: Depart Kathmandu for international departure homeward bound.</b><br>
                                        Finally approaching last day in Kathmandu after great memorable experience and
                                        adventure on Makalu Base Camp, with last final day in Nepal where Mount Vision
                                        Trek staff and guide transfer you to Kathmandu international airport for your
                                        flight home ward bound.


                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=MAKALU BASE CAMP TREKKING"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=MAKALU BASE CAMP TREKKING" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



