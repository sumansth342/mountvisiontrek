<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/poon4.JPG); ">


            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>ANNAPURNA SUNRISE PANORAMA TREKKING- 10 Days</h1>
            <p style="font-size:17px">“Glorious mountain panorama with stunning sunrise from scenic Poon Hill
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Daily views of Annapurna Himalaya with Dhaulagiri and Manaslu peaks
                From low warm paddy fields to cooler alpine hills a great change of temperatures
                Explore traditional farm villages of Poon-Magar and Gurung people the hill tribes”
            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> 'adventure on both scenic sides of massive Annapurna Himalaya with high Thorang-la
                                    pass’</h3>
                                <p style="font-size: 17px;text-align: justify">Annapurna Sunrise Panorama Trekking,
                                    probably one of the best destinations in whole Himalaya region for spectacular
                                    panorama of high mountain range with stunning sunrise views, no- where in this world
                                    can beat the experience like on this scenic walk to Ghorepani and Poon trekking.
                                    Annapurna Sunrise Panorama Trekking a short adventure and combination of beautiful
                                    walks into traditional farm villages enriched with timeless culture and its own
                                    heritage of great interest, including views of white snow capped peaks on daily
                                    routine of walks.
                                    Besides trekking into nice and lovely hill villages in the shade of high Annapurna
                                    Himalaya, where walk leads you into enchanting woodland covered with rhododendron
                                    trees and bushes, when in spring and summer time from March to May, the whole
                                    country will be a paradise with rhododendron (national flower of the country) and
                                    magnolia in full bloom with other wild-flowers.
                                    Our scenic short adventure starts from beautiful Pokhara leading you to high green
                                    hills following the old Trans Himalayan Salt Trade Route of Nepal and Tibet all the
                                    way to lovely village at Ghorepani for great hike to Poon Hill with exceptional
                                    panorama of mountains with wonderful sunrise views.
                                    After a great time in high hills and nice traditional villages of Ghorepani and
                                    Ghandruk where walk leads to an end at Pokhara with journey back to Kathmandu,
                                    enjoying great exciting and super moments with fabulous experience on Annapurna
                                    Sunrise Panorama Trekking.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking:</b></br>Hotels and in lodge on walks.</br>
                                    <b> Trek Grade:</b></br>Moderate with ups and downhill walks.</br>
                                    <b>Area of Trek:</b></br>Nepal North Mid-West within Annapurna Himalaya region.</br>
                                    <b> Highest Altitude Gain: </b></br>On top Poon Hill 3,210 m high.</br>
                                    <b> People and Culture:</b></br>Mainly populated by Gurung and Poon-Magar enriched
                                    with</br>
                                    both Hindu and Buddhist religions with impressive culture.</br>
                                    <b> Trek Duration: </b></br>04 Nights and 05 Days (Pokhara to Pokhara)</br>
                                    <b> Average walks: </b></br>Minimum 4 hrs to Maximum 6 hrs.</br>
                                    <b> Total Trip:</b></br>09 Nights and 10 Days Kathmandu to Kathmandu.</br>
                                    <b>Seasons:</b></br>All months of the year except monsoon times from June to</br>
                                    August, however March to May is good for treks when wild-
                                    flowers in full bloom, where most of the days are clear for views
                                    Morning and Night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.


                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: </b>Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.</br>
                                    <b> Day 02: </b>In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.</br>
                                    <b> Day 03: </b>Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by
                                    air)</br>
                                    <b> Day 04: </b>Drive to Bhirethati 1,100 m and walk to Tirkhedhunga village 1,465 m
                                    - 04 hrs.</br>
                                    <b> Day 05: </b>Trek to Ghorepani 2,875 m via Ulleri village - 06 hrs.</br>
                                    <b> Day 06: </b>Hike to Poon Hill 3,210 m and continue trek to Tadapani 2,720 m - 05
                                    hrs.</br>
                                    <b> Day 07: </b>Trek to Ghandruk village 1,945 m - 04 hrs.</br>
                                    <b> Day 08: </b>Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                    ride.</br>
                                    <b> Day 09: </b>Drive or fly back to Kathmandu and transfer to hotel with free
                                    afternoon.</br>
                                    <b> Day 10: </b>Depart Kathmandu for international departure homeward bound.</br>


                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Annapurna
                                    Sunrise Panorama Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>


                                    <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach at Pokhara
                                    around Nepal Mid-West region for Annapurna Sunrise Panorama trekking.<br><br>


                                    <b> Day 03: Drive or fly to Pokhara 865 m - 06 hrs journeys (30 mins by air)</b><br>
                                    Morning after breakfast taking an interesting drive on Nepal main highway that
                                    connects with various parts of the country, where drive leads around warmer low
                                    areas past farm areas with villages and towns, following Trisuli River for sometime
                                    as drive heads further west to reach at beautiful and scenic Pokhara city by its
                                    serene Phewa lake for overnight stop.

                                    Pokhara one of the renowned touristic spot in whole Himalaya of warmer climate but
                                    with close views of towering and massive Annapurna Himalaya and majestic
                                    Machhapuchare Himal known as Fish Tail peak.
                                    Options by air to Pokhara take about 30 minutes of scenic air flight from Kathmandu.<br><br>


                                    <b>Day 04: Drive to Bhirethati 1,100 m and walk to Tirkhedhunga village 1,465 m - 04
                                        hrs.</b><br>
                                    With wonderful views of Annapurna range of peaks, where morning leads to a short
                                    drive of few hours heading past Pokhara city and its valley areas to reach at
                                    Nayapul and Bhirethati with fabulous scenery of Annapurna and green landscapes, on
                                    reaching nice Bhirethati village starting our first day walk following an old
                                    Trans-Himalayan Salt Trade Route of Nepal and Tibet with views of cascading
                                    waterfalls, and walking past farm areas and villages to reach our overnight stop at
                                    Tirkhedhunga close to nice waterfalls.<br><br>


                                    <b> Day 05: Trek to Ghorepani 2,875 m via Ulleri village - 06 hrs.</b><br>
                                    From Tirkhedhunga morning walk on steep stone steps with climb up to the village of
                                    Ulleri with views of Annapurna peaks, an interesting large village of Gurung and
                                    Magar people, from here a gradual walk leads you into enchanting forest of tall
                                    rhododendron-magnolia-oaks-pines and fir tree lines.
                                    As walk progress on winding up feel the change of temperatures with cooler mountain
                                    air as our day journey ends at nice Ghorepani village for overnight, located within
                                    woodland of rhododendron trees and views of Dhaulagiri mountain range.
                                    Ghorepani a famous and popular village for short trekking as well where hike leads
                                    to Poon Hill for stunning sunrise and mountain panorama.<br><br>


                                    <b> Day 06: Hike to Poon Hill 3,210 m and continue trek to Tadapani 2,720 m - 05
                                        hrs.</b><br>
                                    Early morning at the crack of dawn as weather permits, hike to Poon Hill with ups
                                    for an hour to reach the scenic top with a view tower at the height of 3,210 m the
                                    highest spot of the journey.
                                    From Poon Hill catch the striking sunrise views over chain of Himalayan Mountains
                                    that includes Dhaulagiri-Nilgiri’s-Annapurna with Machhapuchare Himal as far towards
                                    Lamjung and Manaslu range of peaks.
                                    After a glorious moments along with the views descend to Ghorepani, having breakfast
                                    where our route leads to high hills covered within rhododendron forest along with
                                    the views of snow-clad mountain range to reach at Deurali, a small settlement and
                                    then downhill into a gorge to Bhanthati for lunch or refreshing stop.
                                    From here a slow walk with short descend to a stream and the climb for short hour to
                                    reach at Tadapani village for overnight surrounded within forest overlooking views
                                    of Annapurna Himalaya range.<br><br>


                                    <b> Day 07: Trek to Ghandruk village 1,945 m - 04 hrs.</b><br>
                                    Morning with grand views of mountains, today a short walk to Ghandruk village where
                                    our journey leads into cool shade of forest and then downhill to reach at the top of
                                    Ghandruk village within farm houses, a short downhill to the centre of the village
                                    where many good lodges located.
                                    On reaching at main hub of Ghandruk village check into a fine lodge with afternoon
                                    free to relax and enjoy the views or visit its small museum of Gurung people, the
                                    tribe of the village as well spread all around Annapurna region.
                                    Gurung are farmers and cattle herders where most of the men-folk since last century
                                    a great demand as famous Gurkha Army in British and India military including
                                    Singapore and Brunei.<br><br>


                                    <b>Day 08: Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                        ride.</b><br>
                                    After a great time on high hills of Annapurna, morning walks lead downhill back into
                                    warmer areas full of farm villages as walk follows Modi River all the way to a nice
                                    Bhirethati village for short drive to reach back at scenic and beautiful Pokhara for
                                    last overnight by its Phewa Lake side.<br><br>


                                    <b> Day 09: Drive or fly back to Kathmandu and transfer to hotel with free
                                        afternoon.</b><br>
                                    With enjoyable time in high Annapurna Himalaya where morning overland journey takes
                                    you on the same route to reach back at Kathmandu, after a wonderful time and
                                    experience on Annapurna Sunrise Panorama trekking with rest of the afternoon free at
                                    leisure.<br><br>

                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.<br><br>


                                    <b> Day 10: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories of Annapurna Sunrise
                                    Panorama Trekking, with your last final day in Nepal where Mount Vision Trek staff
                                    and guide transfer you to Kathmandu international airport for your flight home ward
                                    bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Pokhara.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>

                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/poon1.JPG">
                                                        <img src="assets/images/poon1.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/poon3.jpg">
                                                        <img src="assets/images/poon3.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/poon4.JPG.jpg">
                                                        <img src="assets/images/poon4.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3>
                            <strong class="sidebar-heading" style="color: white;">Price:<strike> $1400</strike></strong>
                        </h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1320</span>
                                <small>/ person</small>
                            </p>
                        </h3>

                    </center>
                    <br>


                    <a href="booking.php?id=ANNAPURNA SUNRISE PANORAMA TREKKING "
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=ANNAPURNA SUNRISE PANORAMA TREKKING"
                       class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



