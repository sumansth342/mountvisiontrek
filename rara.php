<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/ebc1.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>LAKE RARA JOURNEY -10 Days</h1>
            <p style="font-size:17px">“A journey into Nepal Far and remote wild west around Jumla and Mugu districts
                On least visited region of western Nepal where you will be walking in complete wilderness
                Glorious mountain views of Api-Shaipal Himal and other western mountain range
                Walk into enchanting alpine forest covered with oaks, pine, spruce and fir tree lines
                From low warm southern country to cooler alpine hills with scenic country around Rara Lake
                Explore traditional Khas and Malla Thalkuri villages the ancient tribes of interesting cultures
                Facing panorama of peaks and dramatic landscapes around hills and lake of Rara area
                Starting and ending with sweeping-scenic flights to and from high country of Rara Lake”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                                <button class="tablinks" onclick="openCity(event, 'Reviews')">Reviews</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3>‘a scenic and fabulous journey to beautiful and largest Lake Rara of Nepal’h </h3>
                                <p style="font-size: 17px;text-align: justify">
                                    Lake Rara Journey one of the most fascinating destination to experience once in your
                                    life-time around Nepal Far North Western Himalaya, enclosed within rolling green
                                    hills and white peaks with verdant forest including nice and lovely farm villages.
                                    Country largest Lake Rara with an area of 10.8 sq kms located at an elevation of
                                    2,990 m high surrounded by awesome scenery of beautiful landscapes and mountains
                                    where you feel heaven on earth within its pristine and untouched areas on this trip
                                    Lake Rara Journey.
                                    Rara lake protected by Rara National Park one of the smallest park of Nepal covering
                                    an area 106 sq.kms where altitude ranges from 1,800m to 4,048 m high, within Rara
                                    covered by coniferous forest of blue pine, black juniper, West Himalayan spruce,
                                    oaks, Himalayan cypress and other associated species.
                                    At about 3,350 m, pine and spruce give away to fir, oak and birch. Deciduous tree
                                    species like Indian horse-chestnut, walnut and Himalayan popular are also found.
                                    Where this beautiful area serves as an excellent habitat for musk deer, with other
                                    animal Himalayan black bear, leopard, Goral, Himalayan Thar and wild boars, snow
                                    trout only fish species recorded in the lake.
                                    But the lake where gallinaceous birds and migrant waterfowl are also commonly
                                    sighted in this area, with crested grebe, black-necked grebe, and red-crested
                                    pochard are seen during winter includes common birds snow-cock, chukor partridge,
                                    Impeyan pheasant, Kalij pheasant and blood pheasant.
                                    Starting our marvelous Lake Rara Journey taking a delightful flight to reach Nepal
                                    Far South West and then towards high alpine country at Lake Rara area in the comfort
                                    of nice and lovely local lodge with time for excursion and hike around beautiful
                                    areas of Rara Lake.
                                    After an enjoyable and fabulous time heading back to Kathmandu using the same route
                                    back with magnificent memories and experience on Lake Rara Journey.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Mode of Trip: </b><br> Hotels with local lodge at Rara lake area.<br>
                                    <b> Trip Grade: </b><br> Moderate with ups and downhill walks.<br>
                                    <b>Area of Trip: </b><br> Nepal Far North West within Mugu district at Lake
                                    Rara.<br>
                                    <b>Highest Altitude Gain: </b><br> On top Murma Hill at 3,800 m high.<br>
                                    <b> People and Culture: </b><br> Mainly populated by Malla Thakuri and Khas
                                    Chettries with some
                                    Magar hill tribes of Hindu religion and various impressive cultures.<br>
                                    <b> Trip to Rara Duration: </b><br> 04 Nights and 05 Days Rara to Rara Lake with
                                    flights and walks.<br>
                                    <b> Average walks: </b><br> Minimum 02 to 04 hrs Maximum.<br>
                                    <b> Total Trip: </b><br> 09 Nights and 10 Days Kathmandu to Kathmandu.<br>
                                    <b> Seasons:</b><br> March to November months April to May nice for walks when
                                    wild- flowers are in full bloom, where most of the days are clear
                                    for views. Morning and Night time as well in shade will be cold
                                    without wind-chill factor, October to November another best
                                    months for trek when day is clear, but with short sunlight hours
                                    due to autumn season cold morning and night times with chances
                                    of snow.


                                </p>
                            </div>
                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b>Day 01:</b> Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.<br>
                                    <b> Day 02:</b> In Kathmandu with optional sightseeing tour and flight to Nepalgunj.<br>
                                    <b>Day 03: </b>Flight from Nepalgunj to Talcha airport and transfer to lodge.<br>
                                    <b>Day 04:</b> Hike from Talcha to Murma 3,800 m and reach at Hotel Rara View - 04
                                    hrs.<br>
                                    <b>Day 05-6:</b> Explore around Rara Lake 2,990 m and enjoy the views.<br>
                                    <b>Day 07: </b>Trek to Talcha for overnight stop.<br>
                                    <b>Day 08:</b> Morning flight to Kathmandu via Nepalgunj and transfer to hotels.<br>
                                    <b>Day 09: </b>In Kathmandu free as contingency day and for individual
                                    activities.<br>
                                    <b>Day 10:</b> International departure for home ward bound.<br>


                                </p>
                                <p>
                                    <b>Options Using Overland Services:</b><br>
                                    <b> Where four wheel Jeep and Bus available as follows:</b><br>
                                    <b> Day 01:</b>	Fly from Kathmandu to Nepalgunj and drive to Surkhet and transfer to hotel.<br>
                                    <b>Day 02:</b>	Drive to Kalikot, Manma with district headquarters via Dailekh.<br>
                                    <b>Day 03:</b>	Drive to Talcha or at Gamdadhi Bazaar.<br>
                                    <b>Day 04:</b>	Drive to Rara boundary at Milichaur and walk to Murmatop-04 hrs.
                                    Staying overnights at Hotel Rara View.<br>
                                    <b>Day 05:</b>	Spend day at Rara with pleasant having marvelous times.<br>
                                    <b>Day 06:</b>	Back to Kathmandu using the same route as above or taking flight from
                                    Talcha or Nepalgunj airport.<br>
                                    <b>Day 07: </b>	Fly back to Kathmandu via Nepalgunj and transfer to hotel.<br>
                                    <b> Day 08:</b>	Spare and contingency days in case of flight delay at Jhupal.<br>
                                    <b> Day 09:</b>	Depart Kathmandu for international departure homeward bound.<br>

                                </p>
                            </div>
                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: Arrival in Kathmandu via respective international airline and transfer
                                        to hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Lake Rara
                                    Journey.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    the trip regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner.<br><br>
                                    <b> Day 02: In Kathmandu with optional sightseeing tour and flight to Nepalgunj.</b><br>
                                    A half day to prepare for upcoming trip to remote areas of Lake Rara morning an
                                    optional sightseeing tour at places of great interest, exploring around Kathmandu
                                    world heritage sites for few hours and back at hotel, afternoon transfer to
                                    Kathmandu domestic airport for scenic flight to land at warm areas of Nepalgunj city
                                    airport.
                                    Located around Nepal Far South West for transit flight to Jhupal from Kathmandu here
                                    transfer to city hotel close to airport, for next morning early flight to Mugu area,
                                    where late afternoon with short time to witness the city areas of much Indian
                                    cultures due to its closeness to India border.<br><br>
                                    <b>Day 03: Flight from Nepalgunj to Talcha airport and transfer to lodge.</b><br>
                                    Morning after having early breakfast transfer to airport for short scenic flight to
                                    Rara lake areas, during flight views of Dhaulagiri and other western Himalaya,
                                    flying in smaller air-plane to reach at Talcha, newly constructed airstrip close to
                                    Rara Lake.
                                    Here you will be received by our staff and then walk to your lodge for overnight
                                    close to Rara Lake area, having with time in the afternoon to visit around to
                                    witness local people of Far Western Nepal.<br><br>
                                    <b>Day 04: Trek from Talcha to Murma 3,800 meters and reach at Hotel Rara View - 04
                                    hrs.</b><br>
                                    With interesting overnight stop at Talcha, where morning starts walking on gradual
                                    path and then climb up to a ridge to reach at scenic spot in Murma located on a high
                                    hill top, this will be the highest point of the journey.
                                    On reaching to your lodge with time to enjoy views of surrounding beautiful
                                    landscapes covered with pristine forest and snow capped peaks.<br><br>
                                    <b>Day 05-6: Explore and enjoy walk around Rara Lake 2,990 m and stunning views.</b><br>
                                    We have reserved two free days around this marvelous location where you can soak
                                    into beautiful surrounding with dramatic landscapes facing views of high mountains,
                                    as pleasant walk to reach our destination at Rara Lake, around the lake with few
                                    lodge built by the Park and village community for visitors.
                                    A perfect and ideal place to enjoy rest of the days, explore around this fabulous
                                    area where foreign visitors seldom venture this beautiful areas of Rara Lake, climb
                                    to a wooden tower for best views of Rara Lake with surrounding beautiful rolling
                                    green hills and snowcapped peaks.<br><br>
                                    <b>Day 07: Trek to Talcha for overnight stop.</b><br>
                                    Morning catching 380° panorama of stunning views of landscapes with Rara Lake and
                                    its surrounding lovely woodland including distant high snow-clad peaks, after
                                    breakfast walk back to Talcha for last overnight around marvelous country of Rara
                                    Lake.<br><br>
                                    <b>Day 08: Fly back to Kathmandu via Nepalgunj and transfer to hotel.</b><br>
                                    Morning after overnight stop in a local lodge at Talcha, transfer to airstrip for
                                    scenic short flight to land at Nepalgunj airport, here with times to get refreshed
                                    before catching another flight back to Kathmandu. On reaching Kathmandu transfer to
                                    your respective hotels with free afternoon for individual activities.
                                    <b> Day 09: Spare and contingency days in case of flight delay at Jhupal.</b><br>
                                    We have set this day as free as contingency with time to enjoy extra leisure day in
                                    Kathmandu after scenic journey on Lake Rara, this day free for individual activities
                                    and shopping souvenirs, or join in for another exciting tour around Kathmandu-Patan
                                    and Bhaktapur at places of great interest.<br><br>

                                    <b>Day 10: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memorable experience on Lake
                                    Rara Journey, with last final day in Nepal where Mount Vision Trek staff and guide
                                    transfer you to Kathmandu international airport for your flight home ward bound.

                                </p>
                            </div>
                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Lukla-Kathmandu.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/blog-1.jpg">
                                                        <img src="assets/images/blog-1.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/blog-2.jpg">
                                                        <img src="assets/images/blog-2.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Reviews" class="tabcontent" style="background-color: whitesmoke">
                                <p>fdngfdm,gnfdngdkfnglkdfg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $
                                    1500</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1250</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <img id="myImg" src="assets/images/map.png" style="width:100%;max-width:300px">

                                <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- The Close Button -->
                                    <span class="close">&times;</span>

                                    <!-- Modal Content (The Image) -->
                                    <img class="modal-content" id="img01">

                                    <!-- Modal Caption (Image Text) -->
                                    <div id="caption"></div>
                                </div>
                    </center>
                    <br>
                    <a href="booking.php?id=LAKE RARA JOURNEY"
                       class="btn btn-primary btn-outline btn-block">Book Now</a></p>
                    <a href="enquiry.php?id=LAKE RARA JOURNEY" class=" btn btn-primary btn-outline
                       btn-block">Enquiry</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--NewsLetter-->

<!--Footer-->
<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



