<?php include('navigation.php');
?>
<head>
    <link type="text/css" rel="stylesheet" href="light-gallery/css/lightGallery.css"/>

    <script src="light-gallery/js/lightgallery.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="light-gallery/js/lg-fullscreen.min.js"></script>

    <script src="light-gallery/js/lightgallery-all.js"></script>
</head>
<aside id="vision-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(assets/images/annapurna.JPG); ">
            </li>
        </ul>
    </div>
</aside>


<div class="vision-wrap">
    <div class="row">
        <div class="col-md-8 col-md-push-1">
            <h1>AROUND ANNAPURNA TREKKING- 13 Days</h1>
            <p style="font-size:17px">“A great wonderful adventure around both sides of massive Annapurna Mountains
                Range
                Listed as world’s best adventure destinations by Lonely Planet and other guide / travel books
                Walking into high and scenic Manang valley towards Mustang area via Thorang-la pass
                Exceptional views if peaks on daily walks to windswept Manang valley and its villages
                Explore beautiful culture and custom of Buddhist religion around traditional Manang villages
                Glorious panorama with stunning sunrise over chain of mountains from scenic Poon Hill
                Walk into beautiful hill forest covered with rhododendron-oak and pine trees
                Daily views of Annapurna Himalaya with close views of Dhaulagiri and Manaslu peaks
                From low warm paddy fields to cooler alpine hills and towards arctic zone of ice and glaciers”


            </p>
            <div class="row">
                <div class="wrap-division">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab" style="background-color: #effbfe">
                                <button class="tablinks" onclick="openCity(event, 'Overview')" id="defaultOpen">
                                    Overview
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Glance')">Trip at a Glance</button>
                                <button class="tablinks" onclick="openCity(event, 'Itinarary')">Brief Itineary</button>
                                <button class="tablinks" onclick="openCity(event, 'Day to Day Itinerary')">Day to Day
                                    Itinerary
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Includes Excludes')">Include
                                    Excludes
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Gallary')">Gallary</button>
                            </div>

                            <div id="Overview" class="tabcontent" style="background-color: whitesmoke">
                                <h3> 'adventure on both scenic sides of massive Annapurna Himalaya with high Thorang-la
                                    pass’</h3>
                                <p style="font-size: 17px;text-align: justify">Around Annapurna Trekking, a great way to
                                    enjoy the adventurous walks the region also called as Annapurna Circuit one of
                                    world’s best adventure destination as listed by famous and popular Guide and Travel
                                    books and magazines.
                                    Truly an adventure of great fascinations from view point to local native cultures of
                                    age-old heritage which you will witness on this fabulous journey on ‘Around
                                    Annapurna Trekking’ the most amazing nature of the walks is variation of culture,
                                    people, vegetations, landscapes and temperatures.
                                    Where walk starts after an interesting long drive from Kathmandu to reach trekking
                                    point around Nepal Mid-West warmer areas of Lamjung district, as walk leads from
                                    warm-low paddy fields to reach at alpine hills of cooler climate covered within
                                    lovely forest of rhododendron and pine trees.
                                    As walk gains height visiting nice villages of Jagat-Chamje-Tal and Dharapani to
                                    reach a fair size town of Chame, headquarter and main town of Manang district, after
                                    a time here walk continues along with wonderful views of Annapurna-Lamjung and
                                    Manaslu peaks to reach at Pisang, where Manang valley starts within windswept
                                    terrain enclosed within high Annapurna and Chulus with Pisang peak.
                                    Slowly our adventure reaches main Manang village hidden within high peaks of
                                    Annapurna II, A-III and Gangapurna with Chulus, in Manang having rest day for short
                                    hike to support acclimatization, an interesting village interwoven with Buddhism
                                    religion and culture as well visiting its old monastery of Braga.
                                    Walk leads further west to cross over high Thorang-La pass at 5,416 m facing
                                    spectacular panorama of surrounding peaks and landscapes with views of Dhaulagiri
                                    and Mustang Himal and then heading towards Lower Mustang area at Mustang, a holy
                                    spot for both Hindu and Buddhism where thousands of pilgrims from all over Nepal and
                                    India visits here to worship the temple of Muktinath dedicated to Hindu Lord Vishnu.
                                    From Muktinath due to motorable road making this trip possible to cover the whole
                                    trek within a short distance of time, where an exciting overland journey leads to
                                    Kaligandaki River valley past Jomsom town and Marpha villages and then heading
                                    towards our destination at Tatopani, a village with natural hot-spring as its names.
                                    Enjoying refreshing moments at Tatopani and around its nice village, where our walk
                                    continues with climb into cooler areas and beautiful rhododendron forest reaching at
                                    famous and popular scenic village at Ghorepani.
                                    After Ghorepani with short hike to Poon Hill for sunrise over grand panorama of
                                    Himalayan mountains, then heading downhill to low and warmer areas where our
                                    memorable adventure and marvelous Around Annapurna Trekking completes on taking a
                                    short drive to reach at beautiful Pokhara city by its calm Phewa lake.

                                </p>
                            </div>
                            <div id="Glance" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Mode of Trekking: </b></br>    Hotels and in lodge on walks.</br>
                                    <b> Trek Grade: </b></br>    Moderate and Adventurous.</br>
                                    <b> Area of Trek: </b></br>    Nepal North Mid-West within Annapurna Himalaya
                                    region.</br>
                                    <b> Highest Altitude Gain: </b></br>On top Thorang-La pass at 5,416 m / 17,700 ft
                                    high.</br>
                                    <b> People and Culture: </b></br>    Mainly populated by Manang, Thakali, Gurung and
                                    Poon-Magar
                                    enriched with Buddhist religions with impressive culture.
                                    <b> Trek Duration: </b></br>        12 Nights and 13 Days (with drives both
                                    ways)</br>
                                    <b> Average walks: </b></br>    Minimum 4 hrs to Maximum 6 hrs.</br>
                                    <b> Total Trip: </b></br>        16 Nights and 17 Days Kathmandu to Kathmandu.</br>
                                    <b> Seasons: </b></br>        March to May and September to December months</br>
                                    March to May good for treks when wild-flowers in full bloom,
                                    where most of the days are clear for views.
                                    Morning and night time as well in shade will be cold without
                                    wind-chill factor, October to December another best months to
                                    trek when day is clear, but short sunlight hours due to autumn
                                    and winter months will be very cold morning / night times with
                                    chances of snow.

                                </p>
                            </div>

                            <div id="Itinarary" class="tabcontent" style="background-color: whitesmoke">
                                <p>
                                    <b> Day 01: </b>Arrival in Kathmandu via respective international airline and
                                    transfer to hotel.</br>
                                    <b> Day 02: </b>In Kathmandu with optional sightseeing tour and preparation for the
                                    trek.</br>
                                    <b> Day 03: </b>Drive to Jagat via Besisar 980 m - 06 hrs journeys.</br>
                                    <b> Day 04: </b>Trek to Dharapani village 1,915 m - 06 hrs.</br>
                                    <b> Day 05: </b>Trek to Chame town 2,675 m - 05 hrs.</br>
                                    <b> Day 06: </b>Trek to Pisang village 3,120 m - 05 hrs.</br>
                                    <b> Day 07: </b>Trek to main Manang village 3,450 m - 05 hrs.</br>
                                    <b> Day 08: </b>At Manang rest day for acclimatization and short hike.</br>
                                    <b> Day 09: </b>Trek to Yak Kharka 4,110 m - 04 hrs.</br>
                                    <b> Day 10: </b>Trek to Thorang High Camp 4,495 m - 04 hrs.</br>
                                    <b> Day 11: </b>Trek over Thorang-la Pass 5,416 m and descend to Muktinath 3,795 m -
                                    06 hrs.</br>
                                    <b> Day 12: </b>Drive to Tatopani 1,100 m via Jomsom-Marpha and Lete- 04 hrs.</br>
                                    <b> Day 13: </b>Trek to Ghorepani 2,875 m - 06 hrs.</br>
                                    <b> Day 14: </b>Hike to Poon Hill 3,210 m and trek to Tirkhedhunga village 1,465m -
                                    05 hrs.</br>
                                    <b> Day 15: </b>Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                    ride.</br>
                                    <b> Day 16: </b>Drive or fly back to Kathmandu and transfer to hotel with free
                                    afternoon.</br>
                                    <b> Day 17: </b>Depart Kathmandu for international departure homeward bound.</br>


                                </p>
                            </div>

                            <div id="Day to Day Itinerary" class="tabcontent" style="background-color: whitesmoke">
                                <p><b>Day 01: Arrival in Kathmandu via respective international airline and transfer to
                                        hotel.</b><br>
                                    On arrival where you will be well received by our staff and guide and then transfer
                                    to your respective hotels in the centre of the city in Kathmandu, after checking
                                    into your rooms getting refreshed from jet-leg join with other members of Around
                                    Annapurna Trekking.
                                    Our guide / leader will brief with information about the hotels-including detail of
                                    trekking regarding local lodge, walks, view, history and culture with few important
                                    do’s and don’ts while you are with us having enjoyable times.
                                    Evening includes a nice dinner in a pleasant Nepalese restaurant with local cultural
                                    programs of all parts of the country to enlighten the environment along with your
                                    dinner..<br><br>

                                    <b> Day 02: In Kathmandu with optional sightseeing tour and preparation for the
                                        trek.</b><br>
                                    A free day to prepare for the upcoming treks to high Khumbu, morning an optional
                                    sightseeing tour at places of great interest, exploring around Kathmandu world
                                    heritage sites for few hours and back at hotel, afternoon free for individual
                                    activities and preparations for next morning overland journey to reach at Pokhara
                                    around Nepal Mid-West region for Around Annapurna trekking.<br><br>

                                    <b> Day 03: Drive to Jagat via Besisar 980 m - 06 hrs journeys.</b><br>
                                    Around Annapurna adventure starts taking an interesting overland journey towards
                                    Nepal Mid West, leading to warmer areas at Besisar town (headquarter of Lamjung
                                    district).
                                    From here transfer to another vehicle and continue drive on dirt road past Khudi,
                                    Bubule and Ngadi villages following along Masrsyangdi River upstream.
                                    Drive continues through rice terraces fields and villages to Syange a small village
                                    and then short uphill drive to reach a nice farm village in Jagat for overnight
                                    stop.<br><br>

                                    <b> Day 04: Trek to Dharapani village 1,915 m - 06 hrs.</b><br>
                                    From Jagat our walk begins with slow climb above Masrsyangdi River as our route
                                    leads on winding path to Chamje, and back at Masrsyangdi River and then crossing to
                                    its east side within forested area, with views of Annapurna and Manaslu mountain
                                    ranges.
                                    As walk continues reaching nice village of Tal Besi, from here another three hours
                                    walk leads to our overnight stop at Dharapani village, this nice village located on
                                    the junction of Annapurna circuit and Manaslu trails.<br><br>

                                    <b> Day 05: Trek to Chame town 2,675 m - 05 hrs.</b><br>
                                    After Dharapani walk leads into cool forest of rhododendron and pine trees and
                                    heading towards Bagarchap and Danque villages to reach at Timang where you will
                                    notice more strong Buddhist culture with views of Mt. Manaslu 8,163m / 26,781ft.
                                    From Timang village few hours walk leads to Koto and then at Chame town for
                                    overnight stop, Chame a large and major town of Manang district which is also head
                                    quarter the area.<br><br>

                                    <b> Day 06: Trek to Pisang village 3,120 m - 05 hrs.</b><br>
                                    From Chame onward heading closer towards mountains as walk leads through alpine
                                    forest following Masrsyangdi River upstream, with short ups and down to reach
                                    Bhratang village, after a short stop walk follows uphill into woodland to Dhukure
                                    Pokhari a small place on top of a hill with few teahouses and lodge facing views of
                                    Chulus and Pisang peaks, with possible lunch stop, then descend towards lower Pisang
                                    village for overnight stop.<br><br>

                                    <b>Day 07: Trek to main Manang village 3,450 m - 05 hrs.</b><br>
                                    Morning catch views of mountains, from here onward altitude gains slowly as walk
                                    leads into forest with short uphill facing excellent views of Annapurna II and III,
                                    Gangapurna, Chulus and Pisang peaks.
                                    From the top ridge downhill to reach a flat valley at Hongde a village with small
                                    airstrip, from here onward tree lines fades for barren, arid landscapes with cold,
                                    dry arctic climate.
                                    Walking into main village of proper Manang for overnight stop, before Manang an
                                    interesting tour of old Braga monastery enriched with ancient Buddhist relics, wall
                                    painting and frescos includes stunning views of Annapurna Himalaya.<br><br>

                                    <b>Day 08: At Manang rest day for acclimatization and short hike.</b><br>
                                    A rest day at Manang for acclimatization before reaching higher areas above
                                    Thorang-la pass,
                                    on this day take a hike around Manang village, a large village situated on scenic
                                    arid valley by the bank of Marysangdi River, which is below from Annapurna and
                                    Gangapurna mountain range, a nice walk uphill at Manang area for great views of
                                    Gangapurna Glacier and Manang glacial Lake.<br><br>

                                    <b> Day 09: Trek to Yak Kharka 4,110 m - 04 hrs.</b><br>
                                    After a pleasant rest at Manang, walk past Ghunsang village with short uphill to
                                    reach at Yak Kharka, an interesting walk with views of snowcapped mountains, on
                                    crossing a small stream reaching Yak Kharka for overnight stop, a small settlement
                                    with views of Gangapurna, Annapurna III and Chulu Peaks.<br><br>

                                    <b> Day 10: Trek to Thorang High Camp 4,495 m - 04 hrs.</b><br>
                                    Morning walk lead to higher elevation and getting closer at the base of Thorang-la,
                                    on crossing a bridge and then climb along a river bank, finally walk leads to
                                    Thorang Phedi, here depending upon the physical conditions of the clients, if all
                                    goes well an hour steep climb to Thorang High Camp for overnight stop.<br><br>

                                    <b> Day 11: Trek over Thorang-la Pass 5,416 m and descend to Muktinath 3,795 m - 06
                                        hrs.</b><br>
                                    One of the highlight the adventure, after early breakfast a long climb leads you to
                                    Thorang-la walking on steep winding path to reach at Thorang La Pass above 5,416 m /
                                    17,700 ft.
                                    The highest point of the adventure with views of snowcapped peaks, and then with
                                    long descends to reach at holy Muktinath around Lower Mustang area.
                                    On reaching Muktinath for overnight stop, this is a holy spot for both Hindus and
                                    Buddhists the word Muktinath literally means ‘the place for Nirvana or
                                    Liberation’.<br><br>

                                    <b>Day 12: Drive to Tatopani 1,100 m via Jomsom-Marpha and Lete- 04 hrs.</b><br>
                                    From holy sites of Muktinath with views of Dhaulagiri range, morning with temporary
                                    overland ride to reach at lower area of Mustang, since motorable road has been built
                                    where trek is disturbed by movement of vehicles, so taking a drive instead of
                                    walking.
                                    Drive leads towards wide Kaligandaki River within its windswept landscapes and
                                    heading past Jomsom the headquarter town of Mustang area, and then heading due west
                                    following the Kaligandaki River downstream to reach at attractive Tatopani a village
                                    famous for its natural hot spring.
                                    Before Tatopani drive leads past world deepest gorge of Kaligandaki River situated
                                    between giant peaks of Dhaulagiri and Annapurna Himalaya, on reaching this nice
                                    village with time to enjoy a dip in the hot spring.<br><br>

                                    <b>Day 13: Trek to Ghorepani 2,875 m - 06 hrs.</b><br>
                                    From Tatopani, our adventure and walk continues crossing over a bridge of
                                    Kaligandaki River and then route diverts towards uphill on terraced hillsides,
                                    dotted with farms and villages after a steep section through dense alpine forest
                                    reaching at Chitre and to Ghorepani- village with super views of Mt. Dhaulagiri,
                                    Annapurna South and Vara-Shikar also known Annapurna Fang.<br><br>

                                    <b> Day 14: Hike to Poon Hill 3,210 m and trek to Tirkhedhunga village 1,465m - 05
                                        hrs.</b><br>
                                    Early morning at the crack of dawn as weather permits, hike to Poon Hill with ups
                                    for an hour to reach the scenic top with a view tower at the height of 3,210 m the
                                    highest spot of the journey.
                                    From Poon Hill catch the striking sunrise views over chain of Himalayan Mountains
                                    that includes Dhaulagiri-Nilgiri’s-Annapurna with Machhapuchare Himal as far towards
                                    Lamjung and Manaslu range of peaks.
                                    After a glorious moment along with the views descend to Ghorepani, having breakfast
                                    where our route leads within rhododendron forest along with the views of snow-clad
                                    mountain range to reach at Bhanthati for lunch or refreshing stop.
                                    From here a slow walk downhill past nice farm village of Uller with long descend on
                                    stone paved path to reach at Tirkhedhunga village for last overnight in rural
                                    village with beautiful cascading waterfalls closeby.<br><br>

                                    <b> Day 15: Trek to Bhirethati or Nayapul - 04 hrs and drive to Pokhara - 02 hrs
                                        ride.</b><br>
                                    After a great time on high hills of Annapurna, morning walks lead downhill back into
                                    warmer areas full of farm villages as walk follows to reach a nice Bhirethati
                                    village, where short drive reaches you at scenic and beautiful Pokhara for last
                                    overnight by its Phewa Lake side.<br><br>

                                    <b> Day 16: Drive or fly back to Kathmandu and transfer to hotel with free
                                        afternoon.</b><br>
                                    With enjoyable time in high both sides of Annapurna Himalaya where morning overland
                                    journey takes you on the same route to reach back at Kathmandu, after a wonderful
                                    time and experience on Around Annapurna trekking with rest of the afternoon free at
                                    leisure.
                                    For people with options by air to Kathmandu 30 minutes of scenic flight from
                                    Pokhara.

                                    <b> Day 17: Depart Kathmandu for international departure homeward bound.</b><br>
                                    Finally approaching last day in Kathmandu after great memories and adventure on
                                    Around Annapurna Trekking, with your last final day in Nepal where Mount Vision Trek
                                    staff and guide transfer you to Kathmandu international airport for your flight home
                                    ward bound.


                                </p>
                            </div>

                            <div id="Includes Excludes" class="tabcontent" style="background-color: whitesmoke">
                                <p><i class="icon-tick"></i></a></li>Airport Pickup and Drop by Private vechicles.<br>
                                    <i class="icon-tick"></i></a></li>Tourists Standard Hotel in kathmandu with
                                    breakfast.<br>
                                    <i class="icon-tick"></i></a></li>Full Kathmandu City Tour (entrance fee not
                                    included).<br>
                                    <i class="icon-tick"></i></a></li>Standard Meals (breakfast, lunch and dinner)
                                    during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Accommodation TeaHouse during the trek.<br>
                                    <i class="icon-tick"></i></a></li>Trip flight fare Kathmandu-Pokhara.<br>
                                    <i class="icon-tick"></i></a></li>Required Paper Works During the Trek(permit &
                                    TIMS)<br>
                                    <i class="icon-tick"></i></a></li>Farewell dinner at the end of Trek.<br>
                                    <i class="icon-tick"></i></a></li> Government taxes and official expenses.</p>

                                <p><i class="icon-cross"></i></a></li>Lunch and dinner during stay in City.<br>
                                    <i class="icon-cross"></i></a></li>All kinds of Personal expenses<br>
                                    <i class="icon-cross"></i></a></li>All kind of beverages (tea/coffee, coke, beer,
                                    bottled water etc)<br>
                                    <i class="icon-cross"></i></a></li> Travel Insurance(must-have).<br>
                                    <i class="icon-cross"></i></a></li>Trekking Gear.<br>
                                    <i class="icon-cross"></i></a></li> Tipping (expected by guide and porters, but not
                                    mandatory)<br>
                                    <i class="icon-cross"></i></a></li> International AirFare<br>
                                    <i class="icon-cross"></i></a></li> Any misfortune emerging because of unanticipated
                                    conditions that is beyond Mount Vision treks & Expedition Controls.<br></p>

                            </div>
                            <div id="Gallary" class="tabcontent">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="gallery">
                                                <div id="lightgallery">
                                                    <a href="assets/images/abc4.jpg">
                                                        <img src="assets/images/abc4.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/abc5.jpg">
                                                        <img src="assets/images/abc5.jpg"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/abc6.JPG">
                                                        <img src="assets/images/abc6.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                    <a href="assets/images/abc7.JPG">
                                                        <img src="assets/images/abc7.JPG"
                                                             style="height:200px;width: 200px;"/>
                                                    </a>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- SIDEBAR-->
        <div class="col-md-2 col-md-push-1">
            <div class="sidebar-wrap">
                <div class="side search-wrap animate-box">
                    <center>
                        <h3><strong class="sidebar-heading" style="color: white;">Price:<strike> $1700</strike></strong></h3>
                        <h3>
                            <p class="price " style="color:white;"><span>$1615</span>
                                <small>/ person</small>
                            </p>
                        </h3>
                    </center>
                    <br>

                    <a href="booking.php?id=AROUND ANNAPURNA TREKKING " class="btn btn-primary btn-outline btn-block">Book
                        Now</a></p>
                    <a href="enquiry.php?id=AROUND ANNAPURNA TREKKING" class="btn btn-primary btn-outline btn-block">Enquiry</a></p>

                </div>
            </div>
        </div>

    </div>
</div>
</div>


<!--NewsLetter-->

<!--Footer-->

<?php include('footer.php'); ?>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<style>
    body {
        font-family: Arial;
    }

    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ffdd00;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ffdd00;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (Image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 90%;
        max-width: 900px;
    }

    /* Caption of Modal Image (Image Text) - Same Width as the Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation - Zoom in the Modal */
    .modal-content, #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: darkred;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: red;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    /*.modal-dialog{text-align:center;}
    .modal-content{display:inline-block;}
    .modal{
        margin-left:0 !important;
        }*/

</style>

<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    function showImage(src) {
        $("#exampleModal").modal("toggle");
        // $('.image-pop').html('<img height="500px" src="'+src+'"/>');


    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#lightgallery").lightGallery();
    });
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }
</script>



